#ifndef STATUS_H
#define STATUS_H

#include <string>

enum status{S, E, Is, Ic, R, U}; // U for unknown
enum herdtype{outdoor, indoor};
enum herdtype2{out, in};

class Status{
	public:
	static std::string statusToString(status s);
};
#endif
