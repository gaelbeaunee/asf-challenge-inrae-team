//
//  metapopulation.cpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#include "metapopulation.hpp"

// Constructor
metapopulation::metapopulation(const int& _rep_id){
    rep_id = _rep_id;
    // select population(s) where to initiate the infection and add them to the list_pop_initially_infected
    // in pop of sp1
    if (simulationParameters::number_of_infected_sp1_pop > 0) {
        auxFunctions::randomSamplingWithoutReplacement(simulationData::sp1_pop_id_list, list_pop_initially_infected, simulationParameters::number_of_infected_sp1_pop, simulationParameters::random_engine);
    }
    // in pop of sp2
    if (simulationParameters::number_of_infected_sp2_pop > 0) {
        auxFunctions::randomSamplingWithoutReplacement(simulationData::sp2_pop_id_list, list_pop_initially_infected, simulationParameters::number_of_infected_sp2_pop, simulationParameters::random_engine);
    }
    // DEBUG: display id of pop. initially infected
//    if (list_pop_initially_infected.size() > 0) {
//        std::cout << "list_pop_initially_infected: ";
//        for (auto item : list_pop_initially_infected) {
//            std::cout << item << ' ';
//        }
//        std::cout << std::endl;
//    }
    // create populations based on the population characteristics data
    for (const auto& pop_char : simulationData::pop_characteristics) {
        std::string the_pop_id = pop_char.second.pop_id;
        std::string the_pop_type = pop_char.second.pop_type;
        int the_pop_index = simulationData::map_of_pop_index[the_pop_id];
        int the_pop_initial_size = pop_char.second.pop_initial_size;
        bool is_initially_infected = false;
        auto search = list_pop_initially_infected.find(the_pop_id);
        if (search != list_pop_initially_infected.end()) {
            is_initially_infected = true;
        }
        bool unknown_pop_type = false;
        if (the_pop_type == simulationParameters::sp1_name) {
            // create and add the population
            populations[the_pop_id] = std::unique_ptr<species1Population>(new species1Population(the_pop_id, the_pop_index, the_pop_initial_size, is_initially_infected, populations, summary)); // equivalent to: populations.insert(std::pair< std::string, std::unique_ptr<species1Population>> (the_pop_id, std::unique_ptr<species1Population>(new species1Population(the_pop_id, the_pop_index, the_pop_initial_size, is_initially_infected, populations, summary))));
            sp1_pop_count++;
        } else if (the_pop_type == simulationParameters::sp2_name) {
            // create and add the population
            populations[the_pop_id] = std::unique_ptr<species2Population>(new species2Population(the_pop_id, the_pop_index, the_pop_initial_size, is_initially_infected, populations, summary));
            sp2_pop_count++;
        } else {
            unknown_pop_type = true;
            std::cout << "WARNING - this population type is unknown: " << the_pop_type << ", the population was not created!" << std::endl;
        }

        // init some variables used to compute FoI
        double I = populations[the_pop_id]->headcount.getIcount(); // TODO: update for ASF
        double D = populations[the_pop_id]->headcount.getDcount(); // TODO: update for ASF
        double N = populations[the_pop_id]->headcount.sum();
        summary.headcount_I_previous_timestep[populations[the_pop_id]->pop_index] = I;
        summary.headcount_I_current_timestep[populations[the_pop_id]->pop_index] = I;
        summary.headcount_D_previous_timestep[populations[the_pop_id]->pop_index] = D;
        summary.headcount_D_current_timestep[populations[the_pop_id]->pop_index] = D;
        summary.headcount_N_previous_timestep[populations[the_pop_id]->pop_index] = N;
        summary.headcount_N_current_timestep[populations[the_pop_id]->pop_index] = N;
        summary.headcount_I_N_previous_timestep[populations[the_pop_id]->pop_index] = (N > 0) ? (I/N) : 0;
        summary.headcount_I_N_current_timestep[populations[the_pop_id]->pop_index] = (N > 0) ? (I/N) : 0;
        summary.headcount_D_N_previous_timestep[populations[the_pop_id]->pop_index] = (N > 0) ? (D/N) : 0;
        summary.headcount_D_N_current_timestep[populations[the_pop_id]->pop_index] = (N > 0) ? (D/N) : 0;

        if (!unknown_pop_type) {
            // update the summary headcount
            summary.headcount_over_time[simulationParameters::T[0]][the_pop_id] = populations[the_pop_id]->headcount;
            // increase the total_pop_count
            total_pop_count++;
            // DEBUG: display pop. info.
//            populations[the_pop_id].displayCurrentStatus();
//            if (is_initially_infected) {
//                populations[the_pop_id].displayCurrentStatus();
//            }
        }
    }
}

// Member functions
double metapopulation::stoc_dyn_iteration(){
    Chronometer chrono_stoc_dyn_iteration;
    chrono_stoc_dyn_iteration.start();
    std::cout << "metapopulation " << std::to_string(rep_id) << " (time for stoc_dyn_iteration)... ";

    #ifdef PGCOUPLING
    // Update the initial status of DP pop in the WB simulation
    if (simulationParameters::model_coupling_for_prediction) {
		  int k=0;
        for (auto& PGcurrentStates : simpg.getStatusPG(0)) {
            sirHeadcount currentSnapShot = sirHeadcount();
            currentSnapShot.update(PGcurrentStates.second, 1);
            populations[PGcurrentStates.first]->updateSnapshotOfTheDynamic(0, currentSnapShot, summary);
        }
    }
    #endif

    for (auto i=1; i<simulationParameters::T.size(); i++) {
        // set the value of the current timestep
        double current_timestep = simulationParameters::T[i];
        // swap current and next objects in order to get information update during previous timestep
        current_pending_movements.swap(next_pending_movements);
        next_pending_movements.clear();
        // compute the sum of headcount for foi calculation (using armadillo matrix multiplication)
        if (current_timestep == simulationParameters::date_on_which_the_fence_is_built) {
            simulationData::rebuildContactMatrix(true);
        }
        summary.computeTermsOfFOI();
        // loop on the populations
        for (auto& this_pop : populations) {
            if (simulationParameters::model_coupling_for_prediction) { // in case of prediction, only the sp2 pop dynamic is computed and sp1 pop dynamic will be udpated separatly after the sp1 dynamic update done using the simulator (part above)
                if (this_pop.second->is_sp2) {
                    this_pop.second->performIngoingMovements(current_timestep, current_pending_movements, summary);
                    this_pop.second->updateStochasticDynamic(current_timestep, populations, summary);
                    this_pop.second->performOutgoingMovements(current_timestep, next_pending_movements, summary);
                }
            } else {
                if (!simulationParameters::sp1_dynamic_from_data) {this_pop.second->performIngoingMovements(current_timestep, current_pending_movements, summary);}
                this_pop.second->updateStochasticDynamic(current_timestep, populations, summary);
                if (!simulationParameters::sp1_dynamic_from_data) {this_pop.second->performOutgoingMovements(current_timestep, next_pending_movements, summary);}
            }
        }
		  
        #ifdef PGCOUPLING
        // HERE THE LOOP ON SP1 POP (DOMESTIC PIG) USING THE SIMULATOR IMPLETED BY FRANÇOIS
        if (simulationParameters::model_coupling_for_prediction) {
            // Update the states between previous and current timestep
				// If fence time, reset contacts
//				if(simulationParameters::T[i-1]==FENCETIME){
                if(simulationParameters::T[i]==simulationParameters::date_on_which_the_fence_is_built){
					string fencecontacts=simulationParameters::data_dir + "/network_contact_with_WB_pop_withFence.csv";
					simpg.pg.readContactsFromFile(fencecontacts);
				}

				// forward
            simpg.forward(simulationParameters::T[i-1], current_timestep);

            // HERE THE PART TO UPDATE INFORMATION ON THE DYNAMIC IN EACH "SUBMODULES" ABOUT THE NON SIMULATED POP
            for (auto& PGcurrentStates : simpg.getStatusPG(current_timestep)) {
                sirHeadcount currentSnapShot = sirHeadcount();
                currentSnapShot.update(PGcurrentStates.second, 1);

std::unordered_map<std::string,std::unique_ptr<population>>::const_iterator got = populations.find(PGcurrentStates.first);
if ( got != populations.end() ){
                populations[PGcurrentStates.first]->updateSnapshotOfTheDynamic(current_timestep, currentSnapShot, summary);
}
            }

            // Updating the wb part of simpg with pop counts
            for (const auto& pop_id : simulationData::sp2_pop_id_list) {
                simpg.wb.addSnapshotPop(current_timestep, pop_id, S,  summary.headcount_over_time[current_timestep][pop_id].getScount());
                simpg.wb.addSnapshotPop(current_timestep, pop_id, E,  summary.headcount_over_time[current_timestep][pop_id].getEcount());
                simpg.wb.addSnapshotPop(current_timestep, pop_id, Is, summary.headcount_over_time[current_timestep][pop_id].getIScount());
                simpg.wb.addSnapshotPop(current_timestep, pop_id, Ic, summary.headcount_over_time[current_timestep][pop_id].getICcount());
                simpg.wb.addSnapshotPop(current_timestep, pop_id, R,  summary.headcount_over_time[current_timestep][pop_id].getRcount());
            }

				// Adding preventive culling where WB were found
				if(current_timestep>CM_PREVENTIVE_CULLING_START){
					int count=simpg.preventiveCulling(summary.carcasses_location, current_timestep);
				}
			cout << current_timestep << endl;	
        }
        #endif



        // TODO: test which implementation is the best (probably the one above)
//        for(auto it_pop = populations.begin(); it_pop != populations.end(); ++it_pop){
//            // All this three ways do the same:
//            // 1) it_pop->second.updateStochasticDynamic(current_timestep, populations, summary);
//            // 2) populations[it_pop->first].updateStochasticDynamic(current_timestep, populations, summary);
//            // 3) std::string the_current_pop_id = it_pop->first; populations[the_current_pop_id].updateStochasticDynamic(current_timestep, populations, summary);
//
//            // receive individuals stored in the current pending movements structure
//            it_pop->second.performIngoingMovements(current_timestep, current_pending_movements, current_infected_individual_to_move, summary);
//            // update the epidemic and demographic dynamic in the current population
//            it_pop->second.updateStochasticDynamic(current_timestep, populations, summary);
//            // send individuals and store them in the next pending movements structure
//            it_pop->second.performOutgoingMovements(current_timestep, next_pending_movements, next_infected_individual_to_move, summary);
//        }
        // swap current and previous summary objects in order to get information update for previous timestep during next timestep
        summary.updateInfosOnPreviousTimestepWithCurrentTimestep();
    }

    // rebuild the contact matrixes if update due to setting up a fenced area during the dynamic
    if (simulationParameters::date_on_which_the_fence_is_built <= simulationParameters::T.back()) {
        simulationData::rebuildContactMatrix(false);
    }

    chrono_stoc_dyn_iteration.stop();
    std::cout << chrono_stoc_dyn_iteration.displayInSecond() << " seconds" << std::endl;
    return chrono_stoc_dyn_iteration.displayInSecond();
}

#ifdef PGCOUPLING
void metapopulation::save_pg(double t0, double tf){
	string file=simulationParameters::experiment_dir+"/status_over_time_"+std::to_string(rep_id)+".csv";
	simpg.pg.printPop(t0, file, true);
	for(double t=t0+PAR_DT; t<=tf; t+=PAR_DT){
		simpg.pg.printPop(t, file, false);
	}
}

void metapopulation::initSimpg(double t0, double tf, double tobs){
	 // Initial and finale times
    simpg.setTimes(-60, t0, tf, tobs, PAR_DT);

	 // Parameters : exchange risk parameter, contact risk parameter, time in E, time in Is, time in Ic, q=1
    simpg.setParameters(simulationParameters::beta_sp1_sp1_ex, simulationParameters::beta_sp2_sp1, simulationParameters::beta_sp1_sp1, PAR_TIMEE, PAR_TIMEIS, PAR_TIMEIC, PAR_TIMER, 1);
	 simpg.setControlMeasures(CM_DURPZ, CM_RADPZ, CM_DURSZ, CM_RADSZ, CM_BTDUR, CM_BTPAST);

	 // WB and PG populations charac
	simpg.pg.readPopDescFromFile2(simulationParameters::data_dir+ "/pop_characteristics.csv");
	 std::unordered_map<std::string, populationCharacteristicsUnit> map=simulationData::pop_characteristics;
    for(const auto& pop_char : simulationData::pop_characteristics){
		  string id=pop_char.first;
		  unsigned int size=pop_char.second.pop_initial_size;
		  string type=pop_char.second.pop_type;
		  if(type=="domestic_pig"){
		      //simpg.pg.addPop(id, outdoor, size);
		  } else if(type=="wild_boar"){
				// NOTE : I simply initialize the first time with the size in the S compartement
				// We will need to change this, see below
		      simpg.wb.addSnapshotPop(t0, id, S, size);
		      simpg.wb.addSnapshotPop(t0, id, E, 0);
		      simpg.wb.addSnapshotPop(t0, id, Is, 0);
		      simpg.wb.addSnapshotPop(t0, id, Ic, 0);
		      simpg.wb.addSnapshotPop(t0, id, R, 0);
		  }
	 }

	 // Contacts
    /*
    for(const auto& contact : simulationData::network_contact_with_sp2_pop){
        for(const auto& with : contact.second){
	         if(!simulationData::sp2_pop_id_list.count(contact.first)){
					simpg.pg.addContact(contact.first, with);
					simpg.wb.addContactDP(with, contact.first);
				}
		  }
	 }
     */

	string contactfile=simulationParameters::data_dir + "/network_contact_with_WB_pop.csv";
	simpg.pg.readContactsFromFile(contactfile);
	simpg.pg.readLocalContactsFromFile(simulationParameters::data_dir + "/network_local_contact_DP_DP.csv");

	 // Movements
	 for(const auto& it1 : simulationData::mvts_network){
	     for(const auto& it2 : it1.second){
	         for(const auto& it3 : it2.second){
	             simpg.pg.addSnapshotExchange(it2.first, it1.first, it3.first, it3.second);
	             simpg.pg.addSnapshotExport(it2.first, it3.first, it1.first, it3.second);
				}
	     }
	 }

	 // Initial state
	 // Get suspicion times
	 string suspfile=simulationParameters::data_dir + "/susp_time_DP.csv";
	 string pcullfile=simulationParameters::data_dir + "/preventive_cull_time_DP.csv";
	 simpg.fillWithObs(simpg.readObsVec(suspfile), simpg.readObsVec(pcullfile));
}
#endif
