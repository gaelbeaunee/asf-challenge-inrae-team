#ifndef POPPG_H
#define POPPG_H

#include <vector>
#include <unordered_map>
#include <random>

#include "status.hpp"
#include "snapshotpg.hpp"

// TODO : add support for pg contact file

class PopPG {
private:
	std::unordered_map<double, snapshotpg*> snapshot;	
	std::unordered_map<string, herdtype> type;	
	std::unordered_map<string, unsigned int> size;	
	std::unordered_map<string, vector<string>> contacts;	
	std::unordered_map<string, vector<string>> localContacts;	
	std::unordered_map<string, double> posx;	
	std::unordered_map<string, double> posy;	
	std::unordered_map<string, int> timeE;	
	std::unordered_map<string, int> timeIs;	
	std::default_random_engine generator;

	int sampleTimeE();
	int sampleTimeIs();

public:
	void addContact(string pg, string wb);
	void addLocalContact(string pg, string pgcontact);
	void addLocalContacts(double maxdist);
	void addPop(string id, herdtype type, unsigned int size);
	void addPop(string id, herdtype type, unsigned int size, double x, double y);
	void addSnapshot(double t);
	void addSnapshotPop(double t, string id, status st, unsigned int si);
	void addSnapshotExchange(double t, string id, string wb, unsigned int weight);
	void addSnapshotExport(double t, string id, string wb, unsigned int weight);
	void setClock(double t, string id, unsigned int clock);
	unsigned int getClock(double t, string id);
	void readPopDescFromFile(string file);
	void readPopDescFromFile2(string file);
	void readContactsFromFile(string file);
	void readLocalContactsFromFile(string file);
	void readExchangesFromFile(string file);
	void readPopFromFile(string file);
	void printPop(double t, string file, bool reset);
	unsigned int getSize(string id, double t);
	void setSize(string id, double t, unsigned int size);

	herdtype getType(string id);
	vector<string> getContacts(string id);
	vector<string> getLocalContacts(string id);
	vector<string> getIds(double t);
	status getStatus(string id, double t);
	unsigned int getSize(string id);
	unordered_map<string, unsigned int> getExchanges(string id, double t);
	unordered_map<string, unsigned int> getExports(string id, double t);
	unordered_map<string, herdtype> getType();
	bool isSet(string id, double t);
	void updateExchanges(double t);
	bool isBanned(string id, double t);
	bool isPZ(string id, double t);

	void addPZ(string id, double t, double rad);
	void addSZ(string id, double t, double rad);
	void addBacktrace(string id, double t, double pastdur, double dur, double dt);

	int getTimeE(string id);
	int getTimeIs(string id);
	void sampleStatusTimes(string id);
	void newStatusTimes();
	void setInfectionProbability(double t, string id, double p);
	double getInfectionProbability(double t, string id);

	double getX(string id);
	double getY(string id);
};

#endif
