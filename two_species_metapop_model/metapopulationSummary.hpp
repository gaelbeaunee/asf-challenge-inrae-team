//
//  metapopulationSummary.hpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#ifndef metapopulationSummary_hpp
#define metapopulationSummary_hpp

#include <stdio.h>
#include <string>
#include <set>
#include <unordered_map>
#include <algorithm>
#include <vector>
#include <armadillo>

#include "simulationParameters.hpp"
#include "simulationData.hpp"
#include "sirHeadcount.hpp"


class metapopulationSummary {
public:
    // Constructor
    metapopulationSummary();

    // Variables
    std::unordered_map<double, std::unordered_map<std::string, sirHeadcount>> headcount_over_time;
    std::unordered_map<double, std::unordered_map<std::string, std::unordered_map<std::string, int>>> events_over_time;
    std::unordered_map<std::string, std::unordered_map<double, std::unordered_map<std::string, int>>> mvts_not_performed;
    std::vector<double> headcount_I_previous_timestep;
    std::vector<double> headcount_I_current_timestep;
    std::vector<double> headcount_IS_previous_timestep;
    std::vector<double> headcount_IS_current_timestep;
    std::vector<double> headcount_IC_previous_timestep;
    std::vector<double> headcount_IC_current_timestep;
    std::vector<double> headcount_D_previous_timestep;
    std::vector<double> headcount_D_current_timestep;
    std::vector<double> headcount_N_previous_timestep;
    std::vector<double> headcount_N_current_timestep;
    std::vector<double> headcount_I_N_previous_timestep;
    std::vector<double> headcount_I_N_current_timestep;
    std::vector<double> headcount_D_N_previous_timestep;
    std::vector<double> headcount_D_N_current_timestep;

    std::vector<double> sum_I_neighbour_sp1_previous_timestep;
    std::vector<double> sum_IS_neighbour_sp1_previous_timestep;
    std::vector<double> sum_IC_neighbour_sp1_previous_timestep;
    std::vector<double> sum_N_neighbour_sp1_previous_timestep;
    std::vector<double> sum_D_neighbour_sp1_previous_timestep;
    std::vector<double> sum_I_neighbour_sp2_previous_timestep;
    std::vector<double> sum_IS_neighbour_sp2_previous_timestep;
    std::vector<double> sum_IC_neighbour_sp2_previous_timestep;
    std::vector<double> sum_N_neighbour_sp2_previous_timestep;
    std::vector<double> sum_D_neighbour_sp2_previous_timestep;

    std::unordered_map<double, std::unordered_map<std::string, std::unordered_map<std::string, double>>> rho_over_time;

    std::unordered_map<double, std::unordered_map<std::string, std::vector<std::unordered_map<std::string, double>>>> carcasses_location;

    // Member functions
    void save_headcount_over_time(const int& rep_id);
    void save_events_over_time(const int& rep_id);
    void save_mvts_not_performed(const int& rep_id);
    void save_rho_over_time(const int& rep_id);
    void save_carcasses_location(const int& rep_id);
    void updateInfosOnPreviousTimestepWithCurrentTimestep();
    void computeTermsOfFOI();
};

#endif /* metapopulationSummary_hpp */
