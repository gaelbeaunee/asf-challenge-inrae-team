//
//  metapopulationSummary.cpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#include "metapopulationSummary.hpp"

// Constructor
metapopulationSummary::metapopulationSummary(){
    for (const auto& t : simulationParameters::T) {
        headcount_over_time.insert(std::pair< double, std::unordered_map<std::string, sirHeadcount> >(t, std::unordered_map<std::string, sirHeadcount>()));
        events_over_time.insert(std::pair< double, std::unordered_map<std::string, std::unordered_map<std::string, int>> >(t, std::unordered_map<std::string, std::unordered_map<std::string, int>>()));
        rho_over_time.insert(std::pair< double, std::unordered_map<std::string, std::unordered_map<std::string, double>> >(t, std::unordered_map<std::string, std::unordered_map<std::string, double>>()));
        carcasses_location.insert(std::pair< double, std::unordered_map<std::string, std::vector<std::unordered_map<std::string, double>>> >(t, std::unordered_map<std::string, std::vector<std::unordered_map<std::string, double>>>()));
        for (const auto& pop_id : simulationData::sp1_pop_id_list) {
            headcount_over_time[t].insert(std::pair<std::string, sirHeadcount > (pop_id, sirHeadcount()));
        }
        for (const auto& pop_id : simulationData::sp2_pop_id_list) {
            headcount_over_time[t].insert(std::pair<std::string, sirHeadcount > (pop_id, sirHeadcount()));
        }
    }
    headcount_I_previous_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_I_current_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_IS_previous_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_IS_current_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_IC_previous_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_IC_current_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_N_previous_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_N_current_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_D_previous_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_D_current_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_I_N_previous_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_I_N_current_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_D_N_previous_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_D_N_current_timestep.resize(simulationData::all_pop_id_list.size(),0);
}

// Member functions
void metapopulationSummary::save_headcount_over_time(const int& rep_id){
    Chronometer chrono_save_headcount_over_time;
    chrono_save_headcount_over_time.start();
    if (simulationParameters::verbose_mode) { std::cout << "\tsaving headcount_over_time" << std::to_string(rep_id) << "... "; }
    // create a directory for the experiment if it does not exist
    auxFunctions::createDirectory(simulationParameters::experiment_dir);
    // create list of states for which dynamic over time will be saved
    std::vector<std::string> v_state_list = {"S", "E", "Is", "Ic", "D", "R", "N"};
    //
    // loop on the v_state_list item
    for (const auto& the_state : v_state_list) {
        // define the filename and the file path
//        std::string the_result_file_path = simulationParameters::experiment_dir + '/' + "headcount_over_time_" + the_state + "_" + std::to_string(simulationParameters::random_engine_seed) + "_rep_" + std::to_string(rep_id) + ".csv";
        std::string the_result_file_path = simulationParameters::experiment_dir + '/' + "headcount_over_time_" + the_state + "_rep_" + std::to_string(rep_id) + ".csv";
        // open file in write mode
        std::ofstream ost_headcount_over_time_X(the_result_file_path, std::fstream::out);
        if(!ost_headcount_over_time_X){std::cerr << "[ERROR] can't open output file for headcount_over_time_" << the_state << "!" << std::endl;}
        // write the header
        ost_headcount_over_time_X << "pop_id";
        for (const auto& t : simulationParameters::T) {
            std::string the_timestep = std::to_string(t);
            // std::replace(the_timestep.begin(), the_timestep.end(), '.', '_');
            ost_headcount_over_time_X << "," << the_timestep;
        }
        ost_headcount_over_time_X << "\n";
        // write the data
        // sp1
        if (simulationParameters::save_sp1_dynamic) {
            for (const auto& pop_id : simulationData::sp1_pop_id_list) {
                ost_headcount_over_time_X << pop_id;
                for (const auto& t : simulationParameters::T) {
                    if (the_state == "N") {
                        ost_headcount_over_time_X << "," << headcount_over_time[t][pop_id].sum();
                    } else {
                        ost_headcount_over_time_X << "," << headcount_over_time[t][pop_id].get(the_state);
                    }
                }
                ost_headcount_over_time_X << "\n";
            }
        }
        // sp2
        if (simulationParameters::save_sp2_dynamic) {
            for (const auto& pop_id : simulationData::sp2_pop_id_list) {
                ost_headcount_over_time_X << pop_id;
                for (const auto& t : simulationParameters::T) {
                    if (the_state == "N") {
                        ost_headcount_over_time_X << "," << headcount_over_time[t][pop_id].sum();
                    } else {
                        ost_headcount_over_time_X << "," << headcount_over_time[t][pop_id].get(the_state);
                    }
                }
                ost_headcount_over_time_X << "\n";
            }
        }
    }
    //
    // second format: list of pop_id, date, status, size
    // sp1
    if (simulationParameters::save_sp1_dynamic) {
        // define the filename and the file path
//        std::string the_result_file_path = simulationParameters::experiment_dir + '/' + simulationParameters::sp1_name + "_" + "SEIsIcDRN_dynamic" + "_" + std::to_string(simulationParameters::random_engine_seed) + "_rep_" + std::to_string(rep_id) + ".csv";
        std::string the_result_file_path = simulationParameters::experiment_dir + '/' + simulationParameters::sp1_name + "_" + "SEIsIcDRN_dynamic_rep_" + std::to_string(rep_id) + ".csv";
        // open file in write mode
        std::ofstream ost_sp1_dynamic_as_a_list_of_popid_date_status_size(the_result_file_path, std::fstream::out);
        if(!ost_sp1_dynamic_as_a_list_of_popid_date_status_size){std::cerr << "[ERROR] can't open output file for " << simulationParameters::sp1_name + "_" + "SEIsIcDRN_dynamic" << "!" << std::endl;}
        // write the header
        ost_sp1_dynamic_as_a_list_of_popid_date_status_size << "pop_id,date,status,size" << "\n";
        // write the data
        for (const auto& pop_id : simulationData::sp1_pop_id_list) {
            for (const auto& t : simulationParameters::T) {
                for (const auto& the_state : v_state_list) {
                    if (the_state == "N") {
                        ost_sp1_dynamic_as_a_list_of_popid_date_status_size << pop_id << "," << t << "," << the_state << "," << headcount_over_time[t][pop_id].sum() << "\n";
                    } else {
                        ost_sp1_dynamic_as_a_list_of_popid_date_status_size << pop_id << "," << t << "," << the_state << "," << headcount_over_time[t][pop_id].get(the_state) << "\n";
                    }
                }
            }
        }
    }
    // sp2
    if (simulationParameters::save_sp2_dynamic) {
        // define the filename and the file path
//        std::string the_result_file_path = simulationParameters::experiment_dir + '/' + simulationParameters::sp2_name + "_" + "SEIsIcDRN_dynamic" + "_" + std::to_string(simulationParameters::random_engine_seed) + "_rep_" + std::to_string(rep_id) + ".csv";
        std::string the_result_file_path = simulationParameters::experiment_dir + '/' + simulationParameters::sp2_name + "_" + "SEIsIcDRN_dynamic_rep_" + std::to_string(rep_id) + ".csv";
        // open file in write mode
        std::ofstream ost_sp2_dynamic_as_a_list_of_popid_date_status_size(the_result_file_path, std::fstream::out);
        if(!ost_sp2_dynamic_as_a_list_of_popid_date_status_size){std::cerr << "[ERROR] can't open output file for " << simulationParameters::sp2_name + "_" + "SEIsIcDRN_dynamic" << "!" << std::endl;}
        // write the header
        ost_sp2_dynamic_as_a_list_of_popid_date_status_size << "pop_id,date,status,size" << "\n";
        // write the data
        for (const auto& pop_id : simulationData::sp2_pop_id_list) {
            for (const auto& t : simulationParameters::T) {
                for (const auto& the_state : v_state_list) {
                    if (the_state == "N") {
                        ost_sp2_dynamic_as_a_list_of_popid_date_status_size << pop_id << "," << t << "," << the_state << "," << headcount_over_time[t][pop_id].sum() << "\n";
                    } else {
                        ost_sp2_dynamic_as_a_list_of_popid_date_status_size << pop_id << "," << t << "," << the_state << "," << headcount_over_time[t][pop_id].get(the_state) << "\n";
                    }
                }
            }
        }
    }

    //
    chrono_save_headcount_over_time.stop();
    if (simulationParameters::verbose_mode) { std::cout << chrono_save_headcount_over_time.displayInSecond() << " seconds" << std::endl; }
}

void metapopulationSummary::save_events_over_time(const int& rep_id){
    Chronometer chrono_save_events_over_time;
    chrono_save_events_over_time.start();
    if (simulationParameters::verbose_mode) { std::cout << "\tsaving events_over_time" << std::to_string(rep_id) << "... "; }
    // create a directory for the experiment if it does not exist
    auxFunctions::createDirectory(simulationParameters::experiment_dir);
    // create list of states for which dynamic over time will be saved
    std::vector<std::string> v_events_list = {"infection_thru_I", "infection_thru_D", "inf_contact_sp1", "inf_contact_sp2_I", "inf_contact_sp2_D", "start_shedding", "onset_of_clinical_signs", "death", "D_no_more_infectious", "carcass_discovery_and_removal_by_passive_surveillance", "carcass_discovery_and_removal_by_active_search", "hunt_of_an_S", "hunt_of_an_E", "hunt_of_an_Is", "hunt_of_an_Ic", "test_on_hunted_SEIsIc", "positive_test_on_hunted_SEIsIc"};
    //
    // sp1
    if (simulationParameters::save_sp1_dynamic) {
        // define the filename and the file path
//        std::string the_result_file_path = simulationParameters::experiment_dir + '/' + simulationParameters::sp1_name + "_" + "events_dynamic" + "_" + std::to_string(simulationParameters::random_engine_seed) + "_rep_" + std::to_string(rep_id) + ".csv";
        std::string the_result_file_path = simulationParameters::experiment_dir + '/' + simulationParameters::sp1_name + "_" + "events_dynamic_rep_" + std::to_string(rep_id) + ".csv";
        // open file in write mode
        std::ofstream ost_sp1_dynamic_as_a_list_of_popid_date_events(the_result_file_path, std::fstream::out);
        if(!ost_sp1_dynamic_as_a_list_of_popid_date_events){std::cerr << "[ERROR] can't open output file for " << simulationParameters::sp1_name + "_" + "events_over_time" << "!" << std::endl;}
        // write the header
        // write the header
        ost_sp1_dynamic_as_a_list_of_popid_date_events << "pop_id,date";
        for (const auto& the_event : v_events_list) {
            ost_sp1_dynamic_as_a_list_of_popid_date_events << "," << the_event;
        }
        ost_sp1_dynamic_as_a_list_of_popid_date_events << "\n";
        // write the data
        for (const auto& pop_id : simulationData::sp1_pop_id_list) {
            for (const auto& t : simulationParameters::T) {
                ost_sp1_dynamic_as_a_list_of_popid_date_events << pop_id << "," << t;
                for (const auto& the_event : v_events_list) {
                    ost_sp1_dynamic_as_a_list_of_popid_date_events << "," << events_over_time[t][pop_id][the_event];
                }
                ost_sp1_dynamic_as_a_list_of_popid_date_events << "\n";
            }
        }
    }
    // sp2
    if (simulationParameters::save_sp2_dynamic) {
        // define the filename and the file path
//        std::string the_result_file_path = simulationParameters::experiment_dir + '/' + simulationParameters::sp2_name + "_" + "events_dynamic" + "_" + std::to_string(simulationParameters::random_engine_seed) + "_rep_" + std::to_string(rep_id) + ".csv";
        std::string the_result_file_path = simulationParameters::experiment_dir + '/' + simulationParameters::sp2_name + "_" + "events_dynamic_rep_" + std::to_string(rep_id) + ".csv";
        // open file in write mode
        std::ofstream ost_sp2_dynamic_as_a_list_of_popid_date_events(the_result_file_path, std::fstream::out);
        if(!ost_sp2_dynamic_as_a_list_of_popid_date_events){std::cerr << "[ERROR] can't open output file for " << simulationParameters::sp2_name + "_" + "events_over_time" << "!" << std::endl;}
        // write the header
        // write the header
        ost_sp2_dynamic_as_a_list_of_popid_date_events << "pop_id,date";
        for (const auto& the_event : v_events_list) {
            ost_sp2_dynamic_as_a_list_of_popid_date_events << "," << the_event;
        }
        ost_sp2_dynamic_as_a_list_of_popid_date_events << "\n";
        // write the data
        for (const auto& pop_id : simulationData::sp2_pop_id_list) {
            for (const auto& t : simulationParameters::T) {
                ost_sp2_dynamic_as_a_list_of_popid_date_events << pop_id << "," << t;
                for (const auto& the_event : v_events_list) {
                    ost_sp2_dynamic_as_a_list_of_popid_date_events << "," << events_over_time[t][pop_id][the_event];
                    
//                    if (t>simulationParameters::last_date_of_hunting_period) {
//                        if ((the_event == "hunt_of_an_S") & (events_over_time[t][pop_id]["hunt_of_an_S"] > 0)) {
//                            std::cout << t << " - " << pop_id << " - " << the_event << " - " << events_over_time[t][pop_id]["hunt_of_an_S"] << std::endl; // DEBUG
//                        }
//                        if ((the_event == "hunt_of_an_E") & (events_over_time[t][pop_id]["hunt_of_an_E"] > 0)) {
//                            std::cout << t << " - " << pop_id << " - " << the_event << " - " << events_over_time[t][pop_id]["hunt_of_an_E"] << std::endl; // DEBUG
//                        }
//                        if ((the_event == "hunt_of_an_Is") & (events_over_time[t][pop_id]["hunt_of_an_Is"] > 0)) {
//                            std::cout << t << " - " << pop_id << " - " << the_event << " - " << events_over_time[t][pop_id]["hunt_of_an_Is"] << std::endl; // DEBUG
//                        }
//                        if ((the_event == "hunt_of_an_Ic") & (events_over_time[t][pop_id]["hunt_of_an_Ic"] > 0)) {
//                            std::cout << t << " - " << pop_id << " - " << the_event << " - " << events_over_time[t][pop_id]["hunt_of_an_Ic"] << std::endl; // DEBUG
//                        }
//                    }

                }
                ost_sp2_dynamic_as_a_list_of_popid_date_events << "\n";
            }
        }
    }
    //
    chrono_save_events_over_time.stop();
    if (simulationParameters::verbose_mode) { std::cout << chrono_save_events_over_time.displayInSecond() << " seconds" << std::endl; }
}

void metapopulationSummary::save_mvts_not_performed(const int& rep_id){

}

void metapopulationSummary::save_rho_over_time(const int& rep_id){
    Chronometer chrono_save_rho_over_time;
    chrono_save_rho_over_time.start();
    if (simulationParameters::verbose_mode) { std::cout << "\tsaving rho_over_time" << std::to_string(rep_id) << "... "; }
    // create a directory for the experiment if it does not exist
    auxFunctions::createDirectory(simulationParameters::experiment_dir);
    // create list of states for which dynamic over time will be saved
    std::vector<std::string> v_rho_list = {"rho_calibrated_on_data", "rho_passive_search_inside_fenced_buffer_area_calibrated_on_data", "rho_active_search_calibrated_on_data", "carcass_discovery_by_passive_surveillance_calibrated_on_data", "carcass_discovery_by_active_search_calibrated_on_data", "nb_active_search_calibrated_on_data"};

    // define the filename and the file path
//        std::string the_result_file_path = simulationParameters::experiment_dir + '/' + simulationParameters::sp2_name + "_" + "calibrated_rho" + "_" + std::to_string(simulationParameters::random_engine_seed) + "_rep_" + std::to_string(rep_id) + ".csv";
    std::string the_result_file_path = simulationParameters::experiment_dir + '/' + simulationParameters::sp2_name + "_" + "calibrated_rho_rep_" + std::to_string(rep_id) + ".csv";
    // open file in write mode
    std::ofstream ost_sp2_dynamic_as_a_list_of_popid_date_rho(the_result_file_path, std::fstream::out);
    if(!ost_sp2_dynamic_as_a_list_of_popid_date_rho){std::cerr << "[ERROR] can't open output file for " << simulationParameters::sp2_name + "_" + "calibrated_rho" << "!" << std::endl;}
    // write the header
    // write the header
    ost_sp2_dynamic_as_a_list_of_popid_date_rho << "pop_id,date";
    for (const auto& the_rho : v_rho_list) {
        ost_sp2_dynamic_as_a_list_of_popid_date_rho << "," << the_rho;
    }
    ost_sp2_dynamic_as_a_list_of_popid_date_rho << "\n";
    // write the data
    for (const auto& pop_id : simulationData::sp2_pop_id_list) {
        for (const auto& t : simulationParameters::T) {
            ost_sp2_dynamic_as_a_list_of_popid_date_rho << pop_id << "," << t;
            for (const auto& the_rho : v_rho_list) {
                ost_sp2_dynamic_as_a_list_of_popid_date_rho << "," << rho_over_time[t][pop_id][the_rho];
            }
            ost_sp2_dynamic_as_a_list_of_popid_date_rho << "\n";
        }
    }
    //
    chrono_save_rho_over_time.stop();
    if (simulationParameters::verbose_mode) { std::cout << chrono_save_rho_over_time.displayInSecond() << " seconds" << std::endl; }
}

void metapopulationSummary::save_carcasses_location(const int& rep_id){
    Chronometer chrono_save_carcasses_location;
    chrono_save_carcasses_location.start();
    if (simulationParameters::verbose_mode) { std::cout << "\tsaving carcasses_location" << std::to_string(rep_id) << "... "; }
    // create a directory for the experiment if it does not exist
    auxFunctions::createDirectory(simulationParameters::experiment_dir);
    // define the filename and the file path
//        std::string the_result_file_path = simulationParameters::experiment_dir + '/' + simulationParameters::sp2_name + "_" + "carcasses_location" + "_" + std::to_string(simulationParameters::random_engine_seed) + "_rep_" + std::to_string(rep_id) + ".csv";
    std::string the_result_file_path = simulationParameters::experiment_dir + '/' + simulationParameters::sp2_name + "_" + "carcasses_location_rep_" + std::to_string(rep_id) + ".csv";
    // open file in write mode
    std::ofstream ost_sp2_dynamic_as_a_list_of_date_popid_carcasseslocation(the_result_file_path, std::fstream::out);
    if(!ost_sp2_dynamic_as_a_list_of_date_popid_carcasseslocation){std::cerr << "[ERROR] can't open output file for " << simulationParameters::sp2_name + "_" + "carcasses_location" << "!" << std::endl;}
    // write the header
    // write the header
    ost_sp2_dynamic_as_a_list_of_date_popid_carcasseslocation << "date,pop_id,coord_X,coord_Y" << "\n";
    // write the data
    for (const auto& date_item : carcasses_location) {
        double current_date = date_item.first;
        for (const auto& pop_item : date_item.second) {
            std::string pop_id = pop_item.first;
            for (std::unordered_map<std::string, double> carcasses_location_item : pop_item.second) {
                double X = carcasses_location_item["x_coord"];
                double Y = carcasses_location_item["y_coord"];
                ost_sp2_dynamic_as_a_list_of_date_popid_carcasseslocation << std::fixed << current_date << "," << pop_id << "," << X << "," << Y << "\n";
            }
        }
    }
    //
    chrono_save_carcasses_location.stop();
    if (simulationParameters::verbose_mode) { std::cout << chrono_save_carcasses_location.displayInSecond() << " seconds" << std::endl; }
}

void metapopulationSummary::updateInfosOnPreviousTimestepWithCurrentTimestep(){
    headcount_I_previous_timestep.swap(headcount_I_current_timestep);
    headcount_I_current_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_IS_previous_timestep.swap(headcount_IS_current_timestep);
    headcount_IS_current_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_IC_previous_timestep.swap(headcount_IC_current_timestep);
    headcount_IC_current_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_N_previous_timestep.swap(headcount_N_current_timestep);
    headcount_N_current_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_D_previous_timestep.swap(headcount_D_current_timestep);
    headcount_D_current_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_I_N_previous_timestep.swap(headcount_I_N_current_timestep);
    headcount_I_N_current_timestep.resize(simulationData::all_pop_id_list.size(),0);
    headcount_D_N_previous_timestep.swap(headcount_D_N_current_timestep);
    headcount_D_N_current_timestep.resize(simulationData::all_pop_id_list.size(),0);
}

void metapopulationSummary::computeTermsOfFOI(){
    // compute the sum of headcount for foi calculation (using armadillo matrix multiplication)
    if (simulationParameters::density_dependent_tranmission_betweenpop_I) { // density dependent tranmission betweenpop -> I
        arma::mat I = arma::conv_to<arma::mat>::from(headcount_I_previous_timestep);
        sum_I_neighbour_sp1_previous_timestep = arma::conv_to< std::vector<double> >::from(simulationData::mat_contact_sp1_pop*I);
        sum_I_neighbour_sp2_previous_timestep = arma::conv_to< std::vector<double> >::from(simulationData::mat_contact_sp2_pop*I);
    } else { // frequency dependent tranmission betweenpop -> I/N
        arma::mat I_N = arma::conv_to<arma::mat>::from(headcount_I_N_previous_timestep);
        sum_I_neighbour_sp1_previous_timestep = arma::conv_to< std::vector<double> >::from(simulationData::mat_contact_sp1_pop*I_N);
        sum_I_neighbour_sp2_previous_timestep = arma::conv_to< std::vector<double> >::from(simulationData::mat_contact_sp2_pop*I_N);
    }
    //
    if (simulationParameters::density_dependent_tranmission_betweenpop_D) { // density dependent tranmission betweenpop -> I
        arma::mat D = arma::conv_to<arma::mat>::from(headcount_D_previous_timestep);
//        sum_D_neighbour_sp1_previous_timestep = arma::conv_to< std::vector<double> >::from(simulationData::mat_contact_sp1_pop*D); // WARNING: commented to save computation time because not used
        sum_D_neighbour_sp2_previous_timestep = arma::conv_to< std::vector<double> >::from(simulationData::mat_contact_sp2_pop*D);
    } else { // frequency dependent tranmission betweenpop -> I/N
        arma::mat D_N = arma::conv_to<arma::mat>::from(headcount_D_N_previous_timestep);
//        sum_D_neighbour_sp1_previous_timestep = arma::conv_to< std::vector<double> >::from(simulationData::mat_contact_sp1_pop*D_N); // WARNING: commented to save computation time because not used
        sum_D_neighbour_sp2_previous_timestep = arma::conv_to< std::vector<double> >::from(simulationData::mat_contact_sp2_pop*D_N);
    }
//    arma::mat N = arma::conv_to<arma::mat>::from(headcount_N_previous_timestep);
//    sum_N_neighbour_sp1_previous_timestep = arma::conv_to< std::vector<int> >::from(simulationData::mat_contact_sp1_pop*N);
//    sum_N_neighbour_sp2_previous_timestep = arma::conv_to< std::vector<int> >::from(simulationData::mat_contact_sp2_pop*N);

}

