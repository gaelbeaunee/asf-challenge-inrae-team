//
//  sirHeadcount.hpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#ifndef sirHeadcount_hpp
#define sirHeadcount_hpp

#include <stdio.h>
#include <string>
#include <unordered_map>
#include <vector>
#include <random>
#include <iterator>
#include <algorithm>

#include "simulationParameters.hpp"
#include "auxiliaryFunctions.hpp"

//using indivCountType = unsigned short int; // Wrong type as negative values can be used
using indivCountType = short int; // WARNING: could be inappropriate if the population size is too large

struct sirHeadcount {
    // Constructor
    sirHeadcount();
    sirHeadcount(const indivCountType& _count_S, const indivCountType& _count_E, const indivCountType& _count_Is, const indivCountType& _count_Ic, const indivCountType& _count_D, const indivCountType& _count_R);

    // Variables
    indivCountType S = 0;
    indivCountType E = 0;
    indivCountType Is = 0;
    indivCountType Ic = 0;
    indivCountType D = 0;
    indivCountType R = 0;

    // Member functions
    int get(const std::string& _states);
    int getScount() {return S;}
    int getEcount() {return E;}
    int getIScount() {return Is;}
    int getICcount() {return Ic;}
    int getIcount() {return Is+Ic;}
    int getDcount() {return D;}
    int getRcount() {return R;}

    void update(const std::string& _states, const indivCountType& _count);
    void update(const indivCountType& _count_S, const indivCountType& _count_E, const indivCountType& _count_Is, const indivCountType& _count_Ic, const indivCountType& _count_D, const indivCountType& _count_R);
    void update(const std::unordered_map<std::string, indivCountType>& _sir_map_for_updating);
    void update(const sirHeadcount& _sirHeadcount_for_updating);
    void replace(const sirHeadcount& _sirHeadcount_for_updating);
    int sum();
    void inverse();
    std::vector<std::string> getDisaggregatedListOfHealthStates();
    std::vector<std::string> getDisaggregatedListOfHealthStates_SEIsIc();
    sirHeadcount selectXHealthStatesRandomly(const indivCountType& _count);
    std::vector<std::string> getNegativeStates();
    void show() const {std::cout << S << ", " << E << ", " << Is << ", " << Ic << ", " << D << ", " << R << std::endl;}
};

struct sirHeadcount_map {
    // Constructor
    sirHeadcount_map();
    sirHeadcount_map(const indivCountType& _count_S, const indivCountType& _count_I, const indivCountType& _count_R);

    // Variables
    std::unordered_map<std::string, indivCountType> headcount = {{"S",0},{"E",0},{"Is",0},{"Ic",0},{"D",0},{"R",0}};

    // Member functions
    int get(const std::string& _states) {return headcount[_states];}
    int getScount() {return headcount["S"];}
    int getEcount() {return headcount["E"];}
    int getIScount() {return headcount["Is"];}
    int getICcount() {return headcount["Ic"];}
    int getIcount() {return headcount["Is"]+headcount["Ic"];}
    int getDcount() {return headcount["D"];}
    int getRcount() {return headcount["R"];}

    void update(const std::string& _states, const indivCountType& _count);
    void update(const indivCountType& _count_S, const indivCountType& _count_I, const indivCountType& _count_R);
    void update(const std::unordered_map<std::string, indivCountType>& _sir_map_for_updating);
    void update(const sirHeadcount_map& _sirHeadcount_for_updating);
    int sum();
    void inverse();
    std::vector<std::string> getDisaggregatedListOfHealthStates();
    sirHeadcount_map selectXHealthStatesRandomly(const indivCountType& _count);
    std::vector<std::string> getNegativeStates();
};

#endif /* sirHeadcount_hpp */
