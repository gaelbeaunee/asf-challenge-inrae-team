# ******NOTICE***************
# Based on optimize.py module by Travis E. Oliphant
#
# You may copy and use this module as you see fit with no
# guarantee implied provided you keep this notice in all copies.
# *****END NOTICE************

# CHANGES:
# Update value of psi and sigma according to [ADD REF]
# Possibility to periodicly relaunch the simplex from a fresh start based on last best iteration
# A function to save the results during optimisation
# Possibility to constrain the parameter definition range

from __future__ import division, print_function, absolute_import

import warnings
# import sys
import numpy
# import math
from numpy import (asfarray)
import numpy as np

import inference_WB_functions as myfnctns
import __main__


###########################################################################
fcalls_sincelastimprovement = 0
fsim_best = None
sim_best = None
###########################################################################


# standard status messages of optimizers
_status_message = {'success': 'Optimization terminated successfully.',
                   'maxfev': 'Maximum number of function evaluations has '
                              'been exceeded.',
                   'maxiter': 'Maximum number of iterations has been '
                              'exceeded.',
                   'maximprov': 'Maximum number of function evaluations since '
                              'last improvement has been exceeded.',
                   'maxfevlast': 'Maximum number of function evaluations allowed '
                              'for the last run has been exceeded.',
                   'pr_loss': 'Desired error not necessarily achieved due '
                              'to precision loss.'}


class OptimizeResult(dict):
    """Represents the optimization result.

    Attributes
    ----------
    x : ndarray
        The solution of the optimization.
    success : bool
        Whether or not the optimizer exited successfully.
    status : int
        Termination status of the optimizer. Its value depends on the
        underlying solver. Refer to `message` for details.
    message : str
        Description of the cause of the termination.
    fun, jac, hess: ndarray
        Values of objective function, its Jacobian and its Hessian (if
        available). The Hessians may be approximations, see the documentation
        of the function in question.
    hess_inv : object
        Inverse of the objective function's Hessian; may be an approximation.
        Not available for all solvers. The type of this attribute may be
        either np.ndarray or scipy.sparse.linalg.LinearOperator.
    nfev, njev, nhev : int
        Number of evaluations of the objective functions and of its
        Jacobian and Hessian.
    nit : int
        Number of iterations performed by the optimizer.
    maxcv : float
        The maximum constraint violation.
    Notes
    -----
    There may be additional attributes not listed above depending of the
    specific solver. Since this class is essentially a subclass of dict
    with attribute accessors, one can see which attributes are available
    using the `keys()` method.

    """

    def __getattr__(self, name):
        """__getattr__."""
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)

    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

    def __repr__(self):
        """__repr__."""
        if self.keys():
            m = max(map(len, list(self.keys()))) + 1
            return '\n'.join([k.rjust(m) + ': ' + repr(v)
                              for k, v in sorted(self.items())])
        else:
            return self.__class__.__name__ + "()"

    def __dir__(self):
        """__dir__."""
        return list(self.keys())


class OptimizeWarning(UserWarning):
    pass


def _check_unknown_options(unknown_options):
    if unknown_options:
        msg = ", ".join(map(str, unknown_options.keys()))
        # Stack level 4: this is called from _minimize_*, which is
        # called from another function in Scipy. Level 4 is the first
        # level in user code.
        warnings.warn("Unknown solver options: %s" % msg, OptimizeWarning, 4)


def wrap_function(function, args):
    ncalls = [0]
    if function is None:
        return ncalls, None

    def function_wrapper(*wrapper_args):
        ncalls[0] += 1
        return function(*(wrapper_args + args))

    return ncalls, function_wrapper


def writeResults(the_iterations, the_fcalls, the_currentParticle,
                 the_particles, the_currentParticle_criterion,
                 the_particles_criteria, the_movement,
                 the_ResumeFilePath_parameters):
    """
    Write results in differents files.

    This function provides facilities to write in the results files.
    """
    vCritSupZero = [the_particles_criteria[i] for i in range(len(the_particles_criteria)) if the_particles_criteria[i] > 0.0]
    index_best = [i for i in range(len(the_particles_criteria)) if the_particles_criteria[i] == min(vCritSupZero)]
    index_best = index_best[0]

    index_worst = [i for i in range(len(the_particles_criteria)) if the_particles_criteria[i] == max(the_particles_criteria)]
    index_worst = index_worst[0]

    theBestCrit = min(the_currentParticle_criterion, the_particles_criteria[index_best])
    theWorstCrit = the_particles_criteria[index_worst]

    with open(the_ResumeFilePath_parameters, 'a') as theResumeFile:
        theResumeFile.write(str(the_iterations) + "\t" + str(the_fcalls[0]) + "\t" +
                            '\t'.join([str(myfnctns.fromSimplexValToParamVal(the_currentParticle[i], __main__.minVal[i], __main__.maxVal[i])) for i in range(len(the_currentParticle))]) + '\t' +
                            str(the_currentParticle_criterion) + "\t" +
                            str(theBestCrit) + "\t" +
                            str(theWorstCrit) + '\t' +
                            the_movement + '\n')


def rescaleParameters(vParam):
    rescaledParam = vParam.copy()
    for i in range(len(rescaledParam)):
        rescaledParam[i] = myfnctns.fromSimplexValToParamVal(rescaledParam[i], __main__.minVal[i], __main__.maxVal[i])
    return rescaledParam


def check_improvement(the_fsim, the_sim, the_accuracy):
    global fcalls_sincelastimprovement, fsim_best, sim_best
    ###########################################################################
    if the_fsim < fsim_best:
        if [round(i, the_accuracy) for i in rescaleParameters(the_sim)] == sim_best:
            fcalls_sincelastimprovement += 1
            fsim_best = the_fsim
        else:
            fsim_best = the_fsim
            sim_best = [round(i, the_accuracy) for i in rescaleParameters(the_sim)]
            fcalls_sincelastimprovement = 0
    else:
        fcalls_sincelastimprovement += 1


def minimize_neldermead(func, x0, args=(), callback=None,
                        maxiter=None, maxfev=None, maxrepeat=3, disp=False,
                        return_all=False, printmov=False, initial_simplex=None,
                        xatol=1e-4, fatol=1e-4, **unknown_options):
    """
    Minimization of scalar function of one or more variables using the Nelder-Mead algorithm.

    Options
    -------
    disp : bool
        Set to True to print convergence messages.
    maxiter, maxfev : int
        Maximum allowed number of iterations and function evaluations.
        Will default to ``N*200``, where ``N`` is the number of
        variables, if neither `maxiter` or `maxfev` is set. If both
        `maxiter` and `maxfev` are set, minimization will stop at the
        first reached.
    initial_simplex : array_like of shape (N + 1, N)
        Initial simplex. If given, overrides `x0`.
        ``initial_simplex[j,:]`` should contain the coordinates of
        the j-th vertex of the ``N+1`` vertices in the simplex, where
        ``N`` is the dimension.
    xatol : float, optional
        Absolute error in xopt between iterations that is acceptable for
        convergence.
    fatol : number, optional
        Absolute error in func(xopt) between iterations that is acceptable for
        convergence.
    """
    ###########################################################################
    global fcalls_sincelastimprovement, fsim_best, sim_best
    ###########################################################################
    print("\n\nLaunch custom nelder mead...")

    print('         maxiter:', maxiter)
    print('         maxfev:', maxfev)
    print('         disp:', disp)
    print('         return_all:', return_all)
    print('         initial_simplex:\n', initial_simplex)
    print('         xatol:', xatol)
    print('         fatol:', fatol)

    ###########################################################################

    theResumeFilePath = __main__.pathOfTheResultsFolder + '/' + __main__.idOfTheCurrentProcess + '.txt'

    with open(theResumeFilePath, 'w') as theResumeFile:
        theResumeFile.write("iteration" + "\t" + "fcall" + "\t" +
                            '\t'.join(__main__.paramName) + "\t" +
                            "criterion" + "\t" + "criterion_min" + "\t" + "criterion_max" + "\t" + "movement" + '\n')

    ###########################################################################

    _check_unknown_options(unknown_options)
    maxfun = maxfev
    retall = return_all

    fcalls, func = wrap_function(func, args)

    rho = 1
    chi = 2
    psi = 0.5  # contraction # update in agree with the article of 1991
    sigma = 0.9  # shrink  # update in agree with the article of 1991

    # nonzdelt = 0.05
    # zdelt = 0.00025

    x0 = asfarray(x0).flatten()

    if initial_simplex is None:
        N = len(x0)
        sim = myfnctns.create_a_regular_initial_simplex(x0)
        # sim = numpy.zeros((N + 1, N), dtype=x0.dtype)
        # sim[0] = x0
        # for k in range(N):
        #     y = numpy.array(x0, copy=True)
        #     if y[k] != 0:
        #         y[k] = (1 + nonzdelt) * y[k]
        #     else:
        #         y[k] = zdelt
        #     sim[k + 1] = y
    else:
        sim = np.asfarray(initial_simplex).copy()
        if sim.ndim != 2 or sim.shape[0] != sim.shape[1] + 1:
            raise ValueError("`initial_simplex` should be an array of shape (N+1,N)")
        if len(x0) != sim.shape[1]:
            raise ValueError("Size of `initial_simplex` is not consistent with `x0`")
        N = sim.shape[1]

    if retall:
        allvecs = [sim[0]]

    # If neither are set, then set both to default
    if maxiter is None and maxfun is None:
        maxiter = N * 200
        maxfun = N * 200
    elif maxiter is None:
        # Convert remaining Nones, to np.inf, unless the other is np.inf, in
        # which case use the default to avoid unbounded iteration
        if maxfun == np.inf:
            maxiter = N * 200
        else:
            maxiter = np.inf
    elif maxfun is None:
        if maxiter == np.inf:
            maxfun = N * 200
        else:
            maxfun = np.inf

    one2np1 = list(range(1, N + 1))
    fsim = numpy.zeros((N + 1,), float)

    for k in range(N + 1):
        fsim[k] = func(sim[k])

        writeResults(0, fcalls, sim[k], sim, fsim[k], fsim, "init", theResumeFilePath)

    ind = numpy.argsort(fsim)
    fsim = numpy.take(fsim, ind, 0)
    # sort so sim[0,:] has the lowest function value
    sim = numpy.take(sim, ind, 0)

    iterations = 1
    actual_repeat = 0
    fcalls_sincelastrelaunch = N + 1

    maxfcalls_sincelastimprovement = 50
    improvement_accuracy = 4

    fcalls_sincelastimprovement = 0
    fsim_best = min(fsim)
    isim_best = [i for i in range(len(fsim)) if fsim[i] == fsim_best][0]
    sim_best = [round(i, improvement_accuracy) for i in rescaleParameters(sim[isim_best])]

    # while (fcalls[0] < maxfun and iterations < maxiter):
    while (fcalls[0] < maxfun and iterations < maxiter) and (not ((actual_repeat == maxrepeat) and (fcalls_sincelastrelaunch >= __main__.relaunch_threshold) and
                                                             ((fcalls_sincelastimprovement >= maxfcalls_sincelastimprovement) or
                                                             (fcalls_sincelastrelaunch >= (2 * __main__.relaunch_threshold))))):
        if printmov:
            print(iterations, '-', fcalls[0])

        if (numpy.max(numpy.ravel(numpy.abs(sim[1:] - sim[0]))) <= xatol and
                numpy.max(numpy.abs(fsim[0] - fsim[1:])) <= fatol):
            break

        if actual_repeat < maxrepeat:
            # if math.floor(iterations / __main__.relaunch_threshold) > actual_repeat:
            # if math.floor(fcalls[0] / __main__.relaunch_threshold) > actual_repeat:
            if fcalls_sincelastrelaunch >= __main__.relaunch_threshold:
                if printmov:
                    print("re-launch", flush=True)
                actual_repeat += 1

                fcalls_sincelastimprovement = 0
                fcalls_sincelastrelaunch = 0

                sim = myfnctns.create_a_regular_initial_simplex(sim[0])
                N = sim.shape[1]

                fsim = numpy.zeros((N + 1,), float)
                for k in range(N + 1):
                    fsim[k] = func(sim[k])
                    fcalls_sincelastrelaunch += 1
                    check_improvement(fsim[k], sim[k], improvement_accuracy)

                    writeResults(0, fcalls, sim[k], sim, fsim[k], fsim, "re-launch", theResumeFilePath)

                ind = numpy.argsort(fsim)
                fsim = numpy.take(fsim, ind, 0)
                # sort so sim[0,:] has the lowest function value
                sim = numpy.take(sim, ind, 0)

        xbar = numpy.add.reduce(sim[:-1], 0) / N
        xr = (1 + rho) * xbar - rho * sim[-1]
        fxr = func(xr)
        fcalls_sincelastrelaunch += 1
        doshrink = 0
        check_improvement(fxr, xr, improvement_accuracy)

        if fxr < fsim[0]:
            writeResults(iterations, fcalls, xr, sim, fxr, fsim, "NA", theResumeFilePath)

            xe = (1 + rho * chi) * xbar - rho * chi * sim[-1]
            fxe = func(xe)
            fcalls_sincelastrelaunch += 1
            check_improvement(fxe, xe, improvement_accuracy)

            if fxe < fxr:
                sim[-1] = xe
                fsim[-1] = fxe
                writeResults(iterations, fcalls, xe, sim, fxe, fsim, "expansion", theResumeFilePath)
            else:
                sim[-1] = xr
                fsim[-1] = fxr
                writeResults(iterations, fcalls, xe, sim, fxe, fsim, "reflection", theResumeFilePath)
        else:  # fsim[0] <= fxr
            if fxr < fsim[-2]:
                sim[-1] = xr
                fsim[-1] = fxr
                writeResults(iterations, fcalls, xr, sim, fxr, fsim, "reflection", theResumeFilePath)
            else:  # fxr >= fsim[-2]
                writeResults(iterations, fcalls, xr, sim, fxr, fsim, "NA", theResumeFilePath)

                # Perform contraction
                if fxr < fsim[-1]:
                    xc = (1 + psi * rho) * xbar - psi * rho * sim[-1]
                    fxc = func(xc)
                    fcalls_sincelastrelaunch += 1
                    check_improvement(fxc, xc, improvement_accuracy)

                    if fxc <= fxr:
                        sim[-1] = xc
                        fsim[-1] = fxc
                        writeResults(iterations, fcalls, xc, sim, fxc, fsim, "contraction", theResumeFilePath)
                    else:
                        doshrink = 1
                        writeResults(iterations, fcalls, xc, sim, fxc, fsim, "NA", theResumeFilePath)
                else:
                    # Perform an inside contraction
                    xcc = (1 - psi) * xbar + psi * sim[-1]
                    fxcc = func(xcc)
                    fcalls_sincelastrelaunch += 1
                    check_improvement(fxcc, xcc, improvement_accuracy)

                    if fxcc < fsim[-1]:
                        sim[-1] = xcc
                        fsim[-1] = fxcc
                        writeResults(iterations, fcalls, xcc, sim, fxcc, fsim, "contraction", theResumeFilePath)
                    else:
                        doshrink = 1
                        writeResults(iterations, fcalls, xcc, sim, fxcc, fsim, "NA", theResumeFilePath)

                if doshrink:
                    for j in one2np1:
                        sim[j] = sim[0] + sigma * (sim[j] - sim[0])
                        fsim[j] = func(sim[j])
                        fcalls_sincelastrelaunch += 1
                        check_improvement(fsim[j], sim[j], improvement_accuracy)
                        writeResults(iterations, fcalls, sim[j], sim, fsim[j], fsim, "shrink", theResumeFilePath)

                    fsim[0] = func(sim[0])  # update the better point in agree with the article of 1991
                    fcalls_sincelastrelaunch += 1
                    check_improvement(fsim[0], sim[0], improvement_accuracy)
                    writeResults(iterations, fcalls, sim[0], sim, fsim[0], fsim, "Shrink", theResumeFilePath)

        ind = numpy.argsort(fsim)
        sim = numpy.take(sim, ind, 0)
        fsim = numpy.take(fsim, ind, 0)
        if callback is not None:
            callback(sim[0])
        iterations += 1
        if retall:
            allvecs.append(sim[0])

    x = sim[0]
    fval = numpy.min(fsim)
    warnflag = 0

    if fcalls[0] >= maxfun:
        warnflag = 1
        msg = _status_message['maxfev']
        if disp:
            print('Warning: ' + msg)
            print("         Current function value: %f" % fval)
            print("         Iterations: %d" % iterations)
            print("         Function evaluations: %d" % fcalls[0])
            print("         fcalls_sincelastimprovement: %d" % fcalls_sincelastimprovement)
    elif iterations >= maxiter:
        warnflag = 2
        msg = _status_message['maxiter']
        if disp:
            print('Warning: ' + msg)
            print("         Current function value: %f" % fval)
            print("         Iterations: %d" % iterations)
            print("         Function evaluations: %d" % fcalls[0])
            print("         fcalls_sincelastimprovement: %d" % fcalls_sincelastimprovement)
    elif ((actual_repeat == maxrepeat) and (fcalls_sincelastrelaunch >= __main__.relaunch_threshold) and (fcalls_sincelastimprovement >= maxfcalls_sincelastimprovement) and (fcalls_sincelastrelaunch < (2 * __main__.relaunch_threshold))):
        warnflag = 3
        msg = _status_message['maximprov']
        if disp:
            print('Warning: ' + msg)
            print("         Current function value: %f" % fval)
            print("         Iterations: %d" % iterations)
            print("         Function evaluations: %d" % fcalls[0])
            print("         fcalls_sincelastimprovement: %d" % fcalls_sincelastimprovement)
    elif ((actual_repeat == maxrepeat) and (fcalls_sincelastrelaunch > __main__.relaunch_threshold) and (fcalls_sincelastrelaunch >= (2 * __main__.relaunch_threshold))):
        warnflag = 4
        msg = _status_message['maxfevlast']
        if disp:
            print('Warning: ' + msg)
            print("         Current function value: %f" % fval)
            print("         Iterations: %d" % iterations)
            print("         Function evaluations: %d" % fcalls[0])
            print("         fcalls_sincelastimprovement: %d" % fcalls_sincelastimprovement)
    else:
        msg = _status_message['success']
        if disp:
            print(msg)
            print("         Current function value: %f" % fval)
            print("         Iterations: %d" % iterations)
            print("         Function evaluations: %d" % fcalls[0])
            print("         fcalls_sincelastimprovement: %d" % fcalls_sincelastimprovement)

    result = OptimizeResult(fun=fval, nit=iterations, nfev=fcalls[0],
                            status=warnflag, success=(warnflag == 0),
                            message=msg, x=x, final_simplex=(sim, fsim))
    if retall:
        result['allvecs'] = allvecs
    return result


# if __name__ == "__main__":
#     main()
