//
//  observedDataPoint.cpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/11/2020.
//

#include "observedDataPoint.hpp"

observedDataPoint::observedDataPoint(){
    carcass_discovery_by_passive_surveillance = 0;
    carcass_discovery_by_active_search = 0;
    test_on_hunted_ind = 0;
    positive_test_on_hunted_ind = 0;
}

observedDataPoint::observedDataPoint(const double& _carcass_discovery_by_passive_surveillance, const double& _carcass_discovery_by_active_search, const double& _test_on_hunted_ind, const double& _positive_test_on_hunted_ind){
    carcass_discovery_by_passive_surveillance = _carcass_discovery_by_passive_surveillance;
    carcass_discovery_by_active_search = _carcass_discovery_by_active_search;
    test_on_hunted_ind = _test_on_hunted_ind;
    positive_test_on_hunted_ind = _positive_test_on_hunted_ind;
}
