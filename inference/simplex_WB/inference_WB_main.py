#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
inference_WB_main.py

author: gael beaunée
email: gael.beaunee@inrae.fr
website: www.gaelbn.com
affiliation: INRA (SA) - BIOEPAR

-

parameter estimation using a custom simplex (Nelder-Mead method) for the ParaTB metapop. model and serological survey data

Use 'python3 inference_WB_main.py -h' for help.

python3 inference_WB_main.py -d /data/15500m -r /results/coupling_iter_<iter n°>/wb -n res_inference_wb_simplex -I 300 -F 300 --paramName beta_sp2 beta_sp2D beta_sp1_sp2 beta_sp2_sp2 beta_sp2D_sp2 rho rho_active_search --paramVal 0.1 0.1 0.1 0.1 0.1 0.1 0.3 --paramMin 0.0 0.0 0.0 0.0 0.0 0.0 0.0 --paramMax 10.0 10.0 10.0 10.0 10.0 10.0 10.0 -e /obs_data_folder --relaunchThreshold 100 --relaunchNbRepeat 2

python3 inference_WB_main.py -d ./data/15500m -r ./results -n res_inference_wb_simplex_test -I 10 -F 10 --paramName beta_sp2 beta_sp2D beta_sp1_sp2 beta_sp2_sp2 beta_sp2D_sp2 rho rho_active_search --paramVal 0.1 0.1 0.1 0.1 0.1 0.1 0.3 --paramMin 0.0 0.0 0.0 0.0 0.0 0.0 0.0 --paramMax 10.0 10.0 10.0 10.0 10.0 10.0 10.0 -e ./data/15500m --relaunchThreshold 100 --relaunchNbRepeat 2

"""


#  o8o                                                      .
#  `"'                                                    .o8
# oooo  ooo. .oo.  .oo.   oo.ooooo.   .ooooo.  oooo d8b .o888oo
# `888  `888P"Y88bP"Y88b   888' `88b d88' `88b `888""8P   888
#  888   888   888   888   888   888 888   888  888       888
#  888   888   888   888   888   888 888   888  888       888 .
# o888o o888o o888o o888o  888bod8P' `Y8bod8P' d888b      "888"
#                          888
#                         o888o

import numpy as np
import pandas as pd
import sys
import os
import argparse
import copy

from inference_WB_functions import (fromParamValToSimplexVal, create_a_regular_initial_simplex, computeCriterionForGivenParameters)


#                        .       .    o8o
#                      .o8     .o8    `"'
#  .oooo.o  .ooooo.  .o888oo .o888oo oooo  ooo. .oo.    .oooooooo  .oooo.o
# d88(  "8 d88' `88b   888     888   `888  `888P"Y88b  888' `88b  d88(  "8
# `"Y88b.  888ooo888   888     888    888   888   888  888   888  `"Y88b.
# o.  )88b 888    .o   888 .   888 .  888   888   888  `88bod8P'  o.  )88b
# 8""888P' `Y8bod8P'   "888"   "888" o888o o888o o888o `8oooooo.  8""888P'
#                                                      d"     YD
#                                                      "Y88888P'

print(sys.version, flush=True)

pd.set_option('display.max_rows', 20)
pd.set_option('display.max_columns', 20)
pd.set_option('display.max_columns', 20)


#  .oooo.   oooo d8b  .oooooooo oo.ooooo.   .oooo.   oooo d8b  .oooo.o  .ooooo.
# `P  )88b  `888""8P 888' `88b   888' `88b `P  )88b  `888""8P d88(  "8 d88' `88b
#  .oP"888   888     888   888   888   888  .oP"888   888     `"Y88b.  888ooo888
# d8(  888   888     `88bod8P'   888   888 d8(  888   888     o.  )88b 888    .o
# `Y888""8o d888b    `8oooooo.   888bod8P' `Y888""8o d888b    8""888P' `Y8bod8P'
#                    d"     YD   888
#                    "Y88888P'  o888o

parser = argparse.ArgumentParser(description="Parameter estimation using a custom simplex (Nelder-Mead method) for the ParaTB metapop. model and serological survey datas. Use 'python3 inference_with_custom_nm_algo.py -h' for help.")

parser.add_argument("-d", "--pathToTheModelDataFolder", dest="pathToTheModelDataFolder",
                    metavar="folder", required=True,
                    help="the path to the model data folder - required")

parser.add_argument("-r", "--resultFolder", dest="pathOfTheResultsFolder",
                    metavar="folder", required=True,
                    help="the result folder - required")

parser.add_argument("-n", "--idOfTheCurrentProcess", dest="idOfTheCurrentProcess",
                    metavar="id", required=True,
                    help="id of the current process - required")

parser.add_argument("-i", "--numberOfRunsPerIteration", dest="numberOfRunsPerIteration",
                    metavar="numberOfRunsPerIteration", type=int, default=100,
                    help="numberOfRunsPerIteration")

parser.add_argument("-j", "--numberOfThreadToUse", dest="numberOfThreadToUse",
                    metavar="numberOfThreadToUse", type=int, default=1,
                    help="numberOfThreadToUse")

parser.add_argument("-I", "--the_maxiter", dest="the_maxiter",
                    metavar="the_maxiter", type=int, default=650,
                    help="the_maxiter")

parser.add_argument("-F", "--the_maxfev", dest="the_maxfev",
                    metavar="the_maxfev", type=int, default=650,
                    help="the_maxfev")

parser.add_argument("--the_xatol", dest="the_xatol",
                    metavar="the_xatol", type=float, default=0.01,
                    help="the_xatol")

parser.add_argument("--the_fatol", dest="the_fatol",
                    metavar="the_fatol", type=float, default=1.,
                    help="the_fatol")

parser.add_argument("-t", "--typeOfInit", dest="typeOfInit",
                    metavar="typeOfInit", default="param0",
                    help="typeOfInit")

parser.add_argument("-s", "--pathOfTheInitialSimplexFile", dest="pathOfTheInitialSimplexFile",
                    metavar="pathOfTheInitialSimplexFile",
                    help="pathOfTheInitialSimplexFile")

parser.add_argument("-e", "--pathOfTheDataFolder", dest="pathOfTheDataFolder",
                    metavar="pathOfTheDataFolder", required=True,
                    help="pathOfTheDataFolder - required")

parser.add_argument('--paramName', nargs='+', dest="paramName",
                    metavar="paramName", required=True,
                    help="parameter names - required")

parser.add_argument('--paramVal', nargs='+', type=float, dest="paramVal",
                    metavar="paramVal", required=True,
                    help="parameter values - required")

parser.add_argument('--paramMin', nargs='+', type=float, dest="paramMin",
                    metavar="paramMin", required=True,
                    help="parameter min. values - required")

parser.add_argument('--paramMax', nargs='+', type=float, dest="paramMax",
                    metavar="paramMax", required=True,
                    help="parameter max. values - required")

parser.add_argument("--typeOfNelderMeadAlgo", dest="typeOfNelderMeadAlgo",
                    metavar="typeOfNelderMeadAlgo", default="custom",
                    help="the type of of nelder mead algo: sale or custom")

parser.add_argument("--relaunchThreshold", dest="relaunchThreshold",
                    metavar="relaunchThreshold", type=int, default=100,
                    help="relaunch threshold")

parser.add_argument("--relaunchNbRepeat", dest="relaunchNbRepeat",
                    metavar="relaunchNbRepeat", type=int, default=5,
                    help="relaunch nb repeat")

parser.add_argument("--periodLengthForCritAggreg", dest="periodLengthForCritAggreg",
                    metavar="periodLengthForCritAggreg", type=int, default=5,
                    help="period length for criterion aggregation")

parser.add_argument('--criterion_based_on_PTprevalence', action="store_true", default=False, help="switch to a criterion based on prevalence for positive tested hunted WB")


parser.add_argument("--tile_size", dest="tile_size",
                    metavar="tile_size", type=int, required=True,
                    help="tile_size")

parser.add_argument("--sim_duration", dest="sim_duration",
                    metavar="sim_duration", type=int, required=True,
                    help="sim_duration")

parser.add_argument("--date_on_which_the_fence_is_built", dest="date_on_which_the_fence_is_built",
                    metavar="date_on_which_the_fence_is_built", type=int, default=60,
                    help="date_on_which_the_fence_is_built")
                    
parser.add_argument("--date_from_which_to_stop_using_plugged_data", dest="date_from_which_to_stop_using_plugged_data",
                    metavar="date_from_which_to_stop_using_plugged_data", type=int, required=True,
                    help="date_from_which_to_stop_using_plugged_data")
                    

#

args = parser.parse_args()

#

pathToTheModelDataFolder = args.pathToTheModelDataFolder
pathOfTheResultsFolder = args.pathOfTheResultsFolder
idOfTheCurrentProcess = args.idOfTheCurrentProcess
numberOfRunsPerIteration = args.numberOfRunsPerIteration
numberOfThreadToUse = args.numberOfThreadToUse
the_maxiter = args.the_maxiter
the_maxfev = args.the_maxfev
the_xatol = args.the_xatol
the_fatol = args.the_fatol
typeOfInit = args.typeOfInit
pathOfTheInitialSimplexFile = args.pathOfTheInitialSimplexFile
pathOfTheDataFolder = args.pathOfTheDataFolder


tile_size = args.tile_size
sim_duration = args.sim_duration
date_on_which_the_fence_is_built = args.date_on_which_the_fence_is_built
date_from_which_to_stop_using_plugged_data = args.date_from_which_to_stop_using_plugged_data


#

paramName = args.paramName
paramVal = args.paramVal
minVal = args.paramMin
maxVal = args.paramMax

#

theInitialParam = paramVal
print(theInitialParam)

#

type_of_nelder_mead_algo = args.typeOfNelderMeadAlgo
relaunch_threshold = args.relaunchThreshold
relaunch_nbRepeat = args.relaunchNbRepeat

#

periodLengthForCritAggreg = args.periodLengthForCritAggreg
criterion_based_on_PTprevalence = args.criterion_based_on_PTprevalence

#

print('type_of_nelder_mead_algo', type_of_nelder_mead_algo, flush=True)
if type_of_nelder_mead_algo == 'custom':
    from inference_custom_nelder_mead_simplex_algo import minimize_neldermead
else:
    raise ValueError("Error: this option does not exist! (try with 'custom').")


# ooooo              o8o      .    o8o            oooo   o8o
# `888'              `"'    .o8    `"'            `888   `"'
#  888  ooo. .oo.   oooo  .o888oo oooo   .oooo.    888  oooo    oooooooo  .ooooo.
#  888  `888P"Y88b  `888    888   `888  `P  )88b   888  `888   d'""7d8P  d88' `88b
#  888   888   888   888    888    888   .oP"888   888   888     .d8P'   888ooo888
#  888   888   888   888    888 .  888  d8(  888   888   888   .d8P'  .P 888    .o
# o888o o888o o888o o888o   "888" o888o `Y888""8o o888o o888o d8888888P  `Y8bod8P'

temp_dir = pathOfTheResultsFolder + "/temp"
if os.path.isdir(temp_dir):
    raise ValueError("Error: results folder already exist! (try with another one or delete it).")
print(temp_dir)
os.mkdir(temp_dir)

# Construction of the inital vector of parameters values
param0 = np.array([fromParamValToSimplexVal(theInitialParam[i], minVal[i], maxVal[i]) for i in range(len(theInitialParam))])


#                                                                  .
#                                                                .o8
#  .ooooo.   .ooooo.  ooo. .oo.  .oo.   oo.ooooo.  oooo  oooo .o888oo  .ooooo.
# d88' `"Y8 d88' `88b `888P"Y88bP"Y88b   888' `88b `888  `888   888   d88' `88b
# 888       888   888  888   888   888   888   888  888   888   888   888ooo888
# 888   .o8 888   888  888   888   888   888   888  888   888   888 . 888    .o
# `Y8bod8P' `Y8bod8P' o888o o888o o888o  888bod8P'  `V88V"V8P'  "888" `Y8bod8P'
#                                        888
#                                       o888o

# Estimation process
if typeOfInit == 'param0':
    initSimpl = create_a_regular_initial_simplex(param0)
    print('initSimplex\n', initSimpl)
else:
    raise ValueError("Error: this typeOfInit does not exist! (try with 'param0').")

res = minimize_neldermead(computeCriterionForGivenParameters, param0, **{'initial_simplex': initSimpl, 'maxiter': the_maxiter, 'maxfev': the_maxfev, 'maxrepeat': relaunch_nbRepeat, 'xatol': the_xatol, 'fatol': the_fatol, 'disp': True, 'return_all': True, 'printmov': True})
# print(res.x, flush=True)


# compute X trajectories using estimated parameter values:

theResumeFilePath_parameters = pathOfTheResultsFolder + '/' + idOfTheCurrentProcess + '.txt'
list_feval = pd.read_csv(theResumeFilePath_parameters, sep='\t')
best_slice = list_feval.loc[list_feval["criterion"] == min(list_feval["criterion"].values)].copy()
print(best_slice, flush=True)
#
beta_sp2 = best_slice.beta_sp2.values[0]  # contamnation intra due to Is + Ic
beta_sp2D = best_slice.beta_sp2D.values[0]  # contamnation intra due to D
beta_sp1_sp2 = best_slice.beta_sp1_sp2.values[0]  # contamination des WB par DP
beta_sp2_sp2 = best_slice.beta_sp2_sp2.values[0]  # contamination des WB par WB
beta_sp2D_sp2 = best_slice.beta_sp2D_sp2.values[0]  # contamination des WB par WB_D
# rho = best_slice.rho.values[0]  # carcass discovery rate with passive surveillance
# rho_active_search = best_slice.rho_active_search.values[0]  # carcass discovery rate with active search
# TODO: launch the model to compute the X trajectories
theNumberOfRuns = 10  # pas super parceque en dur mais bon...
nbRunsPerJob = 1  # idem
command_qsub = 'qsub -e /dev/null -o /dev/null -sync y -q "maiage.q|short.q" -t 1-' + str(theNumberOfRuns) + ':' + str(nbRunsPerJob) + ' ' + './simplex_WB/launchWBDynamicComputation.sh' + " " + str(beta_sp2) + " " + str(beta_sp2D) + " " + str(beta_sp1_sp2) + " " + str(beta_sp2_sp2) + " " + str(beta_sp2D_sp2) + " " + pathToTheModelDataFolder + " " + pathOfTheResultsFolder + " " + str(tile_size) + " " + str(sim_duration) + " " + str(date_on_which_the_fence_is_built) + " " + str(date_from_which_to_stop_using_plugged_data)
os.system(command_qsub)


#   .oooooo.   oooo
#  d8P'  `Y8b  `888
# 888           888   .ooooo.   .oooo.   ooo. .oo.
# 888           888  d88' `88b `P  )88b  `888P"Y88b
# 888           888  888ooo888  .oP"888   888   888
# `88b    ooo   888  888    .o d8(  888   888   888
#  `Y8bood8P'  o888o `Y8bod8P' `Y888""8o o888o o888o

# Delete the folder containing temporary simulations

command_rm_tempSimulationResultsFolderPath = 'rm -rf ' + temp_dir
os.system(command_rm_tempSimulationResultsFolderPath)


#                             .o8
#                            "888
#  .ooooo.  ooo. .oo.    .oooo888
# d88' `88b `888P"Y88b  d88' `888
# 888ooo888  888   888  888   888
# 888    .o  888   888  888   888
# `Y8bod8P' o888o o888o `Y8bod88P"


print("\n\nEnd!")

######################################################################
