#ifndef ZONE_H
#define ZONE_H
class Zone {
private:
	double x;
	double y;
	double r;

public:
	Zone(double posx, double posy, double radius);
	bool isIn(double posx, double posy);
	double getX();
	double getY();
};
#endif
