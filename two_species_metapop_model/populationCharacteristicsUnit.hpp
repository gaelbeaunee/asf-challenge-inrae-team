//
//  populationCharacteristicsUnit.hpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#ifndef populationCharacteristicsUnit_hpp
#define populationCharacteristicsUnit_hpp

#include <stdio.h>
#include <string>

struct populationCharacteristicsUnit {
    // Constructor
    populationCharacteristicsUnit();
    populationCharacteristicsUnit(const std::string& _pop_id, const std::string& _pop_type, const int& _pop_initial_size);
    populationCharacteristicsUnit(const std::string& _pop_id, const std::string& _pop_type, const int& _pop_initial_size, const double& _centroid_X, const double& _centroid_Y, const bool& _is_within_fenced_area, const bool& _is_15km_around_fenced_area);

    // Variables
    std::string pop_id;
    std::string pop_type;
    int pop_initial_size;
    double centroid_X;
    double centroid_Y;
    bool is_within_fenced_area;
    bool is_15km_around_fenced_area;
};

#endif /* populationCharacteristicsUnit_hpp */
