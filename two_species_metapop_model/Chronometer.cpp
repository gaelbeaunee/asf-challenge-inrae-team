//
//  Chronometer.cpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#include "Chronometer.hpp"

// Constructor =========================================================
Chronometer::Chronometer(){

}

// Member functions ====================================================
void Chronometer::start(){
    tbegin = std::chrono::steady_clock::now();
}

void Chronometer::stop(){
    tend = std::chrono::steady_clock::now();
    texec += std::chrono::duration_cast<std::chrono::nanoseconds>(tend - tbegin).count();
}
