#! /bin/bash
# Args : T0 TF SEED ALPHA BETA
# Data files should be passed to stdin

if [ $# -lt 7 ]
then
	echo "Args : TMIN T0 TF SEED ALPHA BETA GAMMA" >> /dev/stderr
	exit 1
fi

TMIN=$1
T0=$2
TF=$3
SEED=$4
ALPHA=$5
BETA=$6
GAMMA=$7

./likelihood_pg $TMIN $T0 $TF $SEED $ALPHA $BETA $GAMMA

exit 0
