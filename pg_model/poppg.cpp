#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <set>
#include <random>
#include <math.h>
#include <boost/tokenizer.hpp>

#include "simulatorpg.hpp"
#include "poppg.hpp"

void PopPG::addContact(string pg, string wb){
	contacts[pg].push_back(wb);
}

void PopPG::addLocalContacts(double maxdist){
	for(auto &pg1 : type){
		for(auto &pg2 : type){
			double dx=posx[pg1.first]-posx[pg2.first];
			double dy=posy[pg1.first]-posy[pg2.first];
			double d=sqrt(dx*dx+dy*dy);
			if(pg1.first!=pg2.first && d<maxdist){
				addLocalContact(pg1.first, pg2.first);
			}
		}
	}
}

void PopPG::addLocalContact(string pg, string pgcont){
	localContacts[pg].push_back(pgcont);
}

void PopPG::addSnapshot(double t){
	snapshot[t]=new snapshotpg();
}

void PopPG::addSnapshotPop(double t, string id, status st, unsigned int si){
	if(snapshot.count(t)==0){
		addSnapshot(t);
	}
	snapshot[t]->addPop(id, st, si);
}

void PopPG::addSnapshotExchange(double t, string id, string wb, unsigned int weight){
	if(snapshot.count(t)==0){
		addSnapshot(t);
	}
	snapshot[t]->addExchange(id, wb, weight);
}

void PopPG::addSnapshotExport(double t, string id, string wb, unsigned int weight){
	if(snapshot.count(t)==0){
		addSnapshot(t);
	}
	snapshot[t]->addExports(id, wb, weight);
}

void PopPG::addPop(string id, herdtype typ, unsigned int si){
	type[id]=typ;
	size[id]=si;

	sampleStatusTimes(id);
}

void PopPG::addPop(string id, herdtype typ, unsigned int si, double x, double y){
	type[id]=typ;
	size[id]=si;
	posx[id]=x;
	posy[id]=y;

	sampleStatusTimes(id);
}

void PopPG::sampleStatusTimes(string id){
	timeE[id]=sampleTimeE();
	timeIs[id]=sampleTimeIs();
}

void PopPG::newStatusTimes(){
	for(auto &herd: type){
		string id=herd.first;
		timeE[id]=sampleTimeE();
		timeIs[id]=sampleTimeIs();
	}
}

unsigned int PopPG::getSize(string id, double t){
	return snapshot[t]->getSize(id);	
}

void PopPG::setSize(string id, double t, unsigned int size){
	snapshot[t]->setSize(id, size);	
}

herdtype PopPG::getType(string id){
	return type[id];
}

vector<string> PopPG::getContacts(string id){
	return contacts[id];
}

vector<string> PopPG::getLocalContacts(string id){
	return localContacts[id];
}

status PopPG::getStatus(string id, double t){
	return snapshot[t]->getStatus(id);
}

void PopPG::setClock(double t, string id, unsigned int clock){
	snapshot[t]->setClock(id, clock);
}

unsigned int PopPG::getClock(double t, string id){
	return snapshot[t]->getClock(id);
}

unsigned int PopPG::getSize(string id){
	return size[id];
}

unordered_map<string, unsigned int> PopPG::getExchanges(string id, double t){
	if(snapshot.count(t)>0){
		return snapshot[t]->getExchanges(id);
	} else {
		return unordered_map<string, unsigned int>();
	}
}

unordered_map<string, unsigned int> PopPG::getExports(string id, double t){
	if(snapshot.count(t)>0){
		return snapshot[t]->getExports(id);
	} else {
		return unordered_map<string, unsigned int>();
	}
}

vector<string> PopPG::getIds(double t){
	if(snapshot.count(t)>0){
		return snapshot[t]->getIds();
	} else {
		return vector<string>();
	}
}

bool PopPG::isSet(string id, double t){
	if(snapshot.count(t)>0){
		return snapshot[t]->isSet(id);
	} else {
		return false;
	}
}

void PopPG::updateExchanges(double t){
	snapshot[t]->updateExchanges();
}

unordered_map<string, herdtype> PopPG::getType(){
	return type;
}

bool PopPG::isBanned(string id, double t){
	return snapshot[t]->isBanned(id, posx[id], posy[id]);
}

void PopPG::addPZ(string id, double t, double rad){
	if(snapshot.count(t)==0){
		addSnapshot(t);
	}
	snapshot[t]->addPZ(Zone(posx[id], posy[id], rad));
}

void PopPG::addSZ(string id, double t,  double rad){
	if(snapshot.count(t)==0){
		addSnapshot(t);
	}
	snapshot[t]->addSZ(Zone(posx[id], posy[id], rad));
}

void PopPG::addBacktrace(string id, double tstart, double pastdur, double tend, double dt){
	set<string> contacts;
	// Get the list of contacts
	for(double t=tstart; t>tstart-pastdur; t-=dt){
		for(auto &ex : getExports(id, t)){
			contacts.insert(ex.first);
		}
	}

	// Add them to the bactraced sets during the correct period
	for(double t=tstart; t<tend; t+=dt){
		for(auto &ex : contacts){
			snapshot[t]->addBacktraceFromInfected(ex);

			// Cull backtraced
#ifdef CMP2_CULL_BT
			if(t>=CMP2_START){
				addSnapshotPop(t, id, R, getSize(id));
			}
#endif
		}
	}
}

void PopPG::readPopDescFromFile(string file){
	using namespace boost;
	typedef boost::tokenizer<boost::char_separator<char>> Tokenizer;
	boost::char_separator<char> sep{";"};
	boost::char_separator<char> sep2{","};
	vector<string> header, vec, counts;
	string line, id;
	double t;

	ifstream in(file.c_str());

	if (!in.is_open()){
		return;
	}

	// Header
	if(getline(in,line)){
		Tokenizer tok(line, sep);
		header.assign(tok.begin(),tok.end());
	}
	while(getline(in,line)){
		Tokenizer tok(line, sep);
		vec.assign(tok.begin(),tok.end());
		if(vec[1]=="outdoor"){
			addPop(vec[0], outdoor, stoi(vec[2]));
		} else {
			addPop(vec[0], indoor, stoi(vec[2]));
		}
	}
	in.close();
}

void PopPG::readPopDescFromFile2(string file){
	using namespace boost;
	typedef boost::tokenizer<boost::char_separator<char>> Tokenizer;
	boost::char_separator<char> sep{","};
	string line, id;
	vector<string> header, vec, counts;
	double t;

	ifstream in(file.c_str());

	if (!in.is_open()){
		return;
	}

	// Header
	if(getline(in,line)){
		Tokenizer tok(line, sep);
		header.assign(tok.begin(),tok.end());
	}
	while(getline(in,line)){
		Tokenizer tok(line, sep);
		vec.assign(tok.begin(),tok.end());
		if(vec[1]=="domestic_pig"){
			// TODO : add outdoor/indoor in the file
			addPop(vec[0], outdoor, stoi(vec[2]), stod(vec[3]), stod(vec[4]));
		}
	}
	in.close();
}

void PopPG::readLocalContactsFromFile(string file){
	using namespace boost;
	typedef boost::tokenizer<boost::char_separator<char>> Tokenizer;
	boost::char_separator<char> sep{","};
	boost::char_separator<char> sep2{","};
	vector<string> header, vec, counts;
	string line, id;
	double t;

	// Clear contacts if necessary
	if(!contacts.empty()){
		localContacts.clear();
	}

	ifstream in(file.c_str());

	if (!in.is_open()){
        cerr << "Local contact file not opened" << endl;
		return;
	}

	// Header
	if(getline(in,line)){
		Tokenizer tok(line, sep);
		header.assign(tok.begin(),tok.end());
	}
	while(getline(in,line)){
		Tokenizer tok(line, sep);
		vec.assign(tok.begin(),tok.end());
		addLocalContact(vec[0], vec[1]);
	}
	in.close();
}

void PopPG::readContactsFromFile(string file){
	using namespace boost;
	typedef boost::tokenizer<boost::char_separator<char>> Tokenizer;
	boost::char_separator<char> sep{","};
	boost::char_separator<char> sep2{","};
	vector<string> header, vec, counts;
	string line, id;
	double t;

	// Clear contacts if necessary
	if(!contacts.empty()){
		contacts.clear();
	}

	ifstream in(file.c_str());

	if (!in.is_open()){
        cerr << "Contact file not opened" << endl;
		return;
	}

	// Header
	if(getline(in,line)){
		Tokenizer tok(line, sep);
		header.assign(tok.begin(),tok.end());
	}
	while(getline(in,line)){
		Tokenizer tok(line, sep);
		vec.assign(tok.begin(),tok.end());
		addContact(vec[0], vec[1]);
	}
	in.close();
}

void PopPG::readExchangesFromFile(string file){
	using namespace boost;
	typedef boost::tokenizer<boost::char_separator<char>> Tokenizer;
	boost::char_separator<char> sep{","};
	vector<string> header, vec, counts;
	string line, id;
	double t;

	ifstream in(file.c_str());

	if (!in.is_open()){
		return;
	}

	// Header
	if(getline(in,line)){
		Tokenizer tok(line, sep);
		header.assign(tok.begin(),tok.end());
	}
	while(getline(in,line)){
		Tokenizer tok(line, sep);
		vec.assign(tok.begin(),tok.end());
		addSnapshotExchange(stod(vec[2]), vec[1], vec[0], stoi(vec[3]));
		addSnapshotExport(stod(vec[2]), vec[0], vec[1], stoi(vec[3]));
	}
	in.close();
}

void PopPG::readPopFromFile(string file){
	using namespace boost;
	typedef boost::tokenizer<boost::char_separator<char>> Tokenizer;
	boost::char_separator<char> sep{";"};
	boost::char_separator<char> sep2{","};
	vector<string> header, vec, counts;
	string line, id;
	double t;

	ifstream in(file.c_str());

	if (!in.is_open()){
		return;
	}

	// Header
	if(getline(in,line)){
		Tokenizer tok(line, sep);
		header.assign(tok.begin(),tok.end());
	}
	while(getline(in,line)){
		Tokenizer tok(line, sep);
		vec.assign(tok.begin(),tok.end());
		status s=S;
		if(vec[2]=="S"){
			s=S;
		} else if(vec[2]=="E"){
			s=E;
		} else if(vec[2]=="Is"){
			s=Is;
		} else if(vec[2]=="Ic"){
			s=Ic;
		} else if(vec[2]=="R"){
			s=R;
		}
		addSnapshotPop(stod(vec[1]), vec[0], s, stoi(vec[3]));
		setClock(stod(vec[1]), vec[0], stoi(vec[4]));
	}
	in.close();
}

void PopPG::printPop(double t, string file, bool reset){
	if(file.empty()){
		if(reset){
			cout << "id";
			cout << "," << "t";
			cout << "," << "status";
			cout << endl;
		}

		vector<string> ids=getIds(t);
		for(auto id=ids.begin(); id!=ids.end(); ++id){
			cout << *id;
			cout << "," << t;
			cout << "," << Status::statusToString(getStatus(*id, t));
			cout << endl;
		}
	} else {
		ofstream ofile;
		if(reset){
  			ofile.open(file);
			ofile << "id";
			ofile << "," << "t";
			ofile << "," << "status";
			ofile << endl;
		} else {
  			ofile.open(file, ios::app);
		}

		vector<string> ids=getIds(t);
		for(auto id=ids.begin(); id!=ids.end(); ++id){
			ofile << *id;
			ofile << "," << t;
			ofile << "," << Status::statusToString(getStatus(*id, t));
			ofile << endl;
		}
		ofile.close();
	}

}

int PopPG::sampleTimeE(){
	//std::exponential_distribution<double> distribution(1.0/PAR_TIMEE);
   //double number = distribution(generator);
	double number=-PAR_TIMEE*log(drand48());
	return ceil(number);
}

int PopPG::sampleTimeIs(){
	//std::exponential_distribution<double> distribution(1.0/PAR_TIMEIS);
   //double number = distribution(generator);
	double number=-PAR_TIMEIS*log(drand48());
	return ceil(number);
}

int PopPG::getTimeE(string id){
	return timeE[id];
}

int PopPG::getTimeIs(string id){
	return timeIs[id];
}

void PopPG::setInfectionProbability(double t, string id, double p){
	snapshot[t]->setPinf(id, p);
}

double PopPG::getInfectionProbability(double t, string id){
	return snapshot[t]->getPinf(id);
}

bool PopPG::isPZ(string id, double t){
	if(snapshot.count(t)>0){
		return snapshot[t]->isPZ(id, posx[id], posy[id]);
	}
	return false;
}

double PopPG::getX(string id){
	return posx[id];
}

double PopPG::getY(string id){
	return posy[id];
}


