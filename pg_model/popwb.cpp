#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <boost/tokenizer.hpp>

#include "popwb.hpp"

void PopWB::addSnapshot(double t){
	snapshot[t]=new snapshotwb();
}

void PopWB::addSnapshotPop(double t, string id, unsigned int S, unsigned int E, unsigned int Is, unsigned int Ic, unsigned int R){
	if(snapshot.count(t)==0){
		addSnapshot(t);
	}
	snapshot[t]->addPop(id, S, E, Is, Ic, R);
}

void PopWB::addSnapshotPop(double t, string id, status s, unsigned int count){
	if(snapshot.count(t)==0){
		addSnapshot(t);
	}
	snapshot[t]->addPop(id, s, count);
}

unsigned int PopWB::getCount(double t, string id, status stat){
	if(snapshot.count(t)>0){
		return snapshot[t]->getCount(id, stat);	
	} else {
		return 0;
	}
}

void PopWB::readPopFromFile(string file){
	using namespace boost;
	typedef boost::tokenizer<boost::char_separator<char>> Tokenizer;
	boost::char_separator<char> sep{";"};
	boost::char_separator<char> sep2{","};
	vector<string> header, vec, counts;
	string line, id;
	double t;

	ifstream in(file.c_str());

	if (!in.is_open()){
		return;
	}

	// Header
	if(getline(in,line)){
		Tokenizer tok(line, sep);
		header.assign(tok.begin(),tok.end());
	}
	while(getline(in,line)){
		Tokenizer tok(line, sep);
		vec.assign(tok.begin(),tok.end());
		id=vec[0];
		
		for(int i=1; i<vec.size(); i++){
			t=stod(header[i]);
			Tokenizer tok(vec[i], sep2);
			counts.assign(tok.begin(),tok.end());
			addSnapshotPop(t, id, stoi(counts[0]), stoi(counts[1]), stoi(counts[2]), stoi(counts[3]), stoi(counts[4]));
		}
	}
	in.close();
}

// Format on pop by rows, times in columns, separator is ","
void PopWB::readPopFromFile(string file, status stat){
	using namespace boost;
	typedef boost::tokenizer<boost::char_separator<char>> Tokenizer;
	boost::char_separator<char> sep{","};
	vector<string> header, vec, counts;
	string line, id;
	double t;

	ifstream in(file.c_str());

	if (!in.is_open()){
		return;
	}

	// Header
	if(getline(in,line)){
		Tokenizer tok(line, sep);
		header.assign(tok.begin(),tok.end());
	}
	while(getline(in,line)){
		Tokenizer tok(line, sep);
		vec.assign(tok.begin(),tok.end());
		id=vec[0];
		
		for(int i=1; i<vec.size(); i++){
			t=stod(header[i]);
			addSnapshotPop(t, id, stat, stoi(vec[i]));
		}
	}
	in.close();
}

vector<string> PopWB::getContactsDP(string id){
	return contactsDP[id];
}


void PopWB::addContactDP(string wb, string dp){
	contactsDP[wb].push_back(dp);
}

