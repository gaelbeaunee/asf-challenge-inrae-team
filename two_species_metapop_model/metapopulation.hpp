//
//  metapopulation.hpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#ifndef metapopulation_hpp
#define metapopulation_hpp

#include <stdio.h>
#include <unordered_map>
#include <set>
#include <string>
#include <iterator>
#include <memory>

#include "simulationParameters.hpp"
#include "simulationData.hpp"
#include "sirHeadcount.hpp"
#include "population.hpp"
#include "metapopulationSummary.hpp"
#include "Chronometer.hpp"
#include "auxiliaryFunctions.hpp"

#ifdef PGCOUPLING
#include "../pg_model/simulatorpg.hpp"
#endif


class metapopulation {
public:
    // Constructor
    metapopulation(const int& _rep_id);

    // Variables
    // repetition info
    int rep_id;
    // metapopulation structure
    std::unordered_map<std::string, std::unique_ptr<population>> populations;
    std::unordered_map<std::string, sirHeadcount> current_pending_movements;
    std::unordered_map<std::string, sirHeadcount> next_pending_movements;
    // metapopulation summary
    metapopulationSummary summary = metapopulationSummary();
    int total_pop_count = 0;
    int sp1_pop_count = 0;
    int sp2_pop_count = 0;
    std::set<std::string> list_pop_initially_infected;

    // Member functions
    double stoc_dyn_iteration();
    void save_headcount_over_time(){ summary.save_headcount_over_time(rep_id); }
    void save_events_over_time(){ summary.save_events_over_time(rep_id); }
    void save_mvts_not_performed(){ summary.save_mvts_not_performed(rep_id); }
    void save_rho_over_time(){ summary.save_rho_over_time(rep_id); }
    void save_carcasses_location(){ summary.save_carcasses_location(rep_id); }

	 #ifdef PGCOUPLING
	 SimulatorPG simpg;
	 void initSimpg(double t0, double tf, double tobs);
	 void save_pg(double t0, double tf);
	 #endif
};

#endif /* metapopulation_hpp */
