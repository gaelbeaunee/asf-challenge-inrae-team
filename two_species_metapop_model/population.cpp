//
//  population.cpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#include "population.hpp"

// Constructor
population::population(const std::string& _pop_id, const int& _pop_index, const int& _pop_init_size, const bool& _is_infected, std::unordered_map<std::string, std::unique_ptr <population>>& _all_populations, metapopulationSummary& _metapop_summary){
    pop_id = _pop_id;
    pop_index = _pop_index;
    pop_init_size = _pop_init_size;
    //
    coord_centroid_X = simulationData::pop_characteristics[pop_id].centroid_X;
    coord_centroid_Y = simulationData::pop_characteristics[pop_id].centroid_Y;
    is_within_a_fenced_area = simulationData::pop_characteristics[pop_id].is_within_fenced_area;
    is_15km_around_a_fenced_area = simulationData::pop_characteristics[pop_id].is_15km_around_fenced_area;
    //
    if (_is_infected) {
        int nb_inf_indiv_used = std::min(simulationParameters::nb_of_inf_indiv_initially_introduced, pop_init_size);
        headcount.update(pop_init_size-nb_inf_indiv_used, 0, nb_inf_indiv_used, 0, 0, 0);
    } else {
        headcount.update(pop_init_size, 0, 0, 0, 0, 0);
    }
    //
    ordered_event_list = {"infection_thru_I", "infection_thru_D", "inf_contact_sp1", "inf_contact_sp2_I", "inf_contact_sp2_D", "start_shedding", "onset_of_clinical_signs", "death", "D_no_more_infectious", "carcass_discovery_and_removal_by_passive_surveillance", "carcass_discovery_and_removal_by_active_search", "hunt_of_an_S", "hunt_of_an_E", "hunt_of_an_Is", "hunt_of_an_Ic", "test_on_hunted_SEIsIc", "positive_test_on_hunted_SEIsIc"};
    //
    m_change["infection_thru_I"] = sirHeadcount(-1, +1, 0, 0, 0, 0);
    m_change["infection_thru_D"] = sirHeadcount(-1, +1, 0, 0, 0, 0);
    m_change["inf_contact_sp1"] = sirHeadcount(-1, +1, 0, 0, 0, 0);
    m_change["inf_contact_sp2_I"] = sirHeadcount(-1, +1, 0, 0, 0, 0);
    m_change["inf_contact_sp2_D"] = sirHeadcount(-1, +1, 0, 0, 0, 0);
    m_change["start_shedding"] = sirHeadcount(0, -1, +1, 0, 0, 0);
    m_change["onset_of_clinical_signs"] = sirHeadcount(0, 0, -1, +1, 0, 0);
    m_change["death"] = sirHeadcount(0, 0, 0, -1, +1, 0);
    m_change["removed"] = sirHeadcount(0, 0, 0, -1, 0, +1);
    m_change["D_no_more_infectious"] = sirHeadcount(0, 0, 0, 0, -1, +1);
    m_change["carcass_discovery_and_removal"] = sirHeadcount(0, 0, 0, 0, -1, +1);
    m_change["hunt_of_an_S"] = sirHeadcount(-1, 0, 0, 0, 0, +1);
    m_change["hunt_of_an_E"] = sirHeadcount(0, -1, 0, 0, 0, +1);
    m_change["hunt_of_an_Is"] = sirHeadcount(0, 0, -1, 0, 0, +1);
    m_change["hunt_of_an_Ic"] = sirHeadcount(0, 0, 0, -1, 0, +1);
    //
    m_rate = {{"infection_thru_I", 0}, {"infection_thru_D", 0}, {"inf_contact_sp1", 0}, {"inf_contact_sp2_I", 0}, {"inf_contact_sp2_D", 0}, {"start_shedding", 0}, {"onset_of_clinical_signs", 0}, {"death", 0}, {"removed", 0}, {"D_no_more_infectious", 0}, {"carcass_discovery_and_removal_by_passive_surveillance", 0}, {"carcass_discovery_and_removal_by_active_search", 0}, {"hunt_of_an_S", 0}, {"hunt_of_an_E", 0}, {"hunt_of_an_Is", 0}, {"hunt_of_an_Ic", 0}, {"test_on_hunted_SEIsIc", 0}, {"positive_test_on_hunted_SEIsIc", 0}};
    //
    m_events_count = {{"infection_thru_I", 0}, {"infection_thru_D", 0}, {"inf_contact_sp1", 0}, {"inf_contact_sp2_I", 0}, {"inf_contact_sp2_D", 0}, {"start_shedding", 0}, {"onset_of_clinical_signs", 0}, {"death", 0}, {"removed", 0}, {"D_no_more_infectious", 0}, {"carcass_discovery_and_removal_by_passive_surveillance", 0}, {"carcass_discovery_and_removal_by_active_search", 0}, {"hunt_of_an_S", 0}, {"hunt_of_an_E", 0}, {"hunt_of_an_Is", 0}, {"hunt_of_an_Ic", 0}, {"test_on_hunted_SEIsIc", 0}, {"positive_test_on_hunted_SEIsIc", 0}};
}

species1Population::species1Population(const std::string& _pop_id, const int& _pop_index, const int& _pop_init_size, const bool& _is_infected, std::unordered_map<std::string, std::unique_ptr <population>>& _all_populations, metapopulationSummary& _metapop_summary) : population(_pop_id, _pop_index, _pop_init_size, _is_infected, _all_populations, _metapop_summary){
    pop_species = simulationParameters::sp1_name;
    is_sp1 = true;
    //
    if (simulationParameters::sp1_dynamic_from_data) {
        headcount.replace(simulationData::sp1_predefined_dynamic[pop_id][0]);
    }
    //
    ordered_event_list.clear();
    ordered_event_list = {"infection_thru_I", "inf_contact_sp1", "inf_contact_sp2_I", "start_shedding", "onset_of_clinical_signs", "removed"};
    //
    m_change.clear();
    m_change["infection_thru_I"] = sirHeadcount(-1, +1, 0, 0, 0, 0);
    m_change["inf_contact_sp1"] = sirHeadcount(-1, +1, 0, 0, 0, 0);
    m_change["inf_contact_sp2_I"] = sirHeadcount(-1, +1, 0, 0, 0, 0);
    m_change["start_shedding"] = sirHeadcount(0, -1, +1, 0, 0, 0);
    m_change["onset_of_clinical_signs"] = sirHeadcount(0, 0, -1, +1, 0, 0);
    m_change["removed"] = sirHeadcount(0, 0, 0, -1, 0, +1);
    //
//    m_rate.clear();
//    m_rate = {{"infection_thru_I", 0}, {"inf_contact_sp1", 0}, {"inf_contact_sp2_I", 0}, {"start_shedding", 0}, {"onset_of_clinical_signs", 0}, {"removed", 0}};
    //
//    m_events_count.clear();
//    m_events_count = {{"infection_thru_I", 0}, {"inf_contact_sp1", 0}, {"inf_contact_sp2_I", 0}, {"start_shedding", 0}, {"onset_of_clinical_signs", 0}, {"removed", 0}};
}

species2Population::species2Population(const std::string& _pop_id, const int& _pop_index, const int& _pop_init_size, const bool& _is_infected, std::unordered_map<std::string, std::unique_ptr <population>>& _all_populations, metapopulationSummary& _metapop_summary) : population(_pop_id, _pop_index, _pop_init_size, _is_infected, _all_populations, _metapop_summary){
    pop_species = simulationParameters::sp2_name;
    is_sp2 = true;
    //
    if (simulationParameters::sp2_dynamic_from_data) {
        headcount.update(simulationData::sp2_predefined_dynamic[pop_id][0]);
    }
    //
    ordered_event_list.clear();
    ordered_event_list = {"infection_thru_I", "infection_thru_D", "inf_contact_sp1", "inf_contact_sp2_I", "inf_contact_sp2_D", "start_shedding", "onset_of_clinical_signs", "death", "D_no_more_infectious", "carcass_discovery_and_removal_by_passive_surveillance", "carcass_discovery_and_removal_by_active_search", "hunt_of_an_S", "hunt_of_an_E", "hunt_of_an_Is", "hunt_of_an_Ic", "test_on_hunted_SEIsIc", "positive_test_on_hunted_SEIsIc"};
    //
    m_change.clear();
    m_change["infection_thru_I"] = sirHeadcount(-1, +1, 0, 0, 0, 0);
    m_change["infection_thru_D"] = sirHeadcount(-1, +1, 0, 0, 0, 0);
    m_change["inf_contact_sp1"] = sirHeadcount(-1, +1, 0, 0, 0, 0);
    m_change["inf_contact_sp2_I"] = sirHeadcount(-1, +1, 0, 0, 0, 0);
    m_change["inf_contact_sp2_D"] = sirHeadcount(-1, +1, 0, 0, 0, 0);
    m_change["start_shedding"] = sirHeadcount(0, -1, +1, 0, 0, 0);
    m_change["onset_of_clinical_signs"] = sirHeadcount(0, 0, -1, +1, 0, 0);
    m_change["death"] = sirHeadcount(0, 0, 0, -1, +1, 0);
    m_change["D_no_more_infectious"] = sirHeadcount(0, 0, 0, 0, -1, +1);
    m_change["carcass_discovery_and_removal_by_passive_surveillance"] = sirHeadcount(0, 0, 0, 0, -1, +1);
    m_change["carcass_discovery_and_removal_by_active_search"] = sirHeadcount(0, 0, 0, 0, -1, +1);
    m_change["hunt_of_an_S"] = sirHeadcount(-1, 0, 0, 0, 0, +1);
    m_change["hunt_of_an_E"] = sirHeadcount(0, -1, 0, 0, 0, +1);
    m_change["hunt_of_an_Is"] = sirHeadcount(0, 0, -1, 0, 0, +1);
    m_change["hunt_of_an_Ic"] = sirHeadcount(0, 0, 0, -1, 0, +1);
    //
//    m_rate.clear();
//    m_rate = {{"infection_thru_I", 0}, {"infection_thru_D", 0}, {"inf_contact_sp1", 0}, {"inf_contact_sp2_I", 0}, {"inf_contact_sp2_D", 0}, {"start_shedding", 0}, {"onset_of_clinical_signs", 0}, {"death", 0}, {"D_no_more_infectious", 0}, {"carcass_discovery_and_removal", 0}, {"hunt_of_an_S", 0}, {"hunt_of_an_E", 0}, {"hunt_of_an_Is", 0}, {"hunt_of_an_Ic", 0}};
    //
//    m_events_count.clear();
//    m_events_count = {{"infection_thru_I", 0}, {"infection_thru_D", 0}, {"inf_contact_sp1", 0}, {"inf_contact_sp2_I", 0}, {"inf_contact_sp2_D", 0}, {"start_shedding", 0}, {"onset_of_clinical_signs", 0}, {"death", 0}, {"D_no_more_infectious", 0}, {"carcass_discovery_and_removal", 0}, {"hunt_of_an_S", 0}, {"hunt_of_an_E", 0}, {"hunt_of_an_Is", 0}, {"hunt_of_an_Ic", 0}};
}

// Member functions
void population::updateStochasticDynamic(const double& _timestep, std::unordered_map<std::string, std::unique_ptr <population>>& _all_populations, metapopulationSummary& _metapop_summary){
//    std::cout << pop_id << " - population::updateStochasticDynamic" << std::endl; // TEST
    // clear the rate value in the map use to store the information
    m_rate = {{"infection_thru_I", 0}, {"infection_thru_D", 0}, {"inf_contact_sp1", 0}, {"inf_contact_sp2_I", 0}, {"inf_contact_sp2_D", 0}, {"start_shedding", 0}, {"onset_of_clinical_signs", 0}, {"death", 0}, {"removed", 0}, {"D_no_more_infectious", 0}, {"carcass_discovery_and_removal_by_passive_surveillance", 0}, {"carcass_discovery_and_removal_by_active_search", 0}, {"hunt_of_an_S", 0}, {"hunt_of_an_E", 0}, {"hunt_of_an_Is", 0}, {"hunt_of_an_Ic", 0}, {"test_on_hunted_SEIsIc", 0}, {"positive_test_on_hunted_SEIsIc", 0}};
    // clear the number of events in the map use to store the information
    m_events_count = {{"infection_thru_I", 0}, {"infection_thru_D", 0}, {"inf_contact_sp1", 0}, {"inf_contact_sp2_I", 0}, {"inf_contact_sp2_D", 0}, {"start_shedding", 0}, {"onset_of_clinical_signs", 0}, {"death", 0}, {"removed", 0}, {"D_no_more_infectious", 0}, {"carcass_discovery_and_removal_by_passive_surveillance", 0}, {"carcass_discovery_and_removal_by_active_search", 0}, {"hunt_of_an_S", 0}, {"hunt_of_an_E", 0}, {"hunt_of_an_Is", 0}, {"hunt_of_an_Ic", 0}, {"test_on_hunted_SEIsIc", 0}, {"positive_test_on_hunted_SEIsIc", 0}};
    //
    int N = headcount.sum();
    if (N > 0) {
//        double previous_timestep = _timestep - simulationParameters::tau;

        //
        if (simulationParameters::density_dependent_tranmission_withinpop_I) { // density dependent tranmission withinpop -> I
            m_rate["infection_thru_I"] = (is_sp1 * simulationParameters::beta_sp1 + is_sp2 * simulationParameters::beta_sp2) * headcount.getScount() * std::pow(headcount.getIcount(), simulationParameters::alpha);
        } else { // frequency dependent tranmission withinpop -> I/N
            m_rate["infection_thru_I"] = (is_sp1 * simulationParameters::beta_sp1 + is_sp2 * simulationParameters::beta_sp2) * headcount.getScount() * std::pow(headcount.getIcount(), simulationParameters::alpha) / N;
        }

        //
        if (simulationParameters::density_dependent_tranmission_withinpop_D) { // density dependent tranmission withinpop -> D
            m_rate["infection_thru_D"] = (is_sp1 * simulationParameters::beta_sp1D + is_sp2 * simulationParameters::beta_sp2D) * headcount.getScount() * headcount.getDcount();
        } else { // frequency dependent tranmission withinpop -> D/N
            m_rate["infection_thru_D"] = (is_sp1 * simulationParameters::beta_sp1D + is_sp2 * simulationParameters::beta_sp2D) * headcount.getScount() * headcount.getDcount() / N;
        }

        // inf_contact_sp1
        double sum_sp1_I = _metapop_summary.sum_I_neighbour_sp1_previous_timestep[pop_index];
        double foi_contact_with_sp1_pop = ((is_sp1 * simulationParameters::beta_sp1_sp1 + is_sp2 * simulationParameters::beta_sp1_sp2) * std::pow(sum_sp1_I, simulationParameters::alpha));
        m_rate["inf_contact_sp1"] = headcount.getScount() * foi_contact_with_sp1_pop;

        // inf_contact_sp2_I
        double sum_sp2_I = _metapop_summary.sum_I_neighbour_sp2_previous_timestep[pop_index];
        double foi_contact_with_sp2_pop = ((is_sp1 * simulationParameters::beta_sp2_sp1 + is_sp2 * simulationParameters::beta_sp2_sp2) * std::pow(sum_sp2_I, simulationParameters::alpha));
        m_rate["inf_contact_sp2_I"] = headcount.getScount() * foi_contact_with_sp2_pop;

        // inf_contact_sp2_D
        double sum_sp2_D = _metapop_summary.sum_D_neighbour_sp2_previous_timestep[pop_index];
        double foi_contact_with_sp2_pop_D = ((is_sp1 * simulationParameters::beta_sp2D_sp1 + is_sp2 * simulationParameters::beta_sp2D_sp2) * sum_sp2_D);
        m_rate["inf_contact_sp2_D"] = headcount.getScount() * foi_contact_with_sp2_pop_D;

        // start_shedding
        m_rate["start_shedding"] = simulationParameters::delta * headcount.getEcount();

        // onset_of_clinical_signs
        m_rate["onset_of_clinical_signs"] = simulationParameters::sigma * headcount.getIScount();

        // death and removed
        double mu = is_sp1 * simulationParameters::mu_sp1 + is_sp2 * simulationParameters::mu_sp2;
        m_rate["death"] = mu * headcount.getICcount();
        m_rate["removed"] = mu * headcount.getICcount();

        //D_no_more_infectious
        m_rate["D_no_more_infectious"] = simulationParameters::gamma * headcount.getDcount();

        // carcass_discovery_and_removal
        double current_active_search_radius = 0.0;
        if ((_timestep >= simulationParameters::date_on_which_p2measures_are_implemented) & (!is_within_a_fenced_area) & (!is_15km_around_a_fenced_area)) {
            current_active_search_radius = simulationParameters::active_search_radius_outside_fenced_buffer_area_p2measure;
        } else {
            current_active_search_radius = simulationParameters::active_search_radius;
        }
        double max_simultaneous_active_searches = (std::sqrt(3)/4*std::pow((simulationParameters::tile_size/2),2)*6) / (M_PI*std::pow(current_active_search_radius,2));

        if(simulationParameters::get_obs_from_data_for_inference & (_timestep < simulationParameters::date_from_which_to_stop_using_plugged_data)){
            double carcass_discovery_by_passive_surveillance_in_obs = simulationData::sp2_obs_data[pop_id][_timestep].carcass_discovery_by_passive_surveillance;
            double carcass_discovery_by_active_search_in_obs = simulationData::sp2_obs_data[pop_id][_timestep].carcass_discovery_by_active_search;
            double rho_calibrated_on_data = 0.0;
            double rho_passive_search_inside_fenced_buffer_area_calibrated_on_data = 0.0;
            double rho_active_search_calibrated_on_data = 0.0;
            if (headcount.getDcount() > 0) {
                if ((_timestep >= simulationParameters::date_on_which_the_fence_is_built) & (is_within_a_fenced_area | is_15km_around_a_fenced_area)) {
                    rho_passive_search_inside_fenced_buffer_area_calibrated_on_data = carcass_discovery_by_passive_surveillance_in_obs / headcount.getDcount();
                } else {
                    rho_calibrated_on_data = carcass_discovery_by_passive_surveillance_in_obs / headcount.getDcount();
                }
                if (nb_current_ongoing_active_search > 0) {
                    rho_active_search_calibrated_on_data = carcass_discovery_by_active_search_in_obs / (headcount.getDcount() * std::min(nb_current_ongoing_active_search/max_simultaneous_active_searches, 1.0));
                }
            }
            m_rate["carcass_discovery_and_removal_by_passive_surveillance"] = (rho_calibrated_on_data + rho_passive_search_inside_fenced_buffer_area_calibrated_on_data) * headcount.getDcount();
            m_rate["carcass_discovery_and_removal_by_active_search"] = ongoing_active_search * rho_active_search_calibrated_on_data * headcount.getDcount() * std::min(nb_current_ongoing_active_search/max_simultaneous_active_searches, 1.0);
            _metapop_summary.rho_over_time[_timestep][pop_id]["rho_calibrated_on_data"] = rho_calibrated_on_data;
            _metapop_summary.rho_over_time[_timestep][pop_id]["rho_passive_search_inside_fenced_buffer_area_calibrated_on_data"] = rho_passive_search_inside_fenced_buffer_area_calibrated_on_data;
            _metapop_summary.rho_over_time[_timestep][pop_id]["rho_active_search_calibrated_on_data"] = rho_active_search_calibrated_on_data;
            _metapop_summary.rho_over_time[_timestep][pop_id]["carcass_discovery_by_passive_surveillance_calibrated_on_data"] = carcass_discovery_by_passive_surveillance_in_obs;
            _metapop_summary.rho_over_time[_timestep][pop_id]["carcass_discovery_by_active_search_calibrated_on_data"] = carcass_discovery_by_active_search_in_obs;
            _metapop_summary.rho_over_time[_timestep][pop_id]["nb_active_search_calibrated_on_data"] = nb_current_ongoing_active_search;
        } else {
            if ((_timestep >= simulationParameters::date_on_which_the_fence_is_built) & (_timestep <= simulationParameters::date_until_which_hunting_pressure_is_increased)) {
                m_rate["carcass_discovery_and_removal_by_passive_surveillance"] = ((!std::min(is_within_a_fenced_area + is_15km_around_a_fenced_area, 1) * simulationParameters::rho) + (std::min(is_within_a_fenced_area + is_15km_around_a_fenced_area, 1) * simulationParameters::rho_passive_search_inside_fenced_buffer_area)) * headcount.getDcount();
            } else {
                m_rate["carcass_discovery_and_removal_by_passive_surveillance"] = simulationParameters::rho * headcount.getDcount();
            }
            m_rate["carcass_discovery_and_removal_by_active_search"] = ongoing_active_search * simulationParameters::rho_active_search * headcount.getDcount() * std::min(nb_current_ongoing_active_search/max_simultaneous_active_searches, 1.0);
        }

        // hunt
        double eta = simulationParameters::eta;
        if ((_timestep >= simulationParameters::date_on_which_the_fence_is_built) & (_timestep <= simulationParameters::date_until_which_hunting_pressure_is_increased)) {
            eta = ((!std::min(is_within_a_fenced_area + is_15km_around_a_fenced_area, 1) * simulationParameters::eta) + (std::min(is_within_a_fenced_area + is_15km_around_a_fenced_area, 1) * simulationParameters::eta_inside_fenced_buffer_area));
        }
        if (_timestep > simulationParameters::last_date_of_hunting_period) {
            eta = 0.0;
        }
        m_rate["hunt_of_an_S"] = eta * headcount.getScount();
        m_rate["hunt_of_an_E"] = eta * headcount.getEcount();
        m_rate["hunt_of_an_Is"] = eta * headcount.getIScount();
        m_rate["hunt_of_an_Ic"] = eta * headcount.getICcount();

        //
        std::unordered_map<std::string, int> m_potential_hunting_events = {{"hunt_of_an_S", 0}, {"hunt_of_an_E", 0}, {"hunt_of_an_Is", 0}, {"hunt_of_an_Ic", 0}};
        if(simulationParameters::get_obs_from_data_for_inference & (_timestep < simulationParameters::date_from_which_to_stop_using_plugged_data)) {
            double test_on_hunted_ind = simulationData::sp2_obs_data[pop_id][_timestep].test_on_hunted_ind;
            double current_proportion_of_hunted_wb_tested = simulationParameters::proportion_of_hunted_wb_tested;
            if ((_timestep >= simulationParameters::date_on_which_the_fence_is_built) & (is_within_a_fenced_area | is_15km_around_a_fenced_area) & (_timestep <= simulationParameters::date_until_which_hunting_pressure_is_increased)) {
                current_proportion_of_hunted_wb_tested = simulationParameters::proportion_of_hunted_wb_tested_inside_fenced_buffer_area;
            }
            double hunted_ind = test_on_hunted_ind / current_proportion_of_hunted_wb_tested;
            std::vector<std::string> disaggregated_list_of_health_states = headcount.getDisaggregatedListOfHealthStates_SEIsIc();
            std::vector<std::string> vector_of_selected_health_states;
            if (disaggregated_list_of_health_states.size() > hunted_ind) {
                vector_of_selected_health_states = auxFunctions::randomSamplingWithoutReplacement(disaggregated_list_of_health_states, hunted_ind, simulationParameters::random_engine);
            } else {
                vector_of_selected_health_states = disaggregated_list_of_health_states;
            }
            for (const auto& item : vector_of_selected_health_states) {
                if (item == "S") {
                    m_potential_hunting_events["hunt_of_an_S"] += 1;
                } else if (item == "E") {
                    m_potential_hunting_events["hunt_of_an_E"] += 1;
                } else if (item == "Is") {
                    m_potential_hunting_events["hunt_of_an_Is"] += 1;
                } else if (item == "Ic") {
                    m_potential_hunting_events["hunt_of_an_Ic"] += 1;
                }
            }
        }

        // shuffle the events to randomize the order in which they occur.
        std::vector<std::string> shuffled_event_list(ordered_event_list);
        std::shuffle(shuffled_event_list.begin(), shuffled_event_list.end(), simulationParameters::random_engine);
        // iterate on the possible events
        // WARNING: Iterating on events like this can lead to situations where multiple events occur for the same individual (i.e. birth and death, birth and infection, infection and recovery ...) at the same timestep!
        for (const auto& the_event : shuffled_event_list) {
            int nb_change = 0;
            std::vector<std::string> v_event_from_data = { "hunt_of_an_S", "hunt_of_an_E", "hunt_of_an_Is", "hunt_of_an_Ic" };
            if((simulationParameters::get_obs_from_data_for_inference) & (_timestep < simulationParameters::date_from_which_to_stop_using_plugged_data) & (std::count(v_event_from_data.begin(), v_event_from_data.end(), the_event))){
                nb_change = m_potential_hunting_events[the_event];
            } else {
                if (m_rate[the_event] > 0.0) {
                    std::poisson_distribution<> poisson_dist(m_rate[the_event] * simulationParameters::tau);
                    nb_change = poisson_dist(simulationParameters::random_engine);
                }
            }
            // make sure things don't go negative
            std::vector<std::string> negative_transitions = m_change[the_event].getNegativeStates();
            int nb_change_to_use = nb_change;
            if (negative_transitions.size() > 0) {
                for (const auto& states : negative_transitions) {
                    nb_change_to_use = std::min(nb_change_to_use, headcount.get(states));
                }
            }
            // update the counter
            // sirHeadcount change_to_apply = sirHeadcount(m_change[the_event].getScount() * nb_change_to_use, m_change[the_event].getIcount() * nb_change_to_use, m_change[the_event].getRcount() * nb_change_to_use);
            // headcount.update(change_to_apply);
            headcount.update("S", m_change[the_event].getScount() * nb_change_to_use);
            headcount.update("E", m_change[the_event].getEcount() * nb_change_to_use);
            headcount.update("Is", m_change[the_event].getIScount() * nb_change_to_use);
            headcount.update("Ic", m_change[the_event].getICcount() * nb_change_to_use);
            headcount.update("D", m_change[the_event].getDcount() * nb_change_to_use);
            headcount.update("R", m_change[the_event].getRcount() * nb_change_to_use);
            //
            m_events_count[the_event] = nb_change_to_use;

//            if ((the_event == "hunt_of_an_S") & (m_events_count["hunt_of_an_S"] > 0)) {
//                std::cout << _timestep << " -1- " << pop_id << " - " << the_event << " - " << m_events_count["hunt_of_an_S"] << std::endl; // DEBUG
//            }
//            if ((the_event == "hunt_of_an_E") & (m_events_count["hunt_of_an_E"] > 0)) {
//                std::cout << _timestep << " -1- " << pop_id << " - " << the_event << " - " << m_events_count["hunt_of_an_E"] << std::endl; // DEBUG
//            }
//            if ((the_event == "hunt_of_an_Is") & (m_events_count["hunt_of_an_Is"] > 0)) {
//                std::cout << _timestep << " -1- " << pop_id << " - " << the_event << " - " << m_events_count["hunt_of_an_Is"] << std::endl; // DEBUG
//            }
//            if ((the_event == "hunt_of_an_Ic") & (m_events_count["hunt_of_an_Ic"] > 0)) {
//                std::cout << _timestep << " -1- " << pop_id << " - " << the_event << " - " << m_events_count["hunt_of_an_Ic"] << std::endl; // DEBUG
//            }

        }
    }
}

void population::updateMetapopSummary(const double& _timestep, metapopulationSummary& _metapop_summary){
    // update the metapop_summary for the current timestep
//     _metapop_summary.headcount_over_time[_timestep].insert(std::pair<std::string, sirHeadcount > (pop_id, headcount));
    _metapop_summary.headcount_over_time[_timestep][pop_id].update(headcount);
    double current_I = headcount.getIcount(); // TODO: update for ASF
    double current_D = headcount.getDcount(); // TODO: update for ASF
    double current_N = headcount.sum();
    _metapop_summary.headcount_I_current_timestep[pop_index] = current_I;
    _metapop_summary.headcount_D_current_timestep[pop_index] = current_D;
    _metapop_summary.headcount_N_current_timestep[pop_index] = current_N;
    _metapop_summary.headcount_I_N_current_timestep[pop_index] = (current_N > 0) ? (current_I/current_N) : 0;
    _metapop_summary.headcount_D_N_current_timestep[pop_index] = (current_N > 0) ? (current_D/current_N) : 0;
    // keep track of events over time
    _metapop_summary.events_over_time[_timestep][pop_id] = m_events_count;

//    if ((_metapop_summary.events_over_time[_timestep][pop_id]["hunt_of_an_S"] > 0)) {
//        std::cout << _timestep << " -2a- " << pop_id << " - " << "hunt_of_an_S" << " - " << m_events_count["hunt_of_an_S"] << std::endl; // DEBUG
//        std::cout << _timestep << " -2b- " << pop_id << " - " << "hunt_of_an_S" << " - " << _metapop_summary.events_over_time[_timestep][pop_id]["hunt_of_an_S"] << std::endl; // DEBUG
//    }
//    if ((_metapop_summary.events_over_time[_timestep][pop_id]["hunt_of_an_E"] > 0)) {
//        std::cout << _timestep << " -2a- " << pop_id << " - " << "hunt_of_an_E" << " - " << m_events_count["hunt_of_an_E"] << std::endl; // DEBUG
//        std::cout << _timestep << " -2b- " << pop_id << " - " << "hunt_of_an_E" << " - " << _metapop_summary.events_over_time[_timestep][pop_id]["hunt_of_an_E"] << std::endl; // DEBUG
//    }
//    if ((_metapop_summary.events_over_time[_timestep][pop_id]["hunt_of_an_Is"] > 0)) {
//        std::cout << _timestep << " -2a- " << pop_id << " - " << "hunt_of_an_Is" << " - " << m_events_count["hunt_of_an_Is"] << std::endl; // DEBUG
//        std::cout << _timestep << " -2b- " << pop_id << " - " << "hunt_of_an_Is" << " - " << _metapop_summary.events_over_time[_timestep][pop_id]["hunt_of_an_Is"] << std::endl; // DEBUG
//    }
//    if ((_metapop_summary.events_over_time[_timestep][pop_id]["hunt_of_an_Ic"] > 0)) {
//        std::cout << _timestep << " -2a- " << pop_id << " - " << "hunt_of_an_Ic" << " - " << m_events_count["hunt_of_an_Ic"] << std::endl; // DEBUG
//        std::cout << _timestep << " -2b- " << pop_id << " - " << "hunt_of_an_Ic" << " - " << _metapop_summary.events_over_time[_timestep][pop_id]["hunt_of_an_Ic"] << std::endl; // DEBUG
//    }

}

void species1Population::updateStochasticDynamic(const double& _timestep, std::unordered_map<std::string, std::unique_ptr <population>>& _all_populations, metapopulationSummary& _metapop_summary){
//    std::cout << pop_id << " - species1Population::updateStochasticDynamic" << std::endl; // TEST
    //
    if (simulationParameters::sp1_dynamic_from_data) {
        updateSnapshotOfTheDynamic(_timestep, simulationData::sp1_predefined_dynamic[pop_id][_timestep], _metapop_summary);
    } else {
        population::updateStochasticDynamic(_timestep, _all_populations, _metapop_summary);
        // update the metapop_summary for the current timestep
        updateMetapopSummary(_timestep, _metapop_summary);
    }
}

//void species1Population::updateSnapshotOfTheDynamic(const double& _timestep, const sirHeadcount& _sirHeadcount_for_updating, metapopulationSummary& _metapop_summary){
void population::updateSnapshotOfTheDynamic(const double& _timestep, const sirHeadcount& _sirHeadcount_for_updating, metapopulationSummary& _metapop_summary){
    // update headcount based on predefined dynamic data
//    _sirHeadcount_for_updating.show();
    headcount.replace(_sirHeadcount_for_updating);
    // update the metapop_summary for the current timestep
    updateMetapopSummary(_timestep, _metapop_summary);
}

void species2Population::updateStochasticDynamic(const double& _timestep, std::unordered_map<std::string, std::unique_ptr <population>>& _all_populations, metapopulationSummary& _metapop_summary){
//    std::cout << pop_id << " - species2Population::updateStochasticDynamic" << std::endl; // TEST
    // check if active search is ongoing
    ongoing_active_search = false; // set active search to false and then update the value if within an active search period
    nb_current_ongoing_active_search = 0.0;
    std::vector<double> period_to_be_removed;
    if ((_timestep >= simulationParameters::date_on_which_the_fence_is_built) & (is_within_a_fenced_area | is_15km_around_a_fenced_area) & (_timestep <= simulationParameters::date_until_which_hunting_pressure_is_increased)) {
        ongoing_active_search = false;
        nb_current_ongoing_active_search = 0.0;
    } else {
        for (const auto& period : period_of_active_search) { // loop on active search period
            // test if within the active search period under consideration
            if ((_timestep >= period.second[0]) & (_timestep <= period.second[1])) { // if yes:
                ongoing_active_search = true; // turn to true
                nb_current_ongoing_active_search += period.second[2];
            } else if (_timestep > period.second[1]) { // if not, but the period under consideration is exceeded:
                period_to_be_removed.emplace_back(period.first);
            } // else: nothing to do, try next period...
        }
    }
    for (const auto& key_date : period_to_be_removed) { // loop on key_period to be removed from the list
        period_of_active_search.erase(key_date); // remove the period from the list
    }

    // update dynamic
    population::updateStochasticDynamic(_timestep, _all_populations, _metapop_summary);
    // perform test on hunted animals
    if ((_timestep >= simulationParameters::start_date_test_on_hunted_wildboar) & (_timestep <= simulationParameters::last_date_of_hunting_period)) {

        double current_proportion_of_hunted_wb_tested = simulationParameters::proportion_of_hunted_wb_tested;
        if ((_timestep >= simulationParameters::date_on_which_the_fence_is_built) & (is_within_a_fenced_area | is_15km_around_a_fenced_area) & (_timestep <= simulationParameters::date_until_which_hunting_pressure_is_increased)) {
            current_proportion_of_hunted_wb_tested = simulationParameters::proportion_of_hunted_wb_tested_inside_fenced_buffer_area;
        }
//        else {
//            current_proportion_of_hunted_wb_tested = simulationParameters::proportion_of_hunted_wb_tested;
//        }
        
        int hunted_animals_S = m_events_count["hunt_of_an_S"];
        std::binomial_distribution<> bin_S(hunted_animals_S, current_proportion_of_hunted_wb_tested);
        int test_on_hunted_animals_S = bin_S(simulationParameters::random_engine);
        int hunted_animals_EIsIc = m_events_count["hunt_of_an_E"] + m_events_count["hunt_of_an_Is"] + m_events_count["hunt_of_an_Ic"];
        std::binomial_distribution<> bin_EIsIc(hunted_animals_EIsIc, current_proportion_of_hunted_wb_tested);
        int test_on_hunted_animals_EIsIc = bin_EIsIc(simulationParameters::random_engine);
        m_events_count["test_on_hunted_SEIsIc"] = test_on_hunted_animals_S + test_on_hunted_animals_EIsIc;
        m_events_count["positive_test_on_hunted_SEIsIc"] = test_on_hunted_animals_EIsIc;
        if ((m_events_count["hunted_animals_S"] + m_events_count["hunted_animals_EIsIc"]) > 0) {
            std::cout << _timestep << " - " << pop_id << "hunted animals: " << m_events_count["hunted_animals_S"] + m_events_count["hunted_animals_EIsIc"] << std::endl; // DEBUG
        }
//        std::cout << pop_id << "perform test on hunted animals: " << m_events_count["test_on_hunted_SEIsIc"] << " - " << m_events_count["positive_test_on_hunted_SEIsIc"] << std::endl; // TEST
    }
    // update the metapop_summary for the current timestep
    updateMetapopSummary(_timestep, _metapop_summary);

//    if ((_metapop_summary.events_over_time[_timestep][pop_id]["hunt_of_an_S"] > 0)) {
//        std::cout << _timestep << " -3- " << pop_id << " - " << "hunt_of_an_S" << " - " << _metapop_summary.events_over_time[_timestep][pop_id]["hunt_of_an_S"] << std::endl; // DEBUG
//    }
//    if ((_metapop_summary.events_over_time[_timestep][pop_id]["hunt_of_an_E"] > 0)) {
//        std::cout << _timestep << " -3- " << pop_id << " - " << "hunt_of_an_E" << " - " << _metapop_summary.events_over_time[_timestep][pop_id]["hunt_of_an_E"] << std::endl; // DEBUG
//    }
//    if ((_metapop_summary.events_over_time[_timestep][pop_id]["hunt_of_an_Is"] > 0)) {
//        std::cout << _timestep << " -3- " << pop_id << " - " << "hunt_of_an_Is" << " - " << _metapop_summary.events_over_time[_timestep][pop_id]["hunt_of_an_Is"] << std::endl; // DEBUG
//    }
//    if ((_metapop_summary.events_over_time[_timestep][pop_id]["hunt_of_an_Ic"] > 0)) {
//        std::cout << _timestep << " -3- " << pop_id << " - " << "hunt_of_an_Ic" << " - " << _metapop_summary.events_over_time[_timestep][pop_id]["hunt_of_an_Ic"] << std::endl; // DEBUG
//    }

    // check if an active search have to be planned
    // m_events_count contains the count of events that occured during the previous timestep
    double nb_potential_active_search_to_be_planned = m_events_count["positive_test_on_hunted_SEIsIc"] + m_events_count["carcass_discovery_and_removal_by_passive_surveillance"];
    if(simulationParameters::get_obs_from_data_for_inference & (_timestep < simulationParameters::date_from_which_to_stop_using_plugged_data)){
        nb_potential_active_search_to_be_planned = simulationData::sp2_obs_data[pop_id][_timestep].positive_test_on_hunted_ind + simulationData::sp2_obs_data[pop_id][_timestep].carcass_discovery_by_passive_surveillance;
    }
    if ((_timestep >= simulationParameters::date_on_which_the_fence_is_built) & (is_within_a_fenced_area | is_15km_around_a_fenced_area) & (_timestep <= simulationParameters::date_until_which_hunting_pressure_is_increased)) {
        nb_potential_active_search_to_be_planned = 0;
    }
    if (nb_potential_active_search_to_be_planned > 0) {
        double candidate_start_date_of_active_search = _timestep + simulationParameters::time_to_test_results + simulationParameters::time_to_plan_an_active_search + 1;
        double candidate_end_date_of_active_search = _timestep + simulationParameters::time_to_test_results + simulationParameters::time_to_plan_an_active_search + simulationParameters::active_search_duration;
        period_of_active_search[candidate_end_date_of_active_search] = {candidate_start_date_of_active_search, candidate_end_date_of_active_search, nb_potential_active_search_to_be_planned};
    }

    // generate random location for detected infected wild boar in the tile
    double nb_carcasses_to_attibute_location = m_events_count["positive_test_on_hunted_SEIsIc"] + m_events_count["carcass_discovery_and_removal_by_passive_surveillance"] + m_events_count["carcass_discovery_and_removal_by_active_search"];
    for (int i = 0; i < nb_carcasses_to_attibute_location; i++) {
        std::unordered_map<std::string, double> random_coords = auxFunctions::randomPointInHexagon(coord_centroid_X, coord_centroid_Y, simulationParameters::tile_size, simulationParameters::random_engine);
        _metapop_summary.carcasses_location[_timestep][pop_id].emplace_back(random_coords);
    }
}

void population::performOutgoingMovements(const double& _timestep, std::unordered_map<std::string, sirHeadcount>& _list_of_next_pending_movements, metapopulationSummary& _metapop_summary){
    if (simulationData::mvts_network.find(pop_id) != simulationData::mvts_network.end()) {
        // find all date of mvt in the interval ]_timestep-tau,_timestep]
        std::vector<double> list_of_date_to_take_into_account;
        if (simulationParameters::mvt_date_according_to_timestep_tau) {
            if (simulationData::mvts_network[pop_id].find(_timestep) != simulationData::mvts_network[pop_id].end()) {
                list_of_date_to_take_into_account.emplace_back(_timestep);
            }
        } else {
            double min_date = _timestep-simulationParameters::tau;
            double max_date = _timestep;
            for (const auto& date : simulationData::mvts_network[pop_id]) {
                if ((date.first > min_date) & (date.first <= max_date)) {
                    list_of_date_to_take_into_account.emplace_back(date.first);
                }
            }
        }
        // loop on the date
        if (list_of_date_to_take_into_account.size() > 0) {
            for (const auto& the_mvt_date : list_of_date_to_take_into_account) {
                for (const auto& mvts : simulationData::mvts_network[pop_id][the_mvt_date]) {
                    std::string the_destination = mvts.first;
                    int the_batch_size = mvts.second;
                    // Make sure things don't go negative: not moving more indiv. than there is in the pop.
                    int the_batch_size_used = std::min(the_batch_size, headcount.sum());
                    // store the informations on the current movements if nb_mvts_used < nb_mvts
                    // (which would correspond to an insufficient number of individuals in the population)
                    if (the_batch_size_used < the_batch_size) {
                        std::cout << "WARNING: not enough individuals in the population to perform all the movements !" << std::endl;
                        if (_metapop_summary.mvts_not_performed.find(pop_id) != _metapop_summary.mvts_not_performed.end()) {
                            if (_metapop_summary.mvts_not_performed[pop_id].find(the_mvt_date) != _metapop_summary.mvts_not_performed[pop_id].end()) {
                                //_metapop_summary.mvts_not_performed[pop_id][_timestep][the_destination] = the_batch_size_used - the_batch_size;
                                _metapop_summary.mvts_not_performed[pop_id][the_mvt_date].insert(std::pair< std::string, int >(the_destination, the_batch_size_used - the_batch_size));
                            } else {
                                _metapop_summary.mvts_not_performed[pop_id].insert(std::pair< double, std::unordered_map<std::string, int> >(the_mvt_date, std::unordered_map<std::string, int>()));
                                _metapop_summary.mvts_not_performed[pop_id][the_mvt_date].insert(std::pair< std::string, int >(the_destination, the_batch_size_used - the_batch_size));
                            }
                        } else {
                            _metapop_summary.mvts_not_performed.insert(std::pair< std::string, std::unordered_map<double, std::unordered_map<std::string, int>> >(pop_id, std::unordered_map<double, std::unordered_map<std::string, int>>()));
                            _metapop_summary.mvts_not_performed[pop_id].insert(std::pair< double, std::unordered_map<std::string, int> >(the_mvt_date, std::unordered_map<std::string, int>()));
                            _metapop_summary.mvts_not_performed[pop_id][the_mvt_date].insert(std::pair< std::string, int >(the_destination, the_batch_size_used - the_batch_size));
                        }
                    }
                    // population decomposition
                    std::vector<std::string> list_of_states_in_the_pop = headcount.getDisaggregatedListOfHealthStates();
                    // choice of indiv. health states to move
                    sirHeadcount headcount_to_move = headcount.selectXHealthStatesRandomly(the_batch_size_used);
                    // add movements in the pending struct.
                    if (_list_of_next_pending_movements.find(the_destination) != _list_of_next_pending_movements.end()) {
                        _list_of_next_pending_movements[the_destination].update(headcount_to_move);
                    } else {
                        _list_of_next_pending_movements[the_destination] = headcount_to_move;
                    }
                    // update current headcount
                    headcount_to_move.inverse();
                    headcount.update(headcount_to_move);
                }
            }
        }
    }
}

void population::performIngoingMovements(const double& _timestep, const std::unordered_map<std::string, sirHeadcount>& _list_of_current_pending_movements, metapopulationSummary& _metapop_summary){
    if (_list_of_current_pending_movements.find(pop_id) != _list_of_current_pending_movements.end()) {
//        // get the headcount for updating
//        sirHeadcount headcount_to_move = _list_of_current_pending_movements.at(pop_id);
//        // update the headcount by adding the movements
//        headcount.update(headcount_to_move);
        // replace the two line above by only one
        headcount.update(_list_of_current_pending_movements.at(pop_id));
    }
}

void population::displayCurrentStatus(){
    // display the population id
    std::cout << "Current status for pop. " << pop_id << std::endl;
    // display the population type
    std::cout << "\tpop_species: " << pop_species << std::endl;
    // display the total number of indiv. in the population
    std::cout << "\ttotal number of indiv: " << headcount.sum() << std::endl;
    // display population composition
    std::cout << "\tpop. composition: " << std::endl;
    // if using sirHeadcount
    std::cout << "\t\t S: " << headcount.getScount() << std::endl;
    std::cout << "\t\t I: " << headcount.getIcount() << std::endl;
    std::cout << "\t\t R: " << headcount.getRcount() << std::endl;
    // if using sirHeadcount_map
//    for (auto item : headcount.headcount) {
//        std::cout << "\t\t" << item.first << ": " << item.second << std::endl;
//    }
}
