#!/usr/bin/env python
# -*- coding: utf-8 -*-

##
# functions for the parameter estimation using the simplex (Nelder-Mead method) for the ParaTB metapop. model and serological survey data
#

import sys
import numpy as np
import pandas as pd
# import copy
import time
# import csv
import math
import os
import glob
from pathlib import Path
# import subprocess

import __main__

pd.set_option('display.max_rows', 30)
pd.set_option('display.max_columns', 30)
pd.set_option('display.width', 300)

################################################################################
################################################################################

# fonction permettant de passer d'une valeur entre - et + l'infini à la valeur de notre paramètre dans son domaine de definition compris entre ymin et ymax
def fromSimplexValToParamVal(theVal, theMinVal, theMaxVal):
    # rescale the value with a logistique transformation
    # ymin + (ymax-ymin)/(1+e(-x))

    theRescaledVal = theMinVal + ((theMaxVal - theMinVal) / (1 + math.exp(-theVal)))

    return theRescaledVal


################################################################################
################################################################################

# fonction permettant de passer de notre valeur de paramètre compris entre ymin et ymax à une valeur entre - et + l'infini
def fromParamValToSimplexVal(theVal, theMinVal, theMaxVal):
    # theRescaleRescaledVal = -math.log(((theMaxVal - theMinVal) / (theVal - theMinVal)) - 1)

    val = theVal
    theRescaleRescaledVal = 0

    if theMinVal == theMaxVal:
        theRescaleRescaledVal = theMinVal
        # print("THEMINVAL == THEMAXVAL !!", flush=True)
    elif val == theMinVal:
        val = theMinVal + 1e-09
        print("VAL == THEMINVAL !!", flush=True)
        theRescaleRescaledVal = math.log((val - theMinVal) / (theMaxVal - val))
    elif val == theMaxVal:
        val = theMaxVal - 1e-09
        print("VAL == THEMAXVAL !!", flush=True)
        theRescaleRescaledVal = math.log((val - theMinVal) / (theMaxVal - val))
    else:
        theRescaleRescaledVal = math.log((val - theMinVal) / (theMaxVal - val))

    return theRescaleRescaledVal


################################################################################
################################################################################


def loadObservedData(theFilePath):
    obs = pd.read_csv(theFilePath)
    return obs


################################################################################
################################################################################


def loadSimulatedData(theFilePath):
    sim = pd.read_csv(theFilePath)
    return sim


################################################################################
################################################################################


def computeCriterion(obsData, simuData, rep_id, period_length, prev):
    criterion = 0.0

    # TODO: sum over the period and zone in consideration of:
    # squared prediction error of #wb_carcasses, divided by the variance of the observations
    # +
    # squared prediction error of #positive_hunted_wb, divided by the variance of the observations

    # définir la période d'aggregation: 5
    # définir l'aggragation spatial -> d'après obs + fichier avec les voisins

    ################################################################################
    # group by period and 'zone'
    neighbours = pd.read_csv(__main__.pathToTheModelDataFolder + "/network_contact_with_WB_pop_all.csv")
    inf_cell_WB = set(obsData.loc[(obsData.carcass_discovery_and_removal_by_passive_surveillance > 0) | (obsData.carcass_discovery_and_removal_by_active_search > 0) | (obsData.positive_test_on_hunted_SEIsIc > 0)].pop_id.values)
    DP_pop_dyn = pd.read_csv(__main__.pathToTheModelDataFolder + "/predefined_dynamic_DP_pop_" + str(rep_id) + ".csv")
    inf_cell_DP = set(DP_pop_dyn.loc[(DP_pop_dyn.status != 'S') & (DP_pop_dyn.status != 'R')].id.values)
    inf_cell = inf_cell_WB | inf_cell_DP
    pop_in_zone = set(neighbours.loc[neighbours.pop_id.isin(inf_cell)].neighbours.values)
    pop_in_zone = pop_in_zone | inf_cell_WB

    new_obs = obsData.copy()
    new_sim = simuData[["pop_id", "date", "carcass_discovery_and_removal_by_passive_surveillance", "carcass_discovery_and_removal_by_active_search", "test_on_hunted_SEIsIc", "positive_test_on_hunted_SEIsIc"]].copy()

    new_obs.loc[~new_obs.pop_id.isin(pop_in_zone), "pop_id"] = "WB_EXT"
    new_obs.date = [math.ceil(i) for i in new_obs.date.values / period_length]
    new_obs = new_obs.groupby(["pop_id", "date"], as_index=False)[["carcass_discovery_and_removal_by_passive_surveillance", "carcass_discovery_and_removal_by_active_search", "test_on_hunted_SEIsIc", "positive_test_on_hunted_SEIsIc"]].sum()
    new_obs.sort_values(by=["pop_id", "date"], inplace=True)

    new_sim.loc[~new_sim.pop_id.isin(pop_in_zone), "pop_id"] = "WB_EXT"
    new_sim.date = [math.ceil(i) for i in new_sim.date.values / period_length]
    new_sim = new_sim.groupby(["pop_id", "date"], as_index=False)[["carcass_discovery_and_removal_by_passive_surveillance", "carcass_discovery_and_removal_by_active_search", "test_on_hunted_SEIsIc", "positive_test_on_hunted_SEIsIc"]].sum()
    new_sim.sort_values(by=["pop_id", "date"], inplace=True)

    C_var = np.var(new_obs.carcass_discovery_and_removal_by_passive_surveillance.values + new_obs.carcass_discovery_and_removal_by_active_search.values)

    PT_var = 1
    if prev:
        PT_var = np.var([(new_obs.positive_test_on_hunted_SEIsIc.values[i] / new_obs.test_on_hunted_SEIsIc.values[i]) if new_obs.test_on_hunted_SEIsIc.values[i] > 0 else 0.0 for i in range(len(new_obs))])
    else:
        PT_var = np.var(new_obs.positive_test_on_hunted_SEIsIc.values)

    ################################################################################
    # compute criterion

    C_part = ((new_sim.carcass_discovery_and_removal_by_passive_surveillance.values + new_sim.carcass_discovery_and_removal_by_active_search.values) - (new_obs.carcass_discovery_and_removal_by_passive_surveillance.values + new_obs.carcass_discovery_and_removal_by_active_search.values))**2 / C_var

    PT_part = 0.0
    if prev:
        PT_part = (np.nan_to_num(new_sim.positive_test_on_hunted_SEIsIc.values / new_sim.test_on_hunted_SEIsIc.values) - np.nan_to_num(new_obs.positive_test_on_hunted_SEIsIc.values / new_obs.test_on_hunted_SEIsIc.values))**2 / PT_var
    else:
        PT_part = (new_sim.positive_test_on_hunted_SEIsIc.values - new_obs.positive_test_on_hunted_SEIsIc.values)**2 / PT_var

    criterion += sum(C_part) + sum(PT_part)

    ################################################################################
    # return the criterion
    return criterion


def computeCriterionCoarseGrain_old(obsData, simuData, rep_id, period_length, prev):
    criterion = 0.0

    # TODO: sum over the period and zone in consideration of:
    # squared prediction error of #wb_carcasses, divided by the variance of the observations
    # +
    # squared prediction error of #positive_hunted_wb, divided by the variance of the observations

    # définir la période d'aggregation: 5
    # définir l'aggragation spatial -> d'après obs + fichier avec les voisins

    ################################################################################
    # group by period and 'zone'
    neighbours = pd.read_csv(__main__.pathToTheModelDataFolder + "/network_contact_with_WB_pop_all.csv")
    inf_cell_WB = set(obsData.loc[(obsData.carcass_discovery_and_removal_by_passive_surveillance > 0) | (obsData.carcass_discovery_and_removal_by_active_search > 0) | (obsData.positive_test_on_hunted_SEIsIc > 0)].pop_id.values)
    DP_pop_dyn = pd.read_csv(__main__.pathToTheModelDataFolder + "/predefined_dynamic_DP_pop_" + str(rep_id) + ".csv")
    inf_cell_DP = set(DP_pop_dyn.loc[(DP_pop_dyn.status != 'S') & (DP_pop_dyn.status != 'R')].id.values)
    inf_cell = inf_cell_WB | inf_cell_DP
    pop_in_zone = set(neighbours.loc[neighbours.pop_id.isin(inf_cell)].neighbours.values)
    pop_in_zone = pop_in_zone | inf_cell_WB
    inf_cell_neighbours = pop_in_zone.difference(inf_cell)

    new_obs = obsData.copy()
    new_sim = simuData[["pop_id", "date", "carcass_discovery_and_removal_by_passive_surveillance", "carcass_discovery_and_removal_by_active_search", "test_on_hunted_SEIsIc", "positive_test_on_hunted_SEIsIc"]].copy()

    new_obs.loc[~new_obs.pop_id.isin(pop_in_zone), "pop_id"] = "WB_EXT"
    new_obs.loc[new_obs.pop_id.isin(inf_cell), "pop_id"] = "INF_CEL"
    new_obs.loc[new_obs.pop_id.isin(inf_cell_neighbours), "pop_id"] = "INF_NGH"
    new_obs.date = [math.ceil(i) for i in new_obs.date.values / period_length]
    new_obs = new_obs.groupby(["pop_id", "date"], as_index=False)[["carcass_discovery_and_removal_by_passive_surveillance", "carcass_discovery_and_removal_by_active_search", "test_on_hunted_SEIsIc", "positive_test_on_hunted_SEIsIc"]].sum()
    new_obs.sort_values(by=["pop_id", "date"], inplace=True)

    new_sim.loc[~new_sim.pop_id.isin(pop_in_zone), "pop_id"] = "WB_EXT"
    new_sim.loc[new_sim.pop_id.isin(inf_cell), "pop_id"] = "INF_CEL"
    new_sim.loc[new_sim.pop_id.isin(inf_cell_neighbours), "pop_id"] = "INF_NGH"
    new_sim.date = [math.ceil(i) for i in new_sim.date.values / period_length]
    new_sim = new_sim.groupby(["pop_id", "date"], as_index=False)[["carcass_discovery_and_removal_by_passive_surveillance", "carcass_discovery_and_removal_by_active_search", "test_on_hunted_SEIsIc", "positive_test_on_hunted_SEIsIc"]].sum()
    new_sim.sort_values(by=["pop_id", "date"], inplace=True)

    C_var = np.var(new_obs.carcass_discovery_and_removal_by_passive_surveillance.values + new_obs.carcass_discovery_and_removal_by_active_search.values)

    PT_var = 1
    if prev:
        PT_var = np.var([(new_obs.positive_test_on_hunted_SEIsIc.values[i] / new_obs.test_on_hunted_SEIsIc.values[i]) if new_obs.test_on_hunted_SEIsIc.values[i] > 0 else 0.0 for i in range(len(new_obs))])
    else:
        PT_var = np.var(new_obs.positive_test_on_hunted_SEIsIc.values)

    ################################################################################
    # compute criterion

    C_part = ((new_sim.carcass_discovery_and_removal_by_passive_surveillance.values + new_sim.carcass_discovery_and_removal_by_active_search.values) - (new_obs.carcass_discovery_and_removal_by_passive_surveillance.values + new_obs.carcass_discovery_and_removal_by_active_search.values))**2 / C_var

    PT_part = 0.0
    if prev:
        PT_part = (np.nan_to_num(new_sim.positive_test_on_hunted_SEIsIc.values / new_sim.test_on_hunted_SEIsIc.values) - np.nan_to_num(new_obs.positive_test_on_hunted_SEIsIc.values / new_obs.test_on_hunted_SEIsIc.values))**2 / PT_var
    else:
        PT_part = (new_sim.positive_test_on_hunted_SEIsIc.values - new_obs.positive_test_on_hunted_SEIsIc.values)**2 / PT_var

    criterion += sum(C_part) + sum(PT_part)

    ################################################################################
    # return the criterion
    return criterion


def computeCriterionCoarseGrain(obsData, simuData, rep_id, period_length, prev):
    criterion = 0.0

    # TODO: sum over the period and zone in consideration of:
    # squared prediction error of #wb_carcasses, divided by the variance of the observations
    # +
    # squared prediction error of #positive_hunted_wb, divided by the variance of the observations

    # définir la période d'aggregation: 5
    # définir l'aggragation spatial -> d'après obs + fichier avec les voisins

    ################################################################################
    # group by period and 'zone'
    neighbours = pd.read_csv(__main__.pathToTheModelDataFolder + "/network_contact_with_WB_pop_all.csv")
    inf_cell_WB = set(obsData.loc[(obsData.carcass_discovery_and_removal_by_passive_surveillance > 0) | (obsData.carcass_discovery_and_removal_by_active_search > 0) | (obsData.positive_test_on_hunted_SEIsIc > 0)].pop_id.values)
    DP_pop_dyn = pd.read_csv(__main__.pathToTheModelDataFolder + "/predefined_dynamic_DP_pop_" + str(rep_id) + ".csv")
    inf_cell_DP = set(DP_pop_dyn.loc[(DP_pop_dyn.status != 'S') & (DP_pop_dyn.status != 'R')].id.values)
    inf_cell = inf_cell_WB | inf_cell_DP
    pop_in_zone = set(neighbours.loc[neighbours.pop_id.isin(inf_cell)].neighbours.values)
    pop_in_zone = pop_in_zone | inf_cell_WB
    inf_cell_neighbours = pop_in_zone.difference(inf_cell)

    new_obs = obsData.copy()
    new_sim = simuData[["pop_id", "date", "carcass_discovery_and_removal_by_passive_surveillance", "carcass_discovery_and_removal_by_active_search", "test_on_hunted_SEIsIc", "positive_test_on_hunted_SEIsIc"]].copy()

    new_obs.loc[~new_obs.pop_id.isin(pop_in_zone), "pop_id"] = "WB_EXT"
    new_obs.loc[new_obs.pop_id.isin(inf_cell), "pop_id"] = "INF_CEL"
    new_obs.loc[new_obs.pop_id.isin(inf_cell_neighbours), "pop_id"] = "INF_NGH"

    new_obs_prt1 = new_obs.loc[new_obs["date"] <= 59].copy()
    new_obs_prt1.date = [math.ceil(i) for i in new_obs_prt1.date.values / period_length]
    new_obs_prt1 = new_obs_prt1.groupby(["pop_id", "date"], as_index=False)[["carcass_discovery_and_removal_by_passive_surveillance", "carcass_discovery_and_removal_by_active_search", "test_on_hunted_SEIsIc", "positive_test_on_hunted_SEIsIc"]].sum()
    new_obs_prt1.sort_values(by=["pop_id", "date"], inplace=True)

    new_obs_prt2 = new_obs.loc[new_obs["date"] >= 60].copy()
    new_obs_prt2.date = [math.ceil(i) for i in new_obs_prt2.date.values / period_length]
    new_obs_prt2 = new_obs_prt2.groupby(["pop_id", "date"], as_index=False)[["carcass_discovery_and_removal_by_passive_surveillance", "carcass_discovery_and_removal_by_active_search", "test_on_hunted_SEIsIc", "positive_test_on_hunted_SEIsIc"]].sum()
    new_obs_prt2.sort_values(by=["pop_id", "date"], inplace=True)

    new_sim.loc[~new_sim.pop_id.isin(pop_in_zone), "pop_id"] = "WB_EXT"
    new_sim.loc[new_sim.pop_id.isin(inf_cell), "pop_id"] = "INF_CEL"
    new_sim.loc[new_sim.pop_id.isin(inf_cell_neighbours), "pop_id"] = "INF_NGH"

    new_sim_prt1 = new_sim.loc[new_sim["date"] <= 59].copy()
    new_sim_prt1.date = [math.ceil(i) for i in new_sim_prt1.date.values / period_length]
    new_sim_prt1 = new_sim_prt1.groupby(["pop_id", "date"], as_index=False)[["carcass_discovery_and_removal_by_passive_surveillance", "carcass_discovery_and_removal_by_active_search", "test_on_hunted_SEIsIc", "positive_test_on_hunted_SEIsIc"]].sum()
    new_sim_prt1.sort_values(by=["pop_id", "date"], inplace=True)

    new_sim_prt2 = new_sim.loc[new_sim["date"] >= 60].copy()
    new_sim_prt2.date = [math.ceil(i) for i in new_sim_prt2.date.values / period_length]
    new_sim_prt2 = new_sim_prt2.groupby(["pop_id", "date"], as_index=False)[["carcass_discovery_and_removal_by_passive_surveillance", "carcass_discovery_and_removal_by_active_search", "test_on_hunted_SEIsIc", "positive_test_on_hunted_SEIsIc"]].sum()
    new_sim_prt2.sort_values(by=["pop_id", "date"], inplace=True)

    C_var_prt1 = np.var(new_obs_prt1.carcass_discovery_and_removal_by_passive_surveillance.values + new_obs_prt1.carcass_discovery_and_removal_by_active_search.values)
    C_var_prt2 = np.var(new_obs_prt2.carcass_discovery_and_removal_by_passive_surveillance.values + new_obs_prt2.carcass_discovery_and_removal_by_active_search.values)

    PT_var_prt1 = 1
    if prev:
        PT_var_prt1 = np.var([(new_obs_prt1.positive_test_on_hunted_SEIsIc.values[i] / new_obs_prt1.test_on_hunted_SEIsIc.values[i]) if new_obs_prt1.test_on_hunted_SEIsIc.values[i] > 0 else 0.0 for i in range(len(new_obs_prt1))])
    else:
        PT_var_prt1 = np.var(new_obs_prt1.positive_test_on_hunted_SEIsIc.values)

    PT_var_prt2 = 1
    if prev:
        PT_var_prt2 = np.var([(new_obs_prt2.positive_test_on_hunted_SEIsIc.values[i] / new_obs_prt2.test_on_hunted_SEIsIc.values[i]) if new_obs_prt2.test_on_hunted_SEIsIc.values[i] > 0 else 0.0 for i in range(len(newnew_obs_prt2_obs))])
    else:
        PT_var_prt2 = np.var(new_obs_prt2.positive_test_on_hunted_SEIsIc.values)

    ################################################################################
    # compute criterion

    C_part_prt1 = ((new_sim_prt1.carcass_discovery_and_removal_by_passive_surveillance.values + new_sim_prt1.carcass_discovery_and_removal_by_active_search.values) - (new_obs_prt1.carcass_discovery_and_removal_by_passive_surveillance.values + new_obs_prt1.carcass_discovery_and_removal_by_active_search.values))**2 / C_var_prt1

    C_part_prt2 = ((new_sim_prt2.carcass_discovery_and_removal_by_passive_surveillance.values + new_sim_prt2.carcass_discovery_and_removal_by_active_search.values) - (new_obs_prt2.carcass_discovery_and_removal_by_passive_surveillance.values + new_obs_prt2.carcass_discovery_and_removal_by_active_search.values))**2 / C_var_prt2

    PT_part_prt1 = 0.0
    if prev:
        PT_part_prt1 = (np.nan_to_num(new_sim_prt1.positive_test_on_hunted_SEIsIc.values / new_sim_prt1.test_on_hunted_SEIsIc.values) - np.nan_to_num(new_obs_prt1.positive_test_on_hunted_SEIsIc.values / new_obs_prt1.test_on_hunted_SEIsIc.values))**2 / PT_var_prt1
    else:
        PT_part_prt1 = (new_sim_prt1.positive_test_on_hunted_SEIsIc.values - new_obs_prt1.positive_test_on_hunted_SEIsIc.values)**2 / PT_var_prt1

    PT_part_prt2 = 0.0
    if prev:
        PT_part_prt2 = (np.nan_to_num(new_sim_prt2.positive_test_on_hunted_SEIsIc.values / new_sim_prt2.test_on_hunted_SEIsIc.values) - np.nan_to_num(new_obs_prt2.positive_test_on_hunted_SEIsIc.values / new_obs_prt2.test_on_hunted_SEIsIc.values))**2 / PT_var_prt2
    else:
        PT_part_prt2 = (new_sim_prt2.positive_test_on_hunted_SEIsIc.values - new_obs_prt2.positive_test_on_hunted_SEIsIc.values)**2 / PT_var_prt2

    criterion += sum(C_part_prt1) + sum(C_part_prt2) + sum(PT_part_prt1) + sum(PT_part_prt2)

    ################################################################################
    # return the criterion
    return criterion


################################################################################
################################################################################


def computeCriterionForGivenParameters(param):
    criterion = 0.0

    theNumberOfRuns = __main__.numberOfRunsPerIteration
    nbRunsPerJob = 1

    ################################################################################
    # rescale the parameter value receive from the simplex:
    val_param = {}

    for pName in __main__.paramName:
        the_index = __main__.paramName.index(pName)
        the_val = fromSimplexValToParamVal(param[the_index], __main__.minVal[the_index], __main__.maxVal[the_index])
        val_param[pName] = the_val

    beta_sp2 = val_param["beta_sp2"]  # contamnation intra due to Is + Ic
    beta_sp2D = val_param["beta_sp2D"]  # contamnation intra due to D
    beta_sp1_sp2 = val_param["beta_sp1_sp2"]  # contamination des WB par DP
    beta_sp2_sp2 = val_param["beta_sp2_sp2"]  # contamination des WB par WB
    beta_sp2D_sp2 = val_param["beta_sp2D_sp2"]  # contamination des WB par WB_D
    # rho = val_param["rho"]  # carcass discovery rate with passive surveillance
    # rho_active_search = val_param["rho_active_search"]  # carcass discovery rate with active search

    ################################################################################
    # launch the model with rescaled parameters
    command_qsub = 'qsub -e /dev/null -o /dev/null -sync y -q "maiage.q|short.q" -t 1-' + str(theNumberOfRuns) + ':' + str(nbRunsPerJob) + ' ' + './simplex_WB/launchWBDynamicComputation.sh' + " " + str(beta_sp2) + " " + str(beta_sp2D) + " " + str(beta_sp1_sp2) + " " + str(beta_sp2_sp2) + " " + str(beta_sp2D_sp2) + " " + __main__.pathToTheModelDataFolder + " " + __main__.pathOfTheResultsFolder + "/temp" + " " + str(__main__.tile_size) + " " + str(__main__.sim_duration) + " " + str(__main__.date_on_which_the_fence_is_built) + " " + str(__main__.date_from_which_to_stop_using_plugged_data)

    os.system(command_qsub)

    # part below no longer needed, because using '-sync y' arg in the qsub command
    # # Wait until all the trajectories have been completed
    # theNumberOfResults = 0
    # while theNumberOfResults < (__main__.numberOfRunsPerIteration):
    #     theNumberOfResults = 0
    #     time.sleep(10)
    #     for rep_id in range(1, (__main__.numberOfRunsPerIteration + 1)):
    #         theFileName = "wild_boar_events_dynamic_rep_"
    #         theResultsFilePath = Path(theSubResultsFolderPath + '/' + theFileName + str(rep_id) + ".csv")
    #         if theResultsFilePath.is_file():
    #             theNumberOfResults += 1

    ################################################################################
    # compute the criterion
    obs_data = loadObservedData(__main__.pathOfTheDataFolder + "/obs_wild_boar_events_dynamic.csv")
    criterion = 0.0
    exp = glob.glob(__main__.pathOfTheResultsFolder +"/temp/" + "experiment_seed_*/wild_boar_events_dynamic_rep_*")
    for rep_id in range(1, (theNumberOfRuns + 1)):
        # load simulation
        simu_data = loadSimulatedData(exp[rep_id-1])
        # compute criterion
        criterion += computeCriterionCoarseGrain(obs_data, simu_data, rep_id, __main__.periodLengthForCritAggreg, __main__.criterion_based_on_PTprevalence)

    # compute the mean over the runs
    criterion = criterion / theNumberOfRuns

    ################################################################################
    # cleaning of temp results used to compute the criterion for this iteration
    command_rm_tempSimulationResultsFolderPath = 'rm -rf ' + __main__.pathOfTheResultsFolder + "/temp/*"
    os.system(command_rm_tempSimulationResultsFolderPath)

    ################################################################################
    return criterion


################################################################################
################################################################################

def create_a_regular_initial_simplex(x0):
    """Create an initial simplex from a vector of values."""
    ############################################################################
    sigma = 0.15  # must be < 0.5

    x0 = np.asfarray(x0).flatten()

    N = len(x0)

    sim = np.zeros((N + 1, N), dtype=x0.dtype)
    sim[0] = x0
    for k in range(1, N + 1):
        x = np.array(x0, copy=True)

        for i in range(1, N + 1):
            alpha = (math.sqrt(i + 1) - 1) / (i * math.sqrt(2))
            beta = 1 / math.sqrt(2)

            theminVal = __main__.minVal[i - 1]
            themaxVal = __main__.maxVal[i - 1]
            theY = fromSimplexValToParamVal(x[i - 1], theminVal, themaxVal)

            if theY > ((theminVal + themaxVal) / 2):
                theNewY = theY - (sigma * (themaxVal - theminVal))
            else:
                theNewY = theY + (sigma * (themaxVal - theminVal))

            theNewX = fromParamValToSimplexVal(theNewY, theminVal, themaxVal)

            lambda_i = theNewX - x[i - 1]
            c_i = lambda_i / (alpha + beta)
            mu_i = c_i * alpha

            if i == k:
                x[i - 1] = x[i - 1] + lambda_i
            else:
                x[i - 1] = x[i - 1] + mu_i

        sim[k] = x

    # place the point at the center of the simplex
    # sim = sim + (sim[0] - sim.mean(0))

    return sim


################################################################################
################################################################################
