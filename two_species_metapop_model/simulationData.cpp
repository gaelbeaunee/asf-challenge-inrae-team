//
//  simulationData.cpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#include "simulationData.hpp"

// Static variables
std::unordered_map<std::string, populationCharacteristicsUnit> simulationData::pop_characteristics;
std::vector<std::string> simulationData::all_pop_id_list;
std::unordered_map<std::string, int> simulationData::map_of_pop_index;
std::set<std::string> simulationData::sp1_pop_id_list;
std::set<std::string> simulationData::sp2_pop_id_list;
std::unordered_map<std::string, std::unordered_map<double, std::unordered_map<std::string, int>>> simulationData::mvts_network;
std::unordered_map<std::string, std::set<std::string>> simulationData::network_contact_with_sp1_pop;
std::unordered_map<std::string, std::set<std::string>> simulationData::network_contact_with_sp2_pop;
arma::mat simulationData::mat_contact_sp1_pop;
arma::mat simulationData::mat_contact_sp2_pop;
std::unordered_map<std::string, std::unordered_map<double, sirHeadcount>> simulationData::sp1_predefined_dynamic;
std::unordered_map<std::string, std::unordered_map<double, sirHeadcount>> simulationData::sp2_predefined_dynamic;
std::unordered_map<std::string, std::unordered_map<double, observedDataPoint>> simulationData::sp2_obs_data;

// Constructor
simulationData *simulationData::instance = NULL;

simulationData& simulationData::get_instance(){
    if (instance == NULL) {
        instance = new simulationData();
    }
    return *instance;
}

void simulationData::kill(){
    if (instance != NULL) {
        delete instance;
    }
}

simulationData::simulationData(){
    Chronometer chrono_data;
    chrono_data.start();

    loadPopulationCharacteristics();
    loadMovementNetworkData();
    loadContactNetworkData();
    buildContactMatrix();
    loadDynamicData();
    loadObsData();

    chrono_data.stop();
    std::cout << "simulationData (time of construction): " << chrono_data.displayInSecond() << " seconds" << std::endl;
}

// Member functions
void simulationData::loadPopulationCharacteristics(){
    Chronometer chrono_load_data_pop_characteristics;
    chrono_load_data_pop_characteristics.start();
    if (simulationParameters::verbose_mode) { std::cout << "Construction of the population characteristics data structure: "; }
    std::ifstream population_characteristics_file(simulationParameters::data_dir + '/' + "pop_characteristics.csv", std::fstream::in); // Open file in reading mode
    if(!population_characteristics_file){std::cerr << "[ERROR] Unable to open the file 'pop_characteristics.csv' !!" << std::endl;} // Check if the file is open!
    //
    std::string line_in_the_dataset;
    // Skip the first line, which corresponds to the file header
    std::getline(population_characteristics_file, line_in_the_dataset);
    // For each line:
    int pop_index_counter = 0;
    while (std::getline(population_characteristics_file, line_in_the_dataset)){
        // Construction of a line
        std::stringstream ss(line_in_the_dataset);
        // split the elements of the line
        std::vector<std::string> splited_elements;
        std::string token;
        while(std::getline(ss, token, ',')) {
            splited_elements.emplace_back(token);
        }
        // Data extraction for the line
        std::string pop_id = splited_elements[0];
        std::string pop_type = splited_elements[1];
        int pop_size = std::stoi(splited_elements[2]);
        double coord_centroid_X = std::stof(splited_elements[3]);
        double coord_centroid_Y = std::stof(splited_elements[4]);
        int in_fenced_area = std::stof(splited_elements[5]);
        int in_15km_around_fenced_area = std::stof(splited_elements[6]);

        // build population characteristics unit
        populationCharacteristicsUnit pop_char_unit = populationCharacteristicsUnit(pop_id, pop_type, pop_size, coord_centroid_X, coord_centroid_Y, in_fenced_area, in_15km_around_fenced_area);
        // add the unit in the pop_characteristics structure
        pop_characteristics.insert(std::pair<std::string, populationCharacteristicsUnit>(pop_id,pop_char_unit));
        //
        all_pop_id_list.emplace_back(pop_id);
        map_of_pop_index[pop_id] = pop_index_counter;
        pop_index_counter ++;
        if (pop_type == simulationParameters::sp1_name) {
            sp1_pop_id_list.insert(pop_id);
        } else if (pop_type == simulationParameters::sp2_name) {
            sp2_pop_id_list.insert(pop_id);
        } else {
            std::cout << "WARNING - this population type is unknown: " << pop_type << ", sp1: " << simulationParameters::sp1_name << ", sp2: " << simulationParameters::sp2_name << " !" << std::endl;
        }
    }
    population_characteristics_loaded = true;
    chrono_load_data_pop_characteristics.stop();
    if (simulationParameters::verbose_mode) { std::cout << chrono_load_data_pop_characteristics.displayInSecond() << " seconds" << std::endl; }
}

void simulationData::loadMovementNetworkData(){
    Chronometer chrono_load_data_mvts_network;
    chrono_load_data_mvts_network.start();
    if (simulationParameters::verbose_mode) { std::cout << "Construction of the movement network data structure: "; }
    std::ifstream mvts_network_file(simulationParameters::data_dir + '/' + "mvts_network_" + std::to_string(simulationParameters::first_run_index) + ".csv", std::fstream::in); // Open file in reading mode
    if(!mvts_network_file){std::cerr << "[ERROR] Unable to open the file 'mvts_network.csv' !!" << std::endl;} // Check if the file is open!
    //
    std::string line_in_the_dataset;
    // Skip the first line, which corresponds to the file header
    std::getline(mvts_network_file, line_in_the_dataset);
    // For each line:
    while (std::getline(mvts_network_file, line_in_the_dataset)){
        // Construction of a line
        std::stringstream ss(line_in_the_dataset);
        // split the elements of the line
        std::vector<std::string> splited_elements;
        std::string token;
        while(std::getline(ss, token, ',')) {
            splited_elements.emplace_back(token);
        }
        // Data extraction for the line
        std::string source = splited_elements[0];
        std::string target = splited_elements[1];
        double date = std::stod(splited_elements[2]);
        double weight = std::stod(splited_elements[3]);

        if (mvts_network.find(source) != mvts_network.end()) {
            if (mvts_network[source].find(date) != mvts_network[source].end()) {
                //_metapop_summary.mvts_not_performed[pop_id][_timestep][the_destination] = the_batch_size_used - the_batch_size;
                mvts_network[source][date].insert(std::pair< std::string, int >(target, weight));
            } else {
                mvts_network[source].insert(std::pair< double, std::unordered_map<std::string, int> >(date, std::unordered_map<std::string, int>()));
                mvts_network[source][date].insert(std::pair< std::string, int >(target, weight));
            }
        } else {
            mvts_network.insert(std::pair< std::string, std::unordered_map<double, std::unordered_map<std::string, int>> >(source, std::unordered_map<double, std::unordered_map<std::string, int>>()));
            mvts_network[source].insert(std::pair< double, std::unordered_map<std::string, int> >(date, std::unordered_map<std::string, int>()));
            mvts_network[source][date].insert(std::pair< std::string, int >(target, weight));
        }
    }
//    // DEBUG: display mvts
//    for (auto source : mvts_network) {
//        for (auto date : source.second) {
//            for (auto destination : date.second) {
//                std::cout << source.first << " - " << destination.first << " - " << date.first << " - " << destination.second << std::endl;
//            }
//        }
//    }
    movement_network_data_loaded = true;
    chrono_load_data_mvts_network.stop();
    if (simulationParameters::verbose_mode) { std::cout << chrono_load_data_mvts_network.displayInSecond() << " seconds" << std::endl; }
}

void simulationData::loadContactNetworkData(){
    // network_contact_with_sp1_pop - network_contact_with_sp1_pop.csv
    Chronometer chrono_load_data_network_contact_with_sp1_pop;
    chrono_load_data_network_contact_with_sp1_pop.start();
    if (simulationParameters::verbose_mode) { std::cout << "Construction of the contact (with DP) network data structure: "; }
    std::ifstream network_contact_with_sp1_pop_file(simulationParameters::data_dir + '/' + "network_contact_with_DP_pop.csv", std::fstream::in); // Open file in reading mode
    if(!network_contact_with_sp1_pop_file){std::cerr << "[ERROR] Unable to open the file 'network_contact_with_DP_pop.csv' !!" << std::endl;} // Check if the file is open!
    //
    std::string line_in_the_dataset_sp1;
    // Skip the first line, which corresponds to the file header
    std::getline(network_contact_with_sp1_pop_file, line_in_the_dataset_sp1);
    // For each line:
    while (std::getline(network_contact_with_sp1_pop_file, line_in_the_dataset_sp1)){
        // Construction of a line
        std::stringstream ss(line_in_the_dataset_sp1);
        // split the elements of the line
        std::vector<std::string> splited_elements;
        std::string token;
        while(std::getline(ss, token, ',')) {
            splited_elements.emplace_back(token);
        }
        // Data extraction for the line
        std::string source = splited_elements[0];
        std::string target = splited_elements[1];

        auto search = network_contact_with_sp1_pop.find(source);
        if (search != network_contact_with_sp1_pop.end()) {
            network_contact_with_sp1_pop[source].insert(target);
        } else {
            network_contact_with_sp1_pop.insert(std::pair<std::string, std::set<std::string> > (source, {target}));
        }
    }
    chrono_load_data_network_contact_with_sp1_pop.stop();
    if (simulationParameters::verbose_mode) { std::cout << chrono_load_data_network_contact_with_sp1_pop.displayInSecond() << " seconds" << std::endl; }
//    // DEBUG: display mvts
//    std::cout << "network_contact_with_sp1_pop" << std::endl;
//    for (auto source : network_contact_with_sp1_pop) {
//        std::cout << source.first << ": ";
//        for (auto target : source.second) {
//            std::cout << target << " ";
//        }
//        std::cout << std::endl;
//    }
    // network_contact_with_sp2_pop - network_contact_with_sp2_pop.csv
    Chronometer chrono_load_data_network_contact_with_sp2_pop;
    chrono_load_data_network_contact_with_sp2_pop.start();
    if (simulationParameters::verbose_mode) { std::cout << "Construction of the contact (with WB) network data structure: "; }
    std::ifstream network_contact_with_sp2_pop_file(simulationParameters::data_dir + '/' + "network_contact_with_WB_pop.csv", std::fstream::in); // Open file in reading mode
    if(!network_contact_with_sp2_pop_file){std::cerr << "[ERROR] Unable to open the file 'network_contact_with_WB_pop.csv' !!" << std::endl;} // Check if the file is open!
    //
    std::string line_in_the_dataset_sp2;
    // Skip the first line, which corresponds to the file header
    std::getline(network_contact_with_sp2_pop_file, line_in_the_dataset_sp2);
    // For each line:
    while (std::getline(network_contact_with_sp2_pop_file, line_in_the_dataset_sp2)){
        // Construction of a line
        std::stringstream ss(line_in_the_dataset_sp2);
        // split the elements of the line
        std::vector<std::string> splited_elements;
        std::string token;
        while(std::getline(ss, token, ',')) {
            splited_elements.emplace_back(token);
        }
        // Data extraction for the line
        std::string source = splited_elements[0];
        std::string target = splited_elements[1];

        auto search = network_contact_with_sp2_pop.find(source);
        if (search != network_contact_with_sp2_pop.end()) {
            network_contact_with_sp2_pop[source].insert(target);
        } else {
            network_contact_with_sp2_pop.insert(std::pair<std::string, std::set<std::string> > (source, {target}));
        }
    }
    contact_network_data_loaded = true;
    chrono_load_data_network_contact_with_sp2_pop.stop();
    if (simulationParameters::verbose_mode) { std::cout << chrono_load_data_network_contact_with_sp2_pop.displayInSecond() << " seconds" << std::endl; }
//    // DEBUG: display mvts
//    std::cout << "network_contact_with_sp2_pop" << std::endl;
//    for (auto source : network_contact_with_sp2_pop) {
//        std::cout << source.first << ": ";
//        for (auto target : source.second) {
//            std::cout << target << " ";
//        }
//        std::cout << std::endl;
//    }
}

void simulationData::buildContactMatrix(){
    // build contact matrix for each species using the armadillo library

    mat_contact_sp1_pop = arma::zeros<arma::mat>(all_pop_id_list.size(), all_pop_id_list.size());
    mat_contact_sp2_pop = arma::zeros<arma::mat>(all_pop_id_list.size(), all_pop_id_list.size());

    for (const auto& pop_contact : network_contact_with_sp1_pop) {
        std::string the_pop_id = pop_contact.first;
        int the_pop_index = map_of_pop_index[the_pop_id];
        for (const auto& the_neighbour_id : pop_contact.second) {
            int the_neighbour_index = map_of_pop_index[the_neighbour_id];
            mat_contact_sp1_pop(the_pop_index, the_neighbour_index) = 1; // mat(i, j): access the element/object stored at the i-th row and j-th column
        }
    }

    for (const auto& pop_contact : network_contact_with_sp2_pop) {
        std::string the_pop_id = pop_contact.first;
        int the_pop_index = map_of_pop_index[the_pop_id];
        for (const auto& the_neighbour_id : pop_contact.second) {
            int the_neighbour_index = map_of_pop_index[the_neighbour_id];
            mat_contact_sp2_pop(the_pop_index, the_neighbour_index) = 1; // mat(i, j): access the element/object stored at the i-th row and j-th column
        }
    }

    contact_matrix_built = true;
}

void simulationData::rebuildContactMatrix(const bool& with_fenced_area){
    // rebuild contact matrix for each species due to fence building (using the armadillo library)

    mat_contact_sp1_pop = arma::zeros<arma::mat>(all_pop_id_list.size(), all_pop_id_list.size());
    mat_contact_sp2_pop = arma::zeros<arma::mat>(all_pop_id_list.size(), all_pop_id_list.size());

    for (const auto& pop_contact : network_contact_with_sp1_pop) {
        std::string the_pop_id = pop_contact.first;
        int the_pop_index = map_of_pop_index[the_pop_id];
        for (const auto& the_neighbour_id : pop_contact.second) {
            int the_neighbour_index = map_of_pop_index[the_neighbour_id];
            if (with_fenced_area) {
                if (pop_characteristics[the_pop_id].is_within_fenced_area == pop_characteristics[the_neighbour_id].is_within_fenced_area) {
                    mat_contact_sp1_pop(the_pop_index, the_neighbour_index) = 1; // mat(i, j): access the element/object stored at the i-th row and j-th column
                }
            } else {
                mat_contact_sp1_pop(the_pop_index, the_neighbour_index) = 1; // mat(i, j): access the element/object stored at the i-th row and j-th column
            }
        }
    }

    for (const auto& pop_contact : network_contact_with_sp2_pop) {
        std::string the_pop_id = pop_contact.first;
        int the_pop_index = map_of_pop_index[the_pop_id];
        for (const auto& the_neighbour_id : pop_contact.second) {
            int the_neighbour_index = map_of_pop_index[the_neighbour_id];
            if (with_fenced_area) {
                if (pop_characteristics[the_pop_id].is_within_fenced_area == pop_characteristics[the_neighbour_id].is_within_fenced_area) {
                    mat_contact_sp2_pop(the_pop_index, the_neighbour_index) = 1; // mat(i, j): access the element/object stored at the i-th row and j-th column
                }
            } else {
                mat_contact_sp2_pop(the_pop_index, the_neighbour_index) = 1; // mat(i, j): access the element/object stored at the i-th row and j-th column
            }
        }
    }
    //
    if (with_fenced_area) {
        if (simulationParameters::verbose_mode) { std::cout << "Contact matrixes were rebuilt to consider the fenced area."; }
    } else {
        if (simulationParameters::verbose_mode) { std::cout << "Contact matrixes were rebuilt to not consider the fenced area."; }
    }
}

void simulationData::loadDynamicData(){
    // dynamic_data_for_sp1_pop - dynamic_DP_pop.csv
    if (simulationParameters::sp1_dynamic_from_data) {
        Chronometer chrono_load_predefined_dynamic_sp1_pop;
        chrono_load_predefined_dynamic_sp1_pop.start();
        if (simulationParameters::verbose_mode) { std::cout << "Construction of the predefined dynamic for DP populations: "; }
        std::ifstream predefined_dynamic_sp1_pop_file(simulationParameters::data_dir + '/' + "predefined_dynamic_DP_pop_" + std::to_string(simulationParameters::first_run_index) + ".csv", std::fstream::in); // Open file in reading mode
        if(!predefined_dynamic_sp1_pop_file){std::cerr << "[ERROR] Unable to open the file 'predefined_dynamic_DP_pop_" + std::to_string(simulationParameters::first_run_index) + ".csv' !!" << std::endl;} // Check if the file is open!
        //
        std::string line_in_the_dataset_sp1;
        // Skip the first line, which corresponds to the file header
        std::getline(predefined_dynamic_sp1_pop_file, line_in_the_dataset_sp1);
        // For each line:
        while (std::getline(predefined_dynamic_sp1_pop_file, line_in_the_dataset_sp1)){
            // Construction of a line
            std::stringstream ss(line_in_the_dataset_sp1);
            // split the elements of the line
            std::vector<std::string> splited_elements;
            std::string token;
            while(std::getline(ss, token, ',')) {
                splited_elements.emplace_back(token);
            }
            // Data extraction for the line
            std::string pop_id = splited_elements[0];
            double date = std::stod(splited_elements[1]);
            std::string status = splited_elements[2];
            int size = 1; //std::stoi(splited_elements[3]);
            if (splited_elements.size() > 3) {
                size = std::stoi(splited_elements[3]);
            }

            if (sp1_predefined_dynamic.find(pop_id) == sp1_predefined_dynamic.end()) {
                sp1_predefined_dynamic.insert(std::pair< std::string, std::unordered_map<double, sirHeadcount>>(pop_id, std::unordered_map<double, sirHeadcount>()));
            }
            if (sp1_predefined_dynamic[pop_id].find(date) == sp1_predefined_dynamic[pop_id].end()) {
                sp1_predefined_dynamic[pop_id].insert(std::pair<double, sirHeadcount> (date, sirHeadcount()));
            }
            sp1_predefined_dynamic[pop_id][date].update(status, size);
        }
        chrono_load_predefined_dynamic_sp1_pop.stop();
        if (simulationParameters::verbose_mode) { std::cout << chrono_load_predefined_dynamic_sp1_pop.displayInSecond() << " seconds" << std::endl; }
    }
    // dynamic_data_for_sp2_pop - dynamic_DP_pop.csv
    if (simulationParameters::sp2_dynamic_from_data) {
        Chronometer chrono_load_predefined_dynamic_sp2_pop;
        chrono_load_predefined_dynamic_sp2_pop.start();
        if (simulationParameters::verbose_mode) { std::cout << "Construction of the predefined dynamic for WB populations: "; }
        std::ifstream predefined_dynamic_sp2_pop_file(simulationParameters::data_dir + '/' + "predefined_dynamic_WB_pop_" + std::to_string(simulationParameters::first_run_index) + ".csv", std::fstream::in); // Open file in reading mode
        if(!predefined_dynamic_sp2_pop_file){std::cerr << "[ERROR] Unable to open the file 'predefined_dynamic_WB_pop_" + std::to_string(simulationParameters::first_run_index) + ".csv' !!" << std::endl;} // Check if the file is open!
        //
        std::string line_in_the_dataset_sp2;
        // Skip the first line, which corresponds to the file header
        std::getline(predefined_dynamic_sp2_pop_file, line_in_the_dataset_sp2);
        // For each line:
        while (std::getline(predefined_dynamic_sp2_pop_file, line_in_the_dataset_sp2)){
            // Construction of a line
            std::stringstream ss(line_in_the_dataset_sp2);
            // split the elements of the line
            std::vector<std::string> splited_elements;
            std::string token;
            while(std::getline(ss, token, ',')) {
                splited_elements.emplace_back(token);
            }
            // Data extraction for the line
            std::string pop_id = splited_elements[0];
            double date = std::stod(splited_elements[1]);
            std::string status = splited_elements[2];
            int size = std::stoi(splited_elements[3]);

            if (sp2_predefined_dynamic.find(pop_id) == sp2_predefined_dynamic.end()) {
                sp2_predefined_dynamic.insert(std::pair< std::string, std::unordered_map<double, sirHeadcount>>(pop_id, std::unordered_map<double, sirHeadcount>()));
            }
            if (sp2_predefined_dynamic[pop_id].find(date) == sp2_predefined_dynamic[pop_id].end()) {
                sp2_predefined_dynamic[pop_id].insert(std::pair<double, sirHeadcount> (date, sirHeadcount()));
            }
            sp2_predefined_dynamic[pop_id][date].update(status, size);
        }
        chrono_load_predefined_dynamic_sp2_pop.stop();
        if (simulationParameters::verbose_mode) { std::cout << chrono_load_predefined_dynamic_sp2_pop.displayInSecond() << " seconds" << std::endl; }
    }
}

void simulationData::loadObsData(){
    Chronometer chrono_load_obs_data;
    chrono_load_obs_data.start();
    if (simulationParameters::verbose_mode) { std::cout << "Construction of the population observed data point structure: "; }
    std::ifstream observed_data_point_file(simulationParameters::data_dir + '/' + "obs_wild_boar_events_dynamic.csv", std::fstream::in); // Open file in reading mode
    if(!observed_data_point_file){std::cerr << "[ERROR] Unable to open the file 'obs_wild_boar_events_dynamic.csv' !!" << std::endl;} // Check if the file is open!
    //
    std::string line_in_the_dataset;
    // Skip the first line, which corresponds to the file header
    std::getline(observed_data_point_file, line_in_the_dataset);
    // For each line:
    while (std::getline(observed_data_point_file, line_in_the_dataset)){
        // Construction of a line
        std::stringstream ss(line_in_the_dataset);
        // split the elements of the line
        std::vector<std::string> splited_elements;
        std::string token;
        while(std::getline(ss, token, ',')) {
            splited_elements.emplace_back(token);
        }
        // Data extraction for the line
        std::string pop_id = splited_elements[0];
        double date = std::stoi(splited_elements[1]);
        double carcass_discovery_by_passive_surveillance = std::stoi(splited_elements[2]);
        double carcass_discovery_by_active_search = std::stof(splited_elements[3]);
        double test_on_hunted_ind = std::stof(splited_elements[4]);
        double positive_test_on_hunted_ind = std::stof(splited_elements[5]);

        // build population characteristics unit
        observedDataPoint obs_data_point = observedDataPoint(carcass_discovery_by_passive_surveillance, carcass_discovery_by_active_search, test_on_hunted_ind, positive_test_on_hunted_ind);
        // add the unit in the pop_characteristics structure

        if (sp2_obs_data.find(pop_id) == sp2_obs_data.end()) {
            sp2_obs_data.insert(std::pair< std::string, std::unordered_map<double, observedDataPoint>>(pop_id, std::unordered_map<double, observedDataPoint>()));
        }
        sp2_obs_data[pop_id].insert(std::pair<double, observedDataPoint> (date, obs_data_point));

    }
//    observed_data_point_loaded = true;
    chrono_load_obs_data.stop();
    if (simulationParameters::verbose_mode) { std::cout << chrono_load_obs_data.displayInSecond() << " seconds" << std::endl; }
}
