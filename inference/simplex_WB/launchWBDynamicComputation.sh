#!/bin/bash
#$ -S /bin/bash
#$ -N smplxWB
#$ -m a
#$ -cwd
# #$ -wd

BETA_SP2=$1
BETA_SP2D=$2
BETA_SP1_SP2=$3
BETA_SP2_SP2=$4
BETA_SP2D_SP2=$5
# RHO=$6
# RHO_ACTIVE_SEARCH=$7
# DATA_DIR=$8
# RES_DIR=$9
DATA_DIR=$6
RES_DIR=$7
TILESIZE=$8
SIMDURATION=$9
TFENCE=${10}
TSTOPPLUG=${11}

# ../compilationOnCluster/sir_model_metapop_two_species \
# 	-d ${DATA_DIR} \
# 	-r ${RES_DIR} \
# 	--number_of_runs 1 --first_run_index ${SGE_TASK_ID} \
# 	--beta_sp2 ${BETA_SP2} \
# 	--beta_sp2D ${BETA_SP2D} \
# 	--beta_sp1_sp2 ${BETA_SP1_SP2} \
# 	--beta_sp2_sp2 ${BETA_SP2_SP2} \
# 	--beta_sp2D_sp2 ${BETA_SP2D_SP2} \
# 	--rho ${RHO} \
# 	--rho_active_search ${RHO_ACTIVE_SEARCH} \
# 	--is_get_obs_from_data_for_inference \
# 	--tile_size $TILESIZE \
# 	--sim_duration $SIMDURATION \
# 	--date_on_which_the_fence_is_built $TFENCE \
# 	--date_from_which_to_stop_using_plugged_data $TSTOPPLUG

../compilationOnCluster/sir_model_metapop_two_species \
	-d ${DATA_DIR} \
	-r ${RES_DIR} \
	--number_of_runs 1 --first_run_index ${SGE_TASK_ID} \
	--beta_sp2 ${BETA_SP2} \
	--beta_sp2D ${BETA_SP2D} \
	--beta_sp1_sp2 ${BETA_SP1_SP2} \
	--beta_sp2_sp2 ${BETA_SP2_SP2} \
	--beta_sp2D_sp2 ${BETA_SP2D_SP2} \
	--is_get_obs_from_data_for_inference \
	--tile_size $TILESIZE \
	--sim_duration $SIMDURATION \
	--date_on_which_the_fence_is_built $TFENCE \
	--date_from_which_to_stop_using_plugged_data $TSTOPPLUG

