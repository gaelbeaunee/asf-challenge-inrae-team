//
//  sirHeadcount.cpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#include "sirHeadcount.hpp"

sirHeadcount::sirHeadcount(){
}

sirHeadcount::sirHeadcount(const indivCountType& _count_S, const indivCountType& _count_E, const indivCountType& _count_Is, const indivCountType& _count_Ic, const indivCountType& _count_D, const indivCountType& _count_R){
    S = _count_S;
    E = _count_E;
    Is = _count_Is;
    Ic = _count_Ic;
    D = _count_D;
    R = _count_R;
}

// Member functions
int sirHeadcount::get(const std::string& _states){
    if (_states == "S") {
        return S;
    } else if (_states == "E") {
        return E;
    } else if (_states == "Is") {
        return Is;
    } else if (_states == "Ic") {
        return Ic;
    } else if (_states == "D") {
        return D;
    } else if (_states == "R") {
        return R;
    } else {
        std::cout << "ERROR: wrong states (get): " << _states << std::endl;
        return NAN;
    }
}

void sirHeadcount::update(const std::string& _states, const indivCountType& _count){
    if (_states == "S") {
        S += _count;
    } else if (_states == "E") {
        E += _count;
    } else if (_states == "Is") {
        Is += _count;
    } else if (_states == "Ic") {
        Ic += _count;
    } else if (_states == "D") {
        D += _count;
    } else if (_states == "R") {
        R += _count;
    } else {
        std::cout << "ERROR: wrong states (update): " << _states << std::endl;
    }
}

void sirHeadcount::update(const indivCountType& _count_S, const indivCountType& _count_E, const indivCountType& _count_Is, const indivCountType& _count_Ic, const indivCountType& _count_D, const indivCountType& _count_R){
    S += _count_S;
    E += _count_E;
    Is += _count_Is;
    Ic += _count_Ic;
    D += _count_D;
    R += _count_R;
}

void sirHeadcount::update(const std::unordered_map<std::string, indivCountType>& _sir_map_for_updating){
    // could not be very efficient as it is needed to check if key exist before updating
    if (_sir_map_for_updating.find("S") != _sir_map_for_updating.end()) {
        S += _sir_map_for_updating.at("S");
    }
    if (_sir_map_for_updating.find("E") != _sir_map_for_updating.end()) {
        E += _sir_map_for_updating.at("E");
    }
    if (_sir_map_for_updating.find("Is") != _sir_map_for_updating.end()) {
        Is += _sir_map_for_updating.at("Is");
    }
    if (_sir_map_for_updating.find("Ic") != _sir_map_for_updating.end()) {
        Ic += _sir_map_for_updating.at("Ic");
    }
    if (_sir_map_for_updating.find("D") != _sir_map_for_updating.end()) {
        D += _sir_map_for_updating.at("D");
    }
    if (_sir_map_for_updating.find("R") != _sir_map_for_updating.end()) {
        R += _sir_map_for_updating.at("R");
    }
}

void sirHeadcount::update(const sirHeadcount& _sirHeadcount_for_updating){
    S += _sirHeadcount_for_updating.S;
    E += _sirHeadcount_for_updating.E;
    Is += _sirHeadcount_for_updating.Is;
    Ic += _sirHeadcount_for_updating.Ic;
    D += _sirHeadcount_for_updating.D;
    R += _sirHeadcount_for_updating.R;
}

void sirHeadcount::replace(const sirHeadcount& _sirHeadcount_for_updating){
    S = _sirHeadcount_for_updating.S;
    E = _sirHeadcount_for_updating.E;
    Is = _sirHeadcount_for_updating.Is;
    Ic = _sirHeadcount_for_updating.Ic;
    D = _sirHeadcount_for_updating.D;
    R = _sirHeadcount_for_updating.R;
}

int sirHeadcount::sum(){
    int total = 0;
    total = S + E + Is + Ic + D; // Warning: R is for 'Removed'
    return total;
}

void sirHeadcount::inverse(){
    S *= -1;
    E *= -1;
    Is *= -1;
    Ic *= -1;
    D *= -1;
    R *= -1;
}

std::vector<std::string> sirHeadcount::getDisaggregatedListOfHealthStates(){
    std::vector<std::string> disaggregated_list_of_health_states;
    for (int x = 0; x < S; x++) {
        disaggregated_list_of_health_states.emplace_back("S");
    }
    for (int x = 0; x < E; x++) {
        disaggregated_list_of_health_states.emplace_back("E");
    }
    for (int x = 0; x < Is; x++) {
        disaggregated_list_of_health_states.emplace_back("Is");
    }
    for (int x = 0; x < Ic; x++) {
        disaggregated_list_of_health_states.emplace_back("Ic");
    }
    for (int x = 0; x < D; x++) {
        disaggregated_list_of_health_states.emplace_back("D");
    }
    for (int x = 0; x < R; x++) {
        disaggregated_list_of_health_states.emplace_back("R");
    }
    return disaggregated_list_of_health_states;
}

std::vector<std::string> sirHeadcount::getDisaggregatedListOfHealthStates_SEIsIc(){
    std::vector<std::string> disaggregated_list_of_health_states;
    for (int x = 0; x < S; x++) {
        disaggregated_list_of_health_states.emplace_back("S");
    }
    for (int x = 0; x < E; x++) {
        disaggregated_list_of_health_states.emplace_back("E");
    }
    for (int x = 0; x < Is; x++) {
        disaggregated_list_of_health_states.emplace_back("Is");
    }
    for (int x = 0; x < Ic; x++) {
        disaggregated_list_of_health_states.emplace_back("Ic");
    }
    return disaggregated_list_of_health_states;
}

sirHeadcount sirHeadcount::selectXHealthStatesRandomly(const indivCountType& _count){
    sirHeadcount selected_health_states;
    std::vector<std::string> disaggregated_list_of_health_states = getDisaggregatedListOfHealthStates();
    std::vector<std::string> vector_of_selected_health_states = auxFunctions::randomSamplingWithoutReplacement(disaggregated_list_of_health_states, _count, simulationParameters::random_engine);
    for (const auto& item : vector_of_selected_health_states) {
        selected_health_states.update(item, 1);
    }
    return selected_health_states;
}

std::vector<std::string> sirHeadcount::getNegativeStates(){
    std::vector<std::string> v_negative_states;
    if (S < 0) {
        v_negative_states.emplace_back("S");
    }
    if (E < 0) {
        v_negative_states.emplace_back("E");
    }
    if (Is < 0) {
        v_negative_states.emplace_back("Is");
    }
    if (Ic < 0) {
        v_negative_states.emplace_back("Ic");
    }
    if (D < 0) {
        v_negative_states.emplace_back("D");
    }
    if (R < 0) {
        v_negative_states.emplace_back("R");
    }
    return v_negative_states;
}


// sirHeadcount implementation with a map /////////////////////////////////////


sirHeadcount_map::sirHeadcount_map(){
}

sirHeadcount_map::sirHeadcount_map(const indivCountType& _count_S, const indivCountType& _count_I, const indivCountType& _count_R){
    headcount["S"] = _count_S;
    headcount["Ic"] = _count_I;
    headcount["R"] = _count_R;
//    headcount.insert(std::pair< std::string,indivCountType >("S",_count_S));
//    headcount.insert(std::pair< std::string,indivCountType >("Ic",_count_I));
//    headcount.insert(std::pair< std::string,indivCountType >("R",_count_R));
}

// Member functions
void sirHeadcount_map::update(const std::string& _states, const indivCountType& _count){
    headcount[_states] += _count;
}

void sirHeadcount_map::update(const indivCountType& _count_S, const indivCountType& _count_I, const indivCountType& _count_R){
    headcount["S"] += _count_S;
    headcount["Ic"] += _count_I;
    headcount["R"] += _count_R;
}

void sirHeadcount_map::update(const std::unordered_map<std::string, indivCountType>& _sir_map_for_updating){
    for(auto it = _sir_map_for_updating.begin(); it != _sir_map_for_updating.end(); ++it){
        headcount[it->first] += it->second;
    }
}

void sirHeadcount_map::update(const sirHeadcount_map& _sirHeadcount_for_updating){
    update(_sirHeadcount_for_updating.headcount);
}

int sirHeadcount_map::sum(){
    indivCountType total = 0;
    total = headcount["S"] + headcount["Ic"] + headcount["R"];
    return total;
}

void sirHeadcount_map::inverse(){
    headcount["S"] *= -1;
    headcount["Ic"] *= -1;
    headcount["R"] *= -1;
}

std::vector<std::string> sirHeadcount_map::getDisaggregatedListOfHealthStates(){
    std::vector<std::string> disaggregated_list_of_health_states;
    for (const auto& item : headcount) {
        for (int x = 0; x < item.second; x++) {
            disaggregated_list_of_health_states.emplace_back(item.first);
        }
    }
    return disaggregated_list_of_health_states;
}

sirHeadcount_map sirHeadcount_map::selectXHealthStatesRandomly(const indivCountType& _count){
    sirHeadcount_map selected_health_states;
    std::vector<std::string> disaggregated_list_of_health_states = getDisaggregatedListOfHealthStates();
    std::vector<std::string> vector_of_selected_health_states = auxFunctions::randomSamplingWithoutReplacement(disaggregated_list_of_health_states, _count, simulationParameters::random_engine);
    for (const auto& item : vector_of_selected_health_states) {
        selected_health_states.update(item, 1);
    }
    return selected_health_states;
}

std::vector<std::string> sirHeadcount_map::getNegativeStates(){
    std::vector<std::string> v_negative_states;
    for (const auto& item : headcount) {
        if (item.second < 0) {
            v_negative_states.emplace_back(item.first);
        }
    }
    return v_negative_states;
}
