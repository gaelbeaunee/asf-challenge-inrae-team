#include <iostream>
#include <ctime>
#include <unordered_map>
#include <iomanip>
#include <fstream>
#include <vector>
#include <boost/tokenizer.hpp>

#include "simulatorpg.hpp"

unordered_map<string, double> readObs(string file){
	using namespace boost;
	unordered_map<string, double> tsusp;
	typedef boost::tokenizer<boost::char_separator<char>> Tokenizer;
	boost::char_separator<char> sep{","};
	string line;
	vector<string> vec;
	
	ifstream in(file.c_str());
	if (!in.is_open()){
		return tsusp;
	}

	// header
	if(getline(in,line)){
		Tokenizer tok(line, sep);
		vec.assign(tok.begin(),tok.end());
	}

	while(getline(in,line)){
		Tokenizer tok(line, sep);
		vec.assign(tok.begin(),tok.end());
		tsusp[vec[0]]=stod(vec[1]);
	}
	in.close();

	return tsusp;
}

int main(int argc, char* argv[]){
	SimulatorPG simpg;

	// Parameters from command line
	double alpha, beta, gamma;
	double t0, tf, tmin;
	unsigned int seed;

	tmin=stod(argv[1]);
	t0=stod(argv[2]);
	tf=stod(argv[3]);
	seed=stod(argv[4]);
	alpha=stod(argv[5]);
	beta=stod(argv[6]);
	gamma=stod(argv[7]);

	// Parameters from stdin
	string sfile, efile, isfile, icfile, rfile;
	string popdesc, mvtfile, contactfile, localcontactfile;
	string obsfile, pcullfile;

	cin >> sfile;
	cin >> efile;
	cin >> isfile;
	cin >> icfile;
	cin >> rfile;
	cin >> popdesc;
	cin >> mvtfile;
	cin >> contactfile;
	cin >> localcontactfile;
	cin >> obsfile;
	cin >> pcullfile;

	// Initialise Simulator with parameters
	simpg.setTimes(tmin, t0, tf, PAR_DT);
	simpg.setSeed(seed);
	simpg.setParameters(alpha, beta, gamma, PAR_TIMEE, PAR_TIMEIS, PAR_TIMEIC, PAR_TIMER, 1);
	simpg.setControlMeasures(CM_DURPZ, CM_RADPZ, CM_DURSZ, CM_RADSZ,CM_BTDUR, CM_BTPAST);

	// Initialise WB populations
	simpg.wb.readPopFromFile(sfile, S);
	simpg.wb.readPopFromFile(efile, E);
	simpg.wb.readPopFromFile(isfile, Is);
	simpg.wb.readPopFromFile(icfile, Ic);
	simpg.wb.readPopFromFile(rfile, R);

	// Initialise PG populations
	simpg.pg.readPopDescFromFile2(popdesc);
	simpg.pg.readExchangesFromFile(mvtfile);
	simpg.pg.readContactsFromFile(contactfile);
	//simpg.pg.addLocalContacts(LOCAL_CONT_DIST);
    simpg.pg.readLocalContactsFromFile(localcontactfile);

	// Initialise observations
	//unordered_map<string, double> tsusp=readObs(obsfile);
	//unordered_map<string, double> pcull=readObs(pcullfile);
    vector<obs> tsusp=simpg.readObsVec(obsfile);
	vector<obs> pcull=simpg.readObsVec(pcullfile);
	simpg.fillWithObs(tsusp, pcull);


	// Compute likelihood
	double ll=simpg.likelihood(tsusp, true);
	cout << ll << endl;
}
