#! /bin/bash
# ARGS : <dir_for_results (optionnal)>

unset PYTHONPATH


if [ ! -z $1 ]
then
	RESDIR=$1
else
	RESDIR="../results"
fi


###
### COUPLING ARGS
###

NITER=1
NREP=10

###
### PARAMETERS PG
###

TMIN=-60
T0=1
TF=110
TFOBSEST=30
SEED=1
ALPHA=1e-6
BETA=1e-6
GAMMA=1e-6
PGAVGWB="./avgwb.R"
PGLIK="./ls_pg"
PGINF="./inference_ls.R"
PGSIM="./simulate_pg"
POPDESC="../data/day_110/17321om/pop_characteristics.csv"
MVTFILE="../data/day_110/17321om/mvts_network.csv"
CONTFILE="../data/day_110/17321om/network_contact_DP_with_WB_pop.csv"
LOCALCONTFILE="../data/day_110/17321om/network_local_contact_DP_DP.csv"
OBSFILE="../data/day_110/17321om/susp_time_DP.csv"
PCULLFILE="../data/day_110/17321om/preventive_cull_time_DP.csv"

###
### PARAMETERS WB
###

SIMPLX="./simplex_WB/inference_WB_main.py"
OUTPUT_SIMPLX=""
PROGWB="../compilationOnCluster/sir_model_metapop_two_species"
DATAWB="../data/day_110/17321om"
WBOBSDATA="../data/day_110/17321om"
RESWB=""
ESTWB=""
IVAL="100"
FVAL="100"
# PARAMWBNAMES="beta_sp2 beta_sp2D beta_sp1_sp2 beta_sp2_sp2 beta_sp2D_sp2 rho rho_active_search"
# PARAMWBVAL="0.05 0.01 0.01 0.01 0.01 0.001 0.01"
# PARAMWBMIN="0.000 0.000 0.000 0.000 0.000 0.000 0.000"
# PARAMWBMAX="3.0 3.0 3.0 3.0 3.0 0.1 0.5"
PARAMWBNAMES="beta_sp2 beta_sp2D beta_sp1_sp2 beta_sp2_sp2 beta_sp2D_sp2"
PARAMWBVAL="0.05 0.01 0.01 0.01 0.01"
PARAMWBMIN="0.000 0.000 0.000 0.000 0.000"
PARAMWBMAX="3.0 3.0 3.0 3.0 3.0"
RELAUNCHTHRES="50"
RELAUNCHREP="4"
INITIALSTATUS="predefined_dynamic_DP_pop.csv"
TILESIZE=17321
SIMDUR=110
DATESTOPPLUGDATA=111 # one more timestep than SIMDUR

###
### CONTROL
###
TFENCE=60


###
### ALTERNATE ESTIMATION
###

for i in `seq 1 $NITER`
do
	### Preparation for this iteration
	mkdir -p ${RESDIR}/coupling_iter_${i}
	mkdir -p ${RESDIR}/coupling_iter_${i}/wb
	mkdir -p ${RESDIR}/coupling_iter_${i}/pg

	###
	### WB model
	###
	# Status files for each rep are in the status STATUSDATA array
	# parameter output is expected in file  : wb/parameters_wb_optim.txt
	# Simulations are expected in : wb/simulation_<rep>.csv

	echo "### WB ITER $i"

	# Set the status files
	if [ "$i" == "1" ]
	then
		for j in `seq 1 $NREP`
		do
			STATUSDATA[$j]="${DATAWB}/predefined_dynamic_DP_pop_${j}.csv"
			[ -h "${STATUSDATA[$j]}" ] && rm -f ${STATUSDATA[$j]}
			ln -s $INITIALSTATUS ${STATUSDATA[$j]}
		done
	else
		for j in `seq 1 $NREP`
		do
			STATUSDATA[j]="${DATAWB}/predefined_dynamic_DP_pop_${j}.csv"
			TRGT="../${RESDIR}/coupling_iter_$(( $i - 1 ))/pg/status_rep_${j}.csv"
			[ -h "${STATUSDATA[$j]}" ] && rm -f ${STATUSDATA[$j]}
			ln -s $TRGT ${STATUSDATA[j]}
		done
	fi

	RESWB="${RESDIR}/coupling_iter_${i}/wb"
	OUTPUT_SMPLX="simplex.res"

	python3 $SIMPLX \
		-d $DATAWB \
		-r $RESWB \
		-n $OUTPUT_SMPLX \
		-e $WBOBSDATA \
		-I $IVAL \
		-F $FVAL \
		-i $NREP \
		--paramName $PARAMWBNAMES \
		--paramVal $PARAMWBVAL \
		--paramMin $PARAMWBMIN \
		--paramMax $PARAMWBMAX \
		--relaunchThreshold $RELAUNCHTHRES \
		--relaunchNbRepeat $RELAUNCHREP \
        --tile_size $TILESIZE \
        --sim_duration $SIMDUR \
        --date_on_which_the_fence_is_built $TFENCE \
        --date_from_which_to_stop_using_plugged_data $DATESTOPPLUGDATA

	# Simulations
	j=1
	for EXP in ${RESDIR}/coupling_iter_${i}/wb/exp*
	do
		SFILESSIM[$j]=`ls $EXP/headcount_*S*.csv`
		EFILESSIM[$j]=`ls $EXP/headcount_*E*.csv`
		ISFILESSIM[$j]=`ls $EXP/headcount_*Is*.csv`
		ICFILESSIM[$j]=`ls $EXP/headcount_*Ic*.csv`
		RFILESSIM[$j]=`ls $EXP/headcount_*R*.csv`
		j=$(( $j + 1 ))
	done


	###
	### DP model
	###
	# parameter output is expected in file  : pg/parameters_pg_optim.txt
	# Simulations are expected in : pg/status_rep_<rep>.csv

	echo "### PG ITER $i"
	cd "../pg_model"

	# Average WB simulations
	SFILE="${RESDIR}/coupling_iter_${i}/pg/headcount_S.csv"
	EFILE="${RESDIR}/coupling_iter_${i}/pg/headcount_E.csv"
	ISFILE="${RESDIR}/coupling_iter_${i}/pg/headcount_I_s.csv"
	ICFILE="${RESDIR}/coupling_iter_${i}/pg/headcount_I_c.csv"
	RFILE="${RESDIR}/coupling_iter_${i}/pg/headcount_R.csv"

	Rscript $PGAVGWB ${SFILESSIM[*]} $SFILE
	Rscript $PGAVGWB ${EFILESSIM[*]} $EFILE
	Rscript $PGAVGWB ${ISFILESSIM[*]} $ISFILE
	Rscript $PGAVGWB ${ICFILESSIM[*]} $ICFILE
	Rscript $PGAVGWB ${RFILESSIM[*]} $RFILE

	# Inference
	PARAMFILE="${RESDIR}/coupling_iter_${i}/pg/parameters_pg_optim.csv"
	PGINFILES="${RESDIR}/coupling_iter_${i}/pg/inputfiles.txt"
	cat <<-EOF > $PGINFILES
		$SFILE
		$EFILE
		$ISFILE
		$ICFILE
		$RFILE
		$POPDESC
		$MVTFILE
		$CONTFILE
        $LOCALCONTFILE
		$OBSFILE
		$PCULLFILE
	EOF

	#Rscript $PGINF $TMIN $T0 $TF $SEED $ALPHA $BETA $PARAMFILE $PGLIK $PGINFILES
	#BETA=`tail -n1 $PARAMFILE|cut -d"," -f2`
	Rscript $PGINF $TMIN $T0 $TF $TFOBSEST $SEED $ALPHA $BETA $GAMMA $PARAMFILE $PGLIK $PGINFILES
	ALPHA=`tail -n1 $PARAMFILE|cut -d"," -f1`
	BETA=`tail -n1 $PARAMFILE|cut -d"," -f2`
	GAMMA=`tail -n1 $PARAMFILE|cut -d"," -f3`

	# Simulation
	for j in `seq 1 $NREP`
	do
		$PGSIM $TMIN $T0 $TF $TF $j $ALPHA $BETA $GAMMA < $PGINFILES > ${RESDIR}/coupling_iter_${i}/pg/status_rep_${j}.csv &
	done
	wait
	cd "../inference"

done

exit 0
