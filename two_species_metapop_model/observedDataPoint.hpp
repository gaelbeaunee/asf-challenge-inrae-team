//
//  observedDataPoint.hpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/11/2020.
//

#ifndef observedDataPoint_hpp
#define observedDataPoint_hpp

#include <stdio.h>
#include <string>

struct observedDataPoint {
    // Constructor
    observedDataPoint();
    observedDataPoint(const double& _carcass_discovery_by_passive_surveillance, const double& _carcass_discovery_by_active_search, const double& _test_on_hunted_ind, const double& _positive_test_on_hunted_ind);
//    pop_id
//    date
//    carcass_discovery_by_passive_surveillance
//    carcass_discovery_by_active_search
//    test_on_hunted_ind
//    positive_test_on_hunted_ind

    // Variables
//    std::string pop_id;
//    double date;
    double carcass_discovery_by_passive_surveillance;
    double carcass_discovery_by_active_search;
    double test_on_hunted_ind;
    double positive_test_on_hunted_ind;
};

#endif /* observedDataPoint_hpp */
