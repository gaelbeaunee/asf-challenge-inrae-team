#ifndef SIMULATOR_H
#define SIMULATOR_H

#define LIKELIHOOD_REP (20)

#define PAR_DT (1)
#define PAR_TIMEE (5)
#define PAR_TIMEIS (2)
#define PAR_TIMEIC (4)
#define PAR_TIMER (50)
#define LOCAL_CONT_DIST (4e3)
#define PREVCULL_DIST (3000)

#define CM_DURPZ (40)
#define CM_RADPZ (3000)
#define CM_DURSZ (30)
#define CM_RADSZ (10000)
#define CM_BTDUR (40)
#define CM_BTPAST (21)
#define CM_PREVENTIVE_CULLING_START (90)

#define CMP2_START (80)
//#define CMP2_CULL_PZ
//#define CMP2_SZ
#define CMP2_RADSZ (15000)
//#define CMP2_CULL_BT
#define CMP2_CULL_BT_DUR (21)

/*
#define SUSPFILE ("../data/day_80/17321om/susp_time_DP.csv")
#define CONTACTFENCE ("../data/day_80/17321om/network_contact_with_WB_pop_withFence.csv")
#define DPCONTACTS ("../data/day_80/17321om/network_contact_with_WB_pop_withFence.csv")
#define FENCETIME (60)
*/

#include "popwb.hpp"
#include "poppg.hpp"
#include <set>

struct obs {
    string id;
    double t;
};

class SimulatorPG{
private:
	double timestep;
	double tmin, t0, tf, tfobs;
	double alpha, beta, gamma;
	unsigned int seed;
	unsigned int timeE, timeIs, timeIc, timeR;
	double q;
	double durpz, dursz, radpz, radsz;
	double durbacktrace, pastbacktrace;

public:
	PopPG pg;
	PopWB wb;
	SimulatorPG();
	SimulatorPG(double t0, double tf, double timestep, PopPG pg, PopWB wb);
	void setTimes(double t0, double tf, double dt);
	void setTimes(double tmin, double t0, double tf, double dt);
	void setTimes(double tmin, double t0, double tf, double tfobs, double dt);
	void setSeed(unsigned int seed);
	void setParameters(double alpha, double beta, double gamma, unsigned int tE, unsigned int tIs, unsigned int tIc, double q);
	void setParameters(double alpha, double beta, double gamma, unsigned int tE, unsigned int tIs, unsigned int tIc, unsigned int tR, double q);
	void setControlMeasures(double durpz, double radpz, double dursz, double radsz, double durbacktrace, double pastbacktrace);
	void forward(double t1, double t2);
	double infectionProbability(double t, string id);
	double likelihood(unordered_map<string, double> tobs, bool islog);
	double likelihood(vector<obs> tobs, bool islog);

	double lsCriterion(unordered_map<string, double> tobs);
	double lsCriterion(vector<obs> tobs);
	int getInfections(double t);

	void fillWithObs(unordered_map<string, double> tobs);
	void fillWithObs(vector<obs> tobs);
	void fillWithObs(unordered_map<string, double> tsusp, unordered_map<string, double> tpreventive);
	void fillWithObs(vector<obs> tsusp, vector<obs> tpreventive);
	void fillWithInit();
	unordered_map<string, string> getStatusPG(double t);
	void addControlMeasures(string id, double t);
	int preventiveCulling(std::unordered_map<double, std::unordered_map<std::string, std::vector<std::unordered_map<std::string, double>>>> carasses_location, double t);

	unordered_map<string, double> readSuspTimes(string file);

    static vector<obs> readObsVec(string file);
};

#endif
