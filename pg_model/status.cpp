#include "status.hpp"

std::string Status::statusToString(status s){
	switch(s){
		case S:
			return "S";
			break;
		case E:
			return "E";
			break;
		case Is:
			return "Is";
			break;
		case Ic:
			return "Ic";
			break;
		case R:
			return "R";
			break;
		default:
			return "U";
	}
}
