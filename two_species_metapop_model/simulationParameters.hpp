//
//  simulationParameters.hpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#ifndef simulationParameters_hpp
#define simulationParameters_hpp

#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <random>
//#include <chrono>
#include <tclap/CmdLine.h>

#include "Chronometer.hpp"

class simulationParameters {
private:
    static simulationParameters *instance;

    simulationParameters(int argc, const char * argv[]);

    // Member functions
    static void seedTheRandomEngine(unsigned int custom_seed);
    static void printParameterValues();

public:
    // Constructor
    static simulationParameters& get_instance(int argc, const char * argv[]);

    static void kill();

    // Variables
    // random engine
    static std::mt19937 random_engine;
    static unsigned int custom_random_engine_seed;
    static unsigned int random_engine_seed;
    // data and results directory
    static std::string data_dir;
    static std::string results_dir;
    static std::string experiment_dir;
    // repetitions
    static int number_of_runs;
    static int first_run_index;
    // id length
//    static int pop_id_length;
    // species name
    static std::string sp1_name; // = 'cattle'
    static std::string sp1_prefix; // = 'C'
    static std::string sp2_name; // = 'badger'
    static std::string sp2_prefix; // = 'B'
    // time
    static double sub_period_length; // = 365
    static double observation_period; // = 365
    static double number_of_sub_period; // = 3
    static double maximum_length_of_the_experiment; // = number_of_sub_period * sub_period_length
    static double tau; // = 1.0 // should not be greater than 1.0 for the moment, otherwise the movement of individuals will not be done properly (TODO: take into account period [t-tau,t] when performing the movements in order to consider all movements for the given timestep) -> DONE, but be careful and check that everything goes well
    static std::vector<double> T; //= np.arange(0.0, maximum_length_of_the_experiment + tau, tau)
    static bool mvt_date_according_to_timestep_tau; // if true, it should lead to better computing performance.
    // initial states
    static int number_of_infected_sp1_pop; // = 0
    static int number_of_infected_sp2_pop; // = 1
    static int nb_of_inf_indiv_initially_introduced;
    // epidemic dynamic
    static bool density_dependent_tranmission_withinpop_I; // if false, a frequency dependent transmission is considered
    static bool density_dependent_tranmission_betweenpop_I; // if false, a frequency dependent transmission is considered
    static bool density_dependent_tranmission_withinpop_D; // if false, a frequency dependent transmission is considered
    static bool density_dependent_tranmission_betweenpop_D; // if false, a frequency dependent transmission is considered
    static double beta_sp1;      // within pop. sp1 Is + Ic
    static double beta_sp1Is;      // within pop. sp1 Is
    static double beta_sp1Ic;      // within pop. sp1 Ic
    static double beta_sp1D;      // within pop. sp1 D
    static double beta_sp2;      // within pop. sp2 Is + Ic
    static double beta_sp2Is;      // within pop. sp2 Is
    static double beta_sp2Ic;      // within pop. sp2 Ic
    static double beta_sp2D;      // within pop. sp2 D
    static double beta_sp1_sp1_ex;  // conctact sp1-sp1
    static double beta_sp1_sp1;  // conctact sp1-sp1
    static double beta_sp1_sp2;  // conctact sp1-sp2
    static double beta_sp2_sp1;  // conctact sp2-sp1
    static double beta_sp2_sp2;  // conctact sp2-sp2
    static double beta_sp2D_sp1; // conctact sp2-sp1
    static double beta_sp2D_sp2; // conctact sp2-sp2
    static double alpha;
    static double delta;  // 1/delta = duration in E
    static double sigma;  // 1/sigma = duration in Is
    static double mu_sp1; // mortality rate  for sp1
    static double mu_sp2; // mortality rate for sp2
    static double gamma;  // 1/gamma = duration in D
    static double rho;    // rate at which ind. D are discovered
    static double eta;    // hunting  rate
    // control measures
    static double time_to_test_results; // Time to wait for test results
    static double test_sensitivity; // not use for the moment because test is considered perfect
    static int start_date_test_on_hunted_wildboar;
    static double proportion_of_hunted_wb_tested;
    static double date_on_which_the_fence_is_built;
    static double increase_factor_of_eta_within_a_fenced_area;
    static double increase_factor_of_rho_after_carcass_discovery;
    static double rho_active_search;
    static double time_to_plan_an_active_search;
    static double active_search_duration;
    // inference
    static bool sp1_dynamic_from_data;
    static bool sp2_dynamic_from_data;
    static bool model_coupling_for_prediction;
    static bool get_obs_from_data_for_inference;
    // save
    static bool save_sp1_dynamic;
    static bool save_sp2_dynamic;

    //new param for period 2
    static double tile_size;
    static double active_search_radius;
    static double rho_passive_search_inside_fenced_buffer_area;
    static double proportion_of_hunted_wb_tested_inside_fenced_buffer_area;
    static double eta_inside_fenced_buffer_area;
    static double date_on_which_p2measures_are_implemented;
    static double active_search_radius_outside_fenced_buffer_area_p2measure;

    static double date_from_which_to_stop_using_plugged_data;

    static double date_until_which_hunting_pressure_is_increased;
    static double last_date_of_hunting_period;


    // for debug
    static bool verbose_mode;

    // Member functions
    static void saveCurrentRandomEngineSeed();

};

#endif /* simulationParameters_hpp */
