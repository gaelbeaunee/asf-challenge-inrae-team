#ifndef POPWB_H
#define POPWB_H

#include <vector>
#include <unordered_map>

#include "status.hpp"
#include "snapshotwb.hpp"

class PopWB {
private:
	std::unordered_map<double, snapshotwb*> snapshot;	
	std::unordered_map<string, vector<string>> contactsDP;

public:
	void addSnapshot(double t);
	void addSnapshotPop(double t, string id, unsigned int S, unsigned int E, unsigned int Is, unsigned int Ic, unsigned int R);
	void addSnapshotPop(double t, string id, status s, unsigned int count);

	unsigned int getCount(double t, string id, status stat);
	void readPopFromFile(string file);
	void readPopFromFile(string file, status stat);
	
	vector<string> getContactsDP(string id);
	void addContactDP(string wb, string dp);

};

#endif
