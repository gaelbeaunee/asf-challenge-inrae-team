#include <cmath>

#include "zone.hpp"

Zone::Zone(double posx, double posy, double radius){
	x=posx;
	y=posy;
	r=radius;
}

bool Zone::isIn(double posx, double posy){
	double d=sqrt((posx-x)*(posx-x)+(posy-y)*(posy-y));
	return d<r;
}


double Zone::getX(){
	return x;
}

double Zone::getY(){
	return y;
}
