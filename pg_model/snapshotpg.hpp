#ifndef SNAPSHOTPG_H
#define SNAPSHOTPG_H

#include <string>
#include <unordered_map>
#include <unordered_set>

#include "status.hpp"
#include "zone.hpp"

using namespace std;

class snapshotpg{
private:
	unordered_map<string, status> stat;
	unordered_map<string, unsigned int> size;
	unordered_map<string, unordered_map<string, unsigned int>> exchange;
	unordered_map<string, unordered_map<string, unsigned int>> exports;
	unordered_map<string, unsigned int> clock;
	vector<Zone> pz;
	vector<Zone> sz;
	unordered_set<string> backtraced;

	unordered_map<string, double> pinf;

public:
	//snapshotpg();
	void addPop(string id, status stat, unsigned int size);
	void setClock(string id, unsigned int clock);
	unsigned int getClock(string id);
	void addExchange(string id, string wb, unsigned int weight);
	void addExports(string id, string wb, unsigned int weight);
	unsigned int getSize(string id);
	status getStatus(string id);
	unordered_map<string, unsigned int> getExchanges(string id);
	unordered_map<string, unsigned int> getExports(string id);
	vector<string> getIds();
	bool isSet(string id);
	void setSize(string id, unsigned int size);
	void updateExchanges();
	bool isBanned(string id, double x, double y);
	bool isPZ(string id, double x, double y);
	void addPZ( Zone z);
	void addSZ(Zone z);
	void addBacktraceFromInfected(string id);

	double getPinf(string id);
	void setPinf(string id, double p);
};

#endif
