//
//  simulationParameters.cpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#include "simulationParameters.hpp"

// Static variables
// random engine
std::mt19937 simulationParameters::random_engine;
unsigned int simulationParameters::custom_random_engine_seed;
unsigned int simulationParameters::random_engine_seed;
// data and results directory
std::string simulationParameters::data_dir;
std::string simulationParameters::results_dir;
std::string simulationParameters::experiment_dir;
// repetitions
int simulationParameters::number_of_runs;
int simulationParameters::first_run_index;
// id length
//int simulationParameters::pop_id_length;
// species name
std::string simulationParameters::sp1_name;
std::string simulationParameters::sp1_prefix;
std::string simulationParameters::sp2_name;
std::string simulationParameters::sp2_prefix;
// time
double simulationParameters::sub_period_length;
double simulationParameters::observation_period;
double simulationParameters::number_of_sub_period;
double simulationParameters::maximum_length_of_the_experiment;
double simulationParameters::tau;
std::vector<double> simulationParameters::T;
bool simulationParameters::mvt_date_according_to_timestep_tau;
// initial states
int simulationParameters::number_of_infected_sp1_pop;
int simulationParameters::number_of_infected_sp2_pop;
int simulationParameters::nb_of_inf_indiv_initially_introduced;
// epidemic dynamic
bool simulationParameters::density_dependent_tranmission_withinpop_I;
bool simulationParameters::density_dependent_tranmission_betweenpop_I;
bool simulationParameters::density_dependent_tranmission_withinpop_D;
bool simulationParameters::density_dependent_tranmission_betweenpop_D;
double simulationParameters::beta_sp1;
double simulationParameters::beta_sp1Is;
double simulationParameters::beta_sp1Ic;
double simulationParameters::beta_sp1D;
double simulationParameters::beta_sp2;
double simulationParameters::beta_sp2Is;
double simulationParameters::beta_sp2Ic;
double simulationParameters::beta_sp2D;
double simulationParameters::beta_sp1_sp1_ex;
double simulationParameters::beta_sp1_sp1;
double simulationParameters::beta_sp1_sp2;
double simulationParameters::beta_sp2_sp1;
double simulationParameters::beta_sp2_sp2;
double simulationParameters::beta_sp2D_sp1;
double simulationParameters::beta_sp2D_sp2;
double simulationParameters::alpha;
double simulationParameters::delta;
double simulationParameters::sigma;
double simulationParameters::mu_sp1;
double simulationParameters::mu_sp2;
double simulationParameters::gamma;
double simulationParameters::rho;
double simulationParameters::eta;
// control measures
int simulationParameters::start_date_test_on_hunted_wildboar;
double simulationParameters::proportion_of_hunted_wb_tested;
double simulationParameters::test_sensitivity; // not use for the moment because test is considered perfect
double simulationParameters::increase_factor_of_rho_after_carcass_discovery;
double simulationParameters::time_to_test_results;
double simulationParameters::date_on_which_the_fence_is_built;
double simulationParameters::increase_factor_of_eta_within_a_fenced_area;
double simulationParameters::rho_active_search;
double simulationParameters::time_to_plan_an_active_search;
double simulationParameters::active_search_duration;
// inference
bool simulationParameters::sp1_dynamic_from_data;
bool simulationParameters::sp2_dynamic_from_data;
bool simulationParameters::model_coupling_for_prediction;
bool simulationParameters::get_obs_from_data_for_inference;
// save
bool simulationParameters::save_sp1_dynamic;
bool simulationParameters::save_sp2_dynamic;
//new param for period 2
double simulationParameters::tile_size;
double simulationParameters::active_search_radius;
double simulationParameters::rho_passive_search_inside_fenced_buffer_area;
double simulationParameters::proportion_of_hunted_wb_tested_inside_fenced_buffer_area;
double simulationParameters::eta_inside_fenced_buffer_area;
double simulationParameters::date_on_which_p2measures_are_implemented;
double simulationParameters::active_search_radius_outside_fenced_buffer_area_p2measure;

double simulationParameters::date_from_which_to_stop_using_plugged_data;

double simulationParameters::date_until_which_hunting_pressure_is_increased;
double simulationParameters::last_date_of_hunting_period;

// for debug
bool simulationParameters::verbose_mode;

// Constructor
simulationParameters *simulationParameters::instance = NULL;

simulationParameters& simulationParameters::get_instance(int argc, const char * argv[]){
    if (instance == NULL) {
        instance = new simulationParameters(argc, argv);
    }
    return *instance;
}

void simulationParameters::kill(){
    if (instance != NULL) {
        delete instance;
    }
}

simulationParameters::simulationParameters(int argc, const char * argv[]){
    Chronometer chrono_param;
    chrono_param.start();

    // Wrap everything in a try block.  Do this every time,
    // because exceptions will be thrown for problems.
    try {

        // Define the command line object, and insert a message
        // that describes the program. The "Command description message"
        // is printed last in the help text. The second argument is the
        // delimiter (usually space) and the last one is the version number.
        // The CmdLine object parses the argv array based on the Arg objects
        // that it contains.
        TCLAP::CmdLine cmd("Command description message", ' ', "1.0");

        // Define a value argument and add it to the command line.
        // A value arg defines a flag and a type of value that it expects,
        // such as "-n Bishop".
        // TCLAP::ValueArg<std::string> nameArg("n","name","Name to print",true,"homer","string");

        TCLAP::SwitchArg verbose_mode_switch("v","verbose","to activate the verbose mode", cmd, false);

        TCLAP::ValueArg<std::string> data_dir_path("d","data_dir_path","the path to the directory containing the data",true,"","string");
        TCLAP::ValueArg<std::string> results_dir_path("r","results_dir_path","the path to the directory to store the results",true,"","string");

        TCLAP::ValueArg<int> value_number_of_runs("", "number_of_runs", "number_of_runs", false, 1, "int");
        TCLAP::ValueArg<int> value_first_run_index("", "first_run_index", "first_run_index", false, 1, "int");

        TCLAP::ValueArg<int> custom_seed_rdengine("", "custom_seed_rdengine", "a custom seed for the random engine (can't be 0)", false, 0, "int");

        TCLAP::ValueArg<double> value_of_tau("t", "tau", "value of tau to use for the tau-leap method", false, 1.0, "double");
        TCLAP::ValueArg<int> value_of_simulation_duration("", "sim_duration", "duration of the simulation", false, 50, "int");
        TCLAP::ValueArg<int> value_of_observation_duration("", "obs_duration", "duration of the observation", false, 50, "int");
        TCLAP::ValueArg<int> value_of_nb_simu_period("", "nb_sim_period", "number of simulated period", false, 1, "int");
        TCLAP::SwitchArg mvt_date_is_a_multiple_of_tau("","mvt_date_is_a_multiple_of_tau","to indicate if mvts date in agreement with timestep (a multiple of tau)", cmd, false);

        TCLAP::ValueArg<std::string> value_of_sp1_name("", "sp1_name", "name of the first species", false, "spA", "string");
        TCLAP::ValueArg<std::string> value_of_sp1_prefix("", "sp1_prefix", "prefix of the first species", false, "A", "string");
        TCLAP::ValueArg<std::string> value_of_sp2_name("", "sp2_name", "name of the second species", false, "spB", "string");
        TCLAP::ValueArg<std::string> value_of_sp2_prefix("", "sp2_prefix", "prefix of the second species", false, "B", "string");

        TCLAP::ValueArg<int> value_nb_inf_sp1("", "nb_inf_sp1", "number of simulated period", false, 0, "int");
        TCLAP::ValueArg<int> value_nb_inf_sp2("", "nb_inf_sp2", "number of simulated period", false, 0, "int");
        TCLAP::ValueArg<int> value_nb_of_inf_indiv_init("", "nb_inf_init", "number of simulated period", false, 1, "int");

        TCLAP::SwitchArg is_density_dependent_tranmission_withinpop_I("","is_density_dependent_tranmission_withinpop_I","is_density_dependent_tranmission_withinpop_I", cmd, false);
        TCLAP::SwitchArg is_density_dependent_tranmission_betweenpop_I("","is_density_dependent_tranmission_betweenpop_I","is_density_dependent_tranmission_betweenpop_I", cmd, false);
        TCLAP::SwitchArg is_density_dependent_tranmission_withinpop_D("","is_density_dependent_tranmission_withinpop_D","is_density_dependent_tranmission_withinpop_D", cmd, true);
        TCLAP::SwitchArg is_density_dependent_tranmission_betweenpop_D("","is_density_dependent_tranmission_betweenpop_D","is_density_dependent_tranmission_betweenpop_D", cmd, true);
        TCLAP::ValueArg<double> value_beta_sp1("", "beta_sp1", "transmission rate beta_sp1", false, 0.1, "double");
        TCLAP::ValueArg<double> value_beta_sp1Is("", "beta_sp1Is", "transmission rate beta_sp1Is", false, 0.1, "double");
        TCLAP::ValueArg<double> value_beta_sp1Ic("", "beta_sp1Ic", "transmission rate beta_sp1Ic", false, 0.1, "double");
        TCLAP::ValueArg<double> value_beta_sp1D("", "beta_sp1D", "transmission rate beta_sp1D", false, 0.1, "double");
        TCLAP::ValueArg<double> value_beta_sp2("", "beta_sp2", "transmission rate beta_sp2", false, 0.1, "double");
        TCLAP::ValueArg<double> value_beta_sp2Is("", "beta_sp2Is", "transmission rate beta_sp2Is", false, 0.1, "double");
        TCLAP::ValueArg<double> value_beta_sp2Ic("", "beta_sp2Ic", "transmission rate beta_sp2Ic", false, 0.1, "double");
        TCLAP::ValueArg<double> value_beta_sp2D("", "beta_sp2D", "transmission rate beta_sp2D", false, 0.1, "double");
        TCLAP::ValueArg<double> value_beta_sp1_sp1_ex("", "beta_sp1_sp1_ex", "transmission rate sp1-sp1 via exchanges", false, 0.0, "double");
        TCLAP::ValueArg<double> value_beta_sp1_sp1("", "beta_sp1_sp1", "transmission rate when conctact sp1-sp1", false, 0.0, "double");
        TCLAP::ValueArg<double> value_beta_sp1_sp2("", "beta_sp1_sp2", "transmission rate when conctact sp1-sp1", false, 0.05, "double");
        TCLAP::ValueArg<double> value_beta_sp2_sp1("", "beta_sp2_sp1", "transmission rate when conctact sp2-sp1", false, 0.0, "double");
        TCLAP::ValueArg<double> value_beta_sp2_sp2("", "beta_sp2_sp2", "transmission rate when conctact sp2-sp2", false, 0.05, "double");
        TCLAP::ValueArg<double> value_beta_sp2D_sp1("", "beta_sp2D_sp1", "transmission rate when conctact sp2D-sp1", false, 0.0, "double");
        TCLAP::ValueArg<double> value_beta_sp2D_sp2("", "beta_sp2D_sp2", "transmission rate when conctact sp2D-sp2", false, 0.05, "double");

        TCLAP::ValueArg<double> value_alpha("", "alpha", "alpha", false, 1.0, "double");

        TCLAP::ValueArg<double> value_delta("", "delta", "1/delta = duration in E", false, 1/5.0, "double");
        TCLAP::ValueArg<double> value_sigma("", "sigma", "1/sigma = duration in Is", false, 1/2.0, "double");
        TCLAP::ValueArg<double> value_mu_sp1("", "mu_sp1", "mortality rate  for sp1", false, 1/7.0, "double");
        TCLAP::ValueArg<double> value_mu_sp2("", "mu_sp2", "mortality rate  for sp2", false, 1/7.0, "double");
        TCLAP::ValueArg<double> value_gamma("", "gamma", "1/gamma = duration in D", false, 1/90.0, "double");
        TCLAP::ValueArg<double> value_rho("", "rho", "rate at which ind. D are discovered", false, 0.01, "double");
        TCLAP::ValueArg<double> value_eta("", "eta", "hunting  rate", false, 0.0028881132523331052, "double");
        // control measures
        TCLAP::ValueArg<double> value_time_to_test_results("", "time_to_test_results", "time_to_test_results", false, 3.0, "double");
        TCLAP::ValueArg<double> value_test_sensitivity("", "test_sensitivity", "test_sensitivity", false, 1.0, "double");
        TCLAP::ValueArg<int> value_start_date_test_on_hunted_wildboar("", "start_date_test_on_hunted_WB", "start date test on hunted wild boar", false, 1.0, "int");
        TCLAP::ValueArg<double> value_proportion_of_hunted_wb_tested("", "proportion_of_hunted_wb_tested", "proportion_of_hunted_wb_tested", false, 0.2, "double");
        TCLAP::ValueArg<double> value_date_on_which_the_fence_is_built("", "date_on_which_the_fence_is_built", "date_on_which_the_fence_is_built", false, 61.0, "double");
        TCLAP::ValueArg<double> value_increase_factor_of_eta_within_a_fenced_area("", "increase_factor_of_eta_within_a_fenced_area", "increase_factor_of_eta_within_a_fenced_area", false, 1.0, "double");
        TCLAP::ValueArg<double> value_increase_factor_of_rho_after_carcass_discovery("", "increase_factor_of_rho_after_carcass_discovery", "increase_factor_of_rho_after_carcass_discovery", false, 1.0, "double");
        TCLAP::ValueArg<double> value_rho_active_search("", "rho_active_search", "rho_active_search", false, 0.1, "double");
        TCLAP::ValueArg<double> value_time_to_plan_an_active_search("", "time_to_plan_an_active_search", "time_to_plan_an_active_search", false, 5.0, "double");
        TCLAP::ValueArg<double> value_active_search_duration("", "active_search_duration", "active_search_duration", false, 5.0, "double");
        // inference
        TCLAP::SwitchArg is_sp1_dynamic_from_data("","is_sp1_dynamic_from_data","is_sp1_dynamic_from_data", cmd, true);
        TCLAP::SwitchArg is_sp2_dynamic_from_data("","is_sp2_dynamic_from_data","is_sp2_dynamic_from_data", cmd, false);
        TCLAP::SwitchArg is_model_coupling_for_prediction("","is_model_coupling_for_prediction","is_model_coupling_for_prediction", cmd, false);
        TCLAP::SwitchArg is_get_obs_from_data_for_inference("","is_get_obs_from_data_for_inference","is_get_obs_from_data_for_inference", cmd, false);
        // save
        TCLAP::SwitchArg is_save_sp1_dynamic("","is_save_sp1_dynamic","is_save_sp1_dynamic", cmd, false);
        TCLAP::SwitchArg is_save_sp2_dynamic("","is_save_sp2_dynamic","is_save_sp2_dynamic", cmd, true);

        //new param for period 2
        TCLAP::ValueArg<double> value_tile_size("", "tile_size", "tile_size", true, 0.0, "double");
        TCLAP::ValueArg<double> value_active_search_radius("", "active_search_radius", "active_search_radius", false, 1000, "double");
        TCLAP::ValueArg<double> value_rho_passive_search_inside_fenced_buffer_area("", "rho_passive_search_inside_fenced_buffer_area", "rho_passive_search_inside_fenced_buffer_area", false, 0.0, "double");
        TCLAP::ValueArg<double> value_proportion_of_hunted_wb_tested_inside_fenced_buffer_area("", "proportion_of_hunted_wb_tested_inside_fenced_buffer_area", "proportion_of_hunted_wb_tested_inside_fenced_buffer_area", false, 1.0, "double");
        TCLAP::ValueArg<double> value_eta_inside_fenced_buffer_area("", "eta_inside_fenced_buffer_area", "eta_inside_fenced_buffer_area", false, 0.01543760679464734, "double");
        TCLAP::ValueArg<double> value_date_on_which_p2measures_are_implemented("", "date_on_which_p2measures_are_implemented", "date_on_which_p2measures_are_implemented", false, 999, "double");
        TCLAP::ValueArg<double> value_active_search_radius_outside_fenced_buffer_area_p2measure("", "active_search_radius_outside_fenced_buffer_area_p2measure", "active_search_radius_outside_fenced_buffer_area_p2measure", false, 1000, "double");

        TCLAP::ValueArg<double> value_date_from_which_to_stop_using_plugged_data("", "date_from_which_to_stop_using_plugged_data", "date_from_which_to_stop_using_plugged_data", false, 111, "double");

        TCLAP::ValueArg<double> value_date_until_which_hunting_pressure_is_increased("", "date_until_which_hunting_pressure_is_increased", "date_until_which_hunting_pressure_is_increased", false, 120, "double");
        TCLAP::ValueArg<double> value_last_date_of_hunting_period("", "last_date_of_hunting_period", "last_date_of_hunting_period", false, 204, "double");

        // Add the argument nameArg to the CmdLine object. The CmdLine object
        // uses this Arg to parse the command line.
        // cmd.add( nameArg );

        cmd.add( data_dir_path );
        cmd.add( results_dir_path );

        cmd.add( value_number_of_runs );
        cmd.add( value_first_run_index );

        cmd.add( custom_seed_rdengine );

        cmd.add( value_of_tau );
        cmd.add( value_of_simulation_duration );
        cmd.add( value_of_observation_duration );
        cmd.add( value_of_nb_simu_period );

        cmd.add( value_of_sp1_name );
        cmd.add( value_of_sp1_prefix );
        cmd.add( value_of_sp2_name );
        cmd.add( value_of_sp2_prefix );

        cmd.add( value_nb_inf_sp1 );
        cmd.add( value_nb_inf_sp2 );
        cmd.add( value_nb_of_inf_indiv_init );

        cmd.add( value_beta_sp1 );
        cmd.add( value_beta_sp1Is );
        cmd.add( value_beta_sp1Ic );
        cmd.add( value_beta_sp1D );
        cmd.add( value_beta_sp2 );
        cmd.add( value_beta_sp2Is );
        cmd.add( value_beta_sp2Ic );
        cmd.add( value_beta_sp2D );
        cmd.add( value_beta_sp1_sp1_ex );
        cmd.add( value_beta_sp1_sp1 );
        cmd.add( value_beta_sp1_sp2 );
        cmd.add( value_beta_sp2_sp1 );
        cmd.add( value_beta_sp2_sp2 );
        cmd.add( value_beta_sp2D_sp1 );
        cmd.add( value_beta_sp2D_sp2 );

        cmd.add( value_alpha );

        cmd.add( value_delta );
        cmd.add( value_sigma );
        cmd.add( value_mu_sp1 );
        cmd.add( value_mu_sp2 );
        cmd.add( value_gamma );
        cmd.add( value_rho );
        cmd.add( value_eta );

        cmd.add( value_time_to_test_results );
        cmd.add( value_test_sensitivity );
        cmd.add( value_start_date_test_on_hunted_wildboar );
        cmd.add( value_proportion_of_hunted_wb_tested );
        cmd.add( value_date_on_which_the_fence_is_built );
        cmd.add( value_increase_factor_of_eta_within_a_fenced_area );
        cmd.add( value_increase_factor_of_rho_after_carcass_discovery );
        cmd.add( value_rho_active_search );
        cmd.add( value_time_to_plan_an_active_search );
        cmd.add( value_active_search_duration );

        //new param for period 2
        cmd.add( value_tile_size );
        cmd.add( value_active_search_radius );
        cmd.add( value_rho_passive_search_inside_fenced_buffer_area );
        cmd.add( value_proportion_of_hunted_wb_tested_inside_fenced_buffer_area );
        cmd.add( value_eta_inside_fenced_buffer_area );
        cmd.add( value_date_on_which_p2measures_are_implemented );
        cmd.add( value_active_search_radius_outside_fenced_buffer_area_p2measure );

        cmd.add( value_date_from_which_to_stop_using_plugged_data );

        cmd.add( value_date_until_which_hunting_pressure_is_increased );
        cmd.add( value_last_date_of_hunting_period );

        // Define a switch and add it to the command line.
        // A switch arg is a boolean argument and only defines a flag that
        // indicates true or false.  In this example the SwitchArg adds itself
        // to the CmdLine object as part of the constructor.  This eliminates
        // the need to call the cmd.add() method.  All args have support in
        // their constructors to add themselves directly to the CmdLine object.
        // It doesn't matter which idiom you choose, they accomplish the same thing.
        // TCLAP::SwitchArg reverseSwitch("r","reverse","Print name backwards", cmd, false);

        // Parse the argv array.
        cmd.parse( argc, argv );

        // Get the value parsed by each arg.
        // std::string name = nameArg.getValue();

        verbose_mode = verbose_mode_switch.getValue();

        data_dir = data_dir_path.getValue();
        results_dir = results_dir_path.getValue();

        number_of_runs = value_number_of_runs.getValue();
        first_run_index = value_first_run_index.getValue();

        custom_random_engine_seed = custom_seed_rdengine.getValue();

        tau = value_of_tau.getValue();
        sub_period_length = value_of_simulation_duration.getValue();
        observation_period = value_of_observation_duration.getValue();
        number_of_sub_period = value_of_nb_simu_period.getValue();
        mvt_date_according_to_timestep_tau = mvt_date_is_a_multiple_of_tau.getValue();

        sp1_name = value_of_sp1_name.getValue();
        sp1_prefix = value_of_sp1_prefix.getValue();
        sp2_name = value_of_sp2_name.getValue();
        sp2_prefix = value_of_sp2_prefix.getValue();

        number_of_infected_sp1_pop = value_nb_inf_sp1.getValue();
        number_of_infected_sp2_pop = value_nb_inf_sp2.getValue();
        nb_of_inf_indiv_initially_introduced = value_nb_of_inf_indiv_init.getValue();

        density_dependent_tranmission_withinpop_I = is_density_dependent_tranmission_withinpop_I.getValue();
        density_dependent_tranmission_betweenpop_I = is_density_dependent_tranmission_betweenpop_I.getValue();
        density_dependent_tranmission_withinpop_D = is_density_dependent_tranmission_withinpop_D.getValue();
        density_dependent_tranmission_betweenpop_D = is_density_dependent_tranmission_betweenpop_D.getValue();
        beta_sp1 = value_beta_sp1.getValue();
        beta_sp1Is = value_beta_sp1Is.getValue();
        beta_sp1Ic = value_beta_sp1Ic.getValue();
        beta_sp1D = value_beta_sp1D.getValue();
        beta_sp2 = value_beta_sp2.getValue();
        beta_sp2Is = value_beta_sp2Is.getValue();
        beta_sp2Ic = value_beta_sp2Ic.getValue();
        beta_sp2D = value_beta_sp2D.getValue();
        beta_sp1_sp1_ex = value_beta_sp1_sp1_ex.getValue();
        beta_sp1_sp1 = value_beta_sp1_sp1.getValue();
        beta_sp1_sp2 = value_beta_sp1_sp2.getValue();
        beta_sp2_sp1 = value_beta_sp2_sp1.getValue();
        beta_sp2_sp2 = value_beta_sp2_sp2.getValue();
        beta_sp2D_sp1 = value_beta_sp2D_sp1.getValue();
        beta_sp2D_sp2 = value_beta_sp2D_sp2.getValue();

        alpha = value_alpha.getValue();

        delta = value_delta.getValue();
        sigma = value_sigma.getValue();
        mu_sp1 = value_mu_sp1.getValue();
        mu_sp2 = value_mu_sp2.getValue();
        gamma = value_gamma.getValue();
        rho = value_rho.getValue();
        eta = value_eta.getValue();

        time_to_test_results = value_time_to_test_results.getValue();
        test_sensitivity = value_test_sensitivity.getValue();
        start_date_test_on_hunted_wildboar = value_start_date_test_on_hunted_wildboar.getValue();
        proportion_of_hunted_wb_tested = value_proportion_of_hunted_wb_tested.getValue();
        date_on_which_the_fence_is_built = value_date_on_which_the_fence_is_built.getValue();
        increase_factor_of_eta_within_a_fenced_area = value_increase_factor_of_eta_within_a_fenced_area.getValue();
        increase_factor_of_rho_after_carcass_discovery = value_increase_factor_of_rho_after_carcass_discovery.getValue();
        rho_active_search = value_rho_active_search.getValue();
        time_to_plan_an_active_search = value_time_to_plan_an_active_search.getValue();
        active_search_duration = value_active_search_duration.getValue();

        sp1_dynamic_from_data = is_sp1_dynamic_from_data.getValue();
        sp2_dynamic_from_data = is_sp2_dynamic_from_data.getValue();
        model_coupling_for_prediction = is_model_coupling_for_prediction.getValue();
        get_obs_from_data_for_inference = is_get_obs_from_data_for_inference.getValue();

        save_sp1_dynamic = is_save_sp1_dynamic.getValue();
        save_sp2_dynamic = is_save_sp2_dynamic.getValue();

        tile_size = value_tile_size.getValue();
        active_search_radius = value_active_search_radius.getValue();
        rho_passive_search_inside_fenced_buffer_area = value_rho_passive_search_inside_fenced_buffer_area.getValue();
        proportion_of_hunted_wb_tested_inside_fenced_buffer_area = value_proportion_of_hunted_wb_tested_inside_fenced_buffer_area.getValue();
        eta_inside_fenced_buffer_area = value_eta_inside_fenced_buffer_area.getValue();
        date_on_which_p2measures_are_implemented = value_date_on_which_p2measures_are_implemented.getValue();
        active_search_radius_outside_fenced_buffer_area_p2measure = value_active_search_radius_outside_fenced_buffer_area_p2measure.getValue();

        date_from_which_to_stop_using_plugged_data = value_date_from_which_to_stop_using_plugged_data.getValue();

        date_until_which_hunting_pressure_is_increased = value_date_until_which_hunting_pressure_is_increased.getValue();
        last_date_of_hunting_period = value_last_date_of_hunting_period.getValue();

        // TEMP - start
        sp1_name = "domestic_pig";
        sp1_prefix = "DP";
        sp2_name = "wild_boar";
        sp2_prefix = "WB";
        mvt_date_according_to_timestep_tau = true;
//        beta_sp2 // contamnation intra due to Is + Ic
//        beta_sp2D // contamnation intra due to D
//        beta_sp1_sp2 // contamination des WB par DP
//        beta_sp2_sp2 // contamination des WB par WB
//        beta_sp2D_sp2 // contamination des WB par WB_D
        // TEMP - end

        // Do what you intend.

    } catch (TCLAP::ArgException &e)  // catch any exceptions
    { std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; }


    seedTheRandomEngine(custom_random_engine_seed);

    experiment_dir = results_dir + '/' + "experiment_seed_" + std::to_string(random_engine_seed);
    maximum_length_of_the_experiment = number_of_sub_period * sub_period_length;

    printParameterValues();

    for (double x = 0.0; x <= maximum_length_of_the_experiment; x+=tau) {
        T.emplace_back(x);
    }

    chrono_param.stop();
    std::cout << "simulationParameters (time of construction): " << chrono_param.displayInSecond() << " seconds" << std::endl;

}

// Member functions
void simulationParameters::seedTheRandomEngine(unsigned int custom_seed){
    if (custom_seed != 0) {
        random_engine_seed = custom_seed;
    } else {
        auto now = std::chrono::system_clock::now();
        auto duration = now.time_since_epoch();
//        auto time = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
        int time = static_cast<int>(std::chrono::duration_cast<std::chrono::seconds>(duration).count());
        std::random_device rd; // Seed with a real random value, if available
        random_engine_seed = time + rd();
    }

//    random_engine_seed = 3890049477;
    random_engine.seed(random_engine_seed);

//    std::seed_seq sseq{rd(), rd(), rd(), rd(), rd(), rd()};
//    std::vector<std::uint32_t> seeds(5);
//    sseq.generate(seeds.begin(), seeds.end());
//    for (std::uint32_t n : seeds) {std::cout << n << '\n';}
//    random_engine.seed(sseq);
}

void simulationParameters::saveCurrentRandomEngineSeed(){
    std::cout << "Seed used to initiate the random engine: " << random_engine_seed << std::endl;
}

void simulationParameters::printParameterValues(){
    if(verbose_mode){
        std::cout << "data_dir: " << data_dir << std::endl;
        std::cout << "results_dir: " << results_dir << std::endl;

        std::cout << "custom_random_engine_seed: " << custom_random_engine_seed << std::endl;

        std::cout << "tau: " << tau << std::endl;
        std::cout << "sub_period_length: " << sub_period_length << std::endl;
        std::cout << "observation_period: " << observation_period << std::endl;
        std::cout << "number_of_sub_period: " << number_of_sub_period << std::endl;
        std::cout << "mvt_date_according_to_timestep_tau: " << mvt_date_according_to_timestep_tau << std::endl;

        std::cout << "sp1_name: " << sp1_name << std::endl;
        std::cout << "sp1_prefix: " << sp1_prefix << std::endl;
        std::cout << "sp2_name: " << sp2_name << std::endl;
        std::cout << "sp2_prefix: " << sp2_prefix << std::endl;

        std::cout << "number_of_infected_sp1_pop: " << number_of_infected_sp1_pop << std::endl;
        std::cout << "number_of_infected_sp2_pop: " << number_of_infected_sp2_pop << std::endl;
        std::cout << "nb_of_inf_indiv_initially_introduced: " << nb_of_inf_indiv_initially_introduced << std::endl;

        std::cout << "density_dependent_tranmission_withinpop_I: " << density_dependent_tranmission_withinpop_I << std::endl;
        std::cout << "density_dependent_tranmission_betweenpop_I: " << density_dependent_tranmission_betweenpop_I << std::endl;
        std::cout << "density_dependent_tranmission_withinpop_D: " << density_dependent_tranmission_withinpop_D << std::endl;
        std::cout << "density_dependent_tranmission_betweenpop_D: " << density_dependent_tranmission_betweenpop_D << std::endl;
        std::cout << "beta_sp1: " << beta_sp1 << std::endl;
        std::cout << "beta_sp1Is: " << beta_sp1Is << std::endl;
        std::cout << "beta_sp1Ic: " << beta_sp1Ic << std::endl;
        std::cout << "beta_sp1D: " << beta_sp1D << std::endl;
        std::cout << "beta_sp2: " << beta_sp2 << std::endl;
        std::cout << "beta_sp2Is: " << beta_sp2Is << std::endl;
        std::cout << "beta_sp2Ic: " << beta_sp2Ic << std::endl;
        std::cout << "beta_sp2D: " << beta_sp2D << std::endl;
        std::cout << "beta_sp1_sp1_ex: " << beta_sp1_sp1_ex << std::endl;
        std::cout << "beta_sp1_sp1: " << beta_sp1_sp1 << std::endl;
        std::cout << "beta_sp1_sp2: " << beta_sp1_sp2 << std::endl;
        std::cout << "beta_sp2_sp1: " << beta_sp2_sp1 << std::endl;
        std::cout << "beta_sp2_sp2: " << beta_sp2_sp2 << std::endl;
        std::cout << "beta_sp2D_sp1: " << beta_sp2D_sp1 << std::endl;
        std::cout << "beta_sp2D_sp2: " << beta_sp2D_sp2 << std::endl;

        std::cout << "delta: " << delta << std::endl;
        std::cout << "sigma: " << sigma << std::endl;
        std::cout << "mu_sp1: " << mu_sp1 << std::endl;
        std::cout << "mu_sp2: " << mu_sp2 << std::endl;
        std::cout << "rho: " << rho << std::endl;
        std::cout << "eta: " << eta << std::endl;
    }
}
