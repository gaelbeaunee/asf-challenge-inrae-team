#include <iostream>
#include <string>
#include <vector>

#include "snapshotpg.hpp"

using namespace std;

//snapshotpg::snapshotpg(){
//}

void snapshotpg::addPop(string id, status st, unsigned int si){
	stat[id]=st;
	size[id]=si;
}

void snapshotpg::addExchange(string id, string wb, unsigned int weight){
	exchange[id][wb]=weight;
}

void snapshotpg::addExports(string id, string wb, unsigned int weight){
	exports[id][wb]=weight;
}


unsigned int snapshotpg::getSize(string id){
	return size[id];
}

status snapshotpg::getStatus(string id){
	return stat[id];
}

unordered_map<string, unsigned int> snapshotpg::getExchanges(string id){
	return exchange[id];
}

unordered_map<string, unsigned int> snapshotpg::getExports(string id){
	return exports[id];
}

vector<string> snapshotpg::getIds(){
	vector<string> ids;
	for(auto it=stat.begin(); it!=stat.end(); ++it){
		ids.push_back(it->first);
	}
	return ids;
}

bool snapshotpg::isSet(string id){
	return stat.count(id)>0;
}

void snapshotpg::setClock(string id, unsigned int c){
	clock[id]=c;
}

unsigned int snapshotpg::getClock(string id){
	return clock[id];
}

void snapshotpg::setSize(string id, unsigned int si){
	size[id]=si;
}

void snapshotpg::updateExchanges(){
	for(auto it=exchange.begin(); it!=exchange.end(); ++it){
		string dst=it->first;
		unordered_map<string, unsigned int> ex=exchange[dst];
		for(auto e=ex.begin(); e!=ex.end(); ++e){
			string src=e->first;
			unsigned int w=e->second;
			unsigned int size_dst, size_src;
			size_dst=getSize(dst);
			size_src=getSize(src);
			setSize(src, size_src-w);
			setSize(dst, size_dst+w);
		}
	}
}

bool snapshotpg::isBanned(string id, double x, double y){
	if(backtraced.count(id)>0){
		return true;
	}
	for(auto zone : sz){
		if(zone.isIn(x, y)){
			return true;
		}
	}
	for(auto zone : pz){
		if(zone.isIn(x, y)){
			return true;
		}
	}
	return false;
}

bool snapshotpg::isPZ(string id, double x, double y){
	for(auto zone : pz){
		if(zone.isIn(x, y)){
			return true;
		}
	}
	return false;
}

void snapshotpg::addPZ(Zone z){
	pz.push_back(z);
}

void snapshotpg::addSZ(Zone z){
	sz.push_back(z);
}

void snapshotpg::addBacktraceFromInfected(string id){
	for(auto &traced : exports[id]){
		backtraced.insert(traced.first);	
	}
}

double snapshotpg::getPinf(string id){
	return pinf[id];
}

void snapshotpg::setPinf(string id, double p){	
	pinf[id]=p;
}

