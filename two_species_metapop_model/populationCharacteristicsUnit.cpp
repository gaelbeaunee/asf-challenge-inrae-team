//
//  populationCharacteristicsUnit.cpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#include "populationCharacteristicsUnit.hpp"

populationCharacteristicsUnit::populationCharacteristicsUnit(){
    pop_id = "nan";
    pop_type = "nan";
    pop_initial_size = 0;
}

populationCharacteristicsUnit::populationCharacteristicsUnit(const std::string& _pop_id, const std::string& _pop_type, const int& _pop_initial_size){
    pop_id = _pop_id;
    pop_type = _pop_type;
    pop_initial_size = _pop_initial_size;
}

populationCharacteristicsUnit::populationCharacteristicsUnit(const std::string& _pop_id, const std::string& _pop_type, const int& _pop_initial_size, const double& _centroid_X, const double& _centroid_Y, const bool& _is_within_fenced_area, const bool& _is_15km_around_fenced_area){
    pop_id = _pop_id;
    pop_type = _pop_type;
    pop_initial_size = _pop_initial_size;
    centroid_X = _centroid_X;
    centroid_Y = _centroid_Y;
    is_within_fenced_area = _is_within_fenced_area;
    is_15km_around_fenced_area = _is_15km_around_fenced_area;
}
