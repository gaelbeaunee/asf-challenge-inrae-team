#ifndef SNAPSHOTWB_H
#define SNAPSHOTWB_H

#include <string>
#include <unordered_map>

#include "status.hpp"

using namespace std;

class snapshotwb{
private:
	unordered_map<string, unsigned int> Scount;
	unordered_map<string, unsigned int> Ecount;
	unordered_map<string, unsigned int> Iscount;
	unordered_map<string, unsigned int> Iccount;
	unordered_map<string, unsigned int> Rcount;

public:
	snapshotwb();
	void addPop(string id, unsigned int S, unsigned int E, unsigned int Is, unsigned int Ic, unsigned int R);
	void addPop(string id, status s, unsigned int count);
	unsigned int getCount(string id, status stat);
};

#endif
