#include <iostream>
#include <cmath>
#include <cstdlib> 
#include <unordered_map>
#include <iomanip>
#include <fstream>
#include <vector>
#include <boost/tokenizer.hpp>


#include "simulatorpg.hpp"
#include "status.hpp"

SimulatorPG::SimulatorPG(){
}

SimulatorPG::SimulatorPG(double t_0, double t_f, double step, PopPG poppg, PopWB popwb){
	t0=t_0;
	tf=t_f;
	timestep=step;
	pg=poppg;
	wb=popwb;
}

void SimulatorPG::setTimes(double tini, double tfin, double dt){
	tmin=tini;
	t0=tini;
	tf=tfin;
	timestep=dt;
}

void SimulatorPG::setTimes(double tm, double tini, double tfin, double dt){
	tmin=tm;
	t0=tini;
	tf=tfin;
	tfobs=tfin;
	timestep=dt;
}

void SimulatorPG::setTimes(double tm, double tini, double tfin, double tobs, double dt){
	tmin=tm;
	t0=tini;
	tf=tfin;
	tfobs=tobs;
	timestep=dt;
}

void SimulatorPG::setSeed(unsigned int s){
	seed=s;
	srand48(s);
}

void SimulatorPG::setParameters(double a, double b, double g, unsigned int tE, unsigned int tIs, unsigned int tIc, double qobs){
	alpha=a;
	beta=b;
	gamma=g;
	timeE=tE;
	timeIs=tIs;
	timeIc=tIc;
	timeR=PAR_TIMER;
	q=qobs;
}

void SimulatorPG::setParameters(double a, double b, double g, unsigned int tE, unsigned int tIs, unsigned int tIc, unsigned int tR, double qobs){
	alpha=a;
	beta=b;
	gamma=g;
	timeE=tE;
	timeIs=tIs;
	timeIc=tIc;
	timeR=tR;
	q=qobs;
}

void SimulatorPG::setControlMeasures(double dpz, double rpz, double dsz, double rsz, double durback, double pastback){
	durpz=dpz;
	radpz=rpz;
	dursz=dsz;
	radsz=rsz;
	durbacktrace=durback;
	pastbacktrace=pastback;
}


unordered_map<string, string> SimulatorPG::getStatusPG(double t){
	vector<string> ids=pg.getIds(t);
	unordered_map<string, string> map;
	for(auto id=ids.begin(); id!=ids.end(); ++id){
		map[*id]=Status::statusToString(pg.getStatus(*id, t));
	}
	return map;
}

void SimulatorPG::forward(double t1, double t2){
	double alpha, p;
	unsigned int newsize, clock;
	status newstat;
	vector<string> ids=pg.getIds(t1);

#ifdef CMP2_SZ
	if(t2>CMP2_START){
		setControlMeasures(CM_DURPZ, CM_RADPZ, CM_DURSZ, CMP2_RADSZ,CM_BTDUR, CM_BTPAST);
	}
#endif

#ifdef CMP2_CULL_PZ
	if(t2>CMP2_START){
		// Culling in current PZ
		for(auto id=ids.begin(); id!=ids.end(); ++id){
			if(pg.isPZ(*id, t2)){
				pg.addSnapshotPop(t2, *id, R, pg.getSize(*id));
			}
		}
	}
#endif

	for(auto id=ids.begin(); id!=ids.end(); ++id){
		newsize=pg.getSize(*id, t1);
		clock=pg.getClock(t1, *id);

		switch(pg.getStatus(*id, t1)){
			case S:
				p=infectionProbability(t1, *id);
				alpha=drand48();
				if(alpha<p){
					clock=1;
					newstat=E;
				} else {
					clock=0;
					newstat=S;
				}
				break;
			case E:
				if(clock>=pg.getTimeE(*id)){
					clock=1;
					newstat=Is;
				} else {
					newstat=E;
					clock+=1;
				}
				break;
			case Is:
				if(clock>=pg.getTimeIs(*id)){
					clock=1;
					newstat=Ic;

					// Detection + control measures
					if(!pg.isSet(*id, t2)){
						addControlMeasures(*id, t1);
					}

				} else {
					newstat=Is;
					clock+=1;
				}
				break;
			case Ic:
				if(clock>=timeIc){
					clock=1;
					newstat=R;
				} else {
					newstat=Ic;
					clock+=1;
				}
				break;
			case R:
				if(clock>timeR){
					newstat=S;
					clock=1;
				} else {
					clock+=1;
					newstat=R;
				}
				break;
		}

		if(!pg.isSet(*id, t2)){
			pg.addSnapshotPop(t2, *id, newstat, newsize);
			pg.setClock(t2, *id, clock);
		} else {
			pg.addSnapshotPop(t2, *id, pg.getStatus(*id, t2), newsize);
			pg.setClock(t2, *id, clock);
		}
	}

	// Update exchanges for t2
	//pg.updateExchanges(t2);
}

double SimulatorPG::infectionProbability(double t, string id){
	double p, r1=0, r2=0, r3=0;
	vector<string> wbid;
	vector<string> pgcontid;

	if(!pg.isBanned(id, t)){ // Check for movement bans
		// Infectious exchanges at time t
		unordered_map<string, unsigned int> exchange=pg.getExchanges(id, t);
		for(auto it=exchange.begin(); it!=exchange.end(); ++it){
			string eid=it->first;
			if(!pg.isBanned(eid, t)){
				unsigned int w=it->second;
				status s=pg.getStatus(eid, t);
				if(s==Is || s==Ic){
					r1+=w;
				}
			}
		}

		// Latent exchanges at previous times 
		for(int i=t0; i<t; i++){
			unordered_map<string, unsigned int> exchange=pg.getExchanges(id, i*timestep);
			for(auto it=exchange.begin(); it!=exchange.end(); ++it){
				string eid=it->first;
				if(!pg.isBanned(eid, t)){
					unsigned int w=it->second;
					status s=pg.getStatus(eid, t-i*timestep);
					if(s==E || s==Is || s==Ic){
						r1+=w;
					}
				}
			}
		}
	}

	// WB contact part
	if(pg.getType(id)==outdoor){
		wbid=pg.getContacts(id);
		for(int i=0; i<wbid.size(); i++){
			double I=wb.getCount(t, wbid[i], Is)+wb.getCount(t, wbid[i], Ic);
			double N=I+wb.getCount(t, wbid[i], S)+wb.getCount(t, wbid[i], E);
			if(N>0){
				r2+=I/N;
			}
		}
	}

	// PG contact part
	pgcontid=pg.getLocalContacts(id);
	for(int i=0; i<pgcontid.size(); i++){
		double N=pg.getSize(pgcontid[i]);
		if(pg.getStatus(pgcontid[i], t)==Is || pg.getStatus(pgcontid[i], t)==Ic){
			r3+=N;
		}
	}


	// Probability
	p=1-exp(-(alpha*r1+(double)pg.getSize(id)*beta*r2+(double)pg.getSize(id)*gamma*r3));

	return(p);
}

double SimulatorPG::likelihood(unordered_map<string, double> tobs, bool islog){
	double tol=1e-16, p, pold; 
	string id;
	double l=0;

	// Fill with obs
int N=LIKELIHOOD_REP;
for(int k=0; k<N; k++){
	pg.newStatusTimes();
	fillWithObs(tobs);

	// Herds with observation
	for(auto &it: tobs){
		id=it.first;
		int durE=pg.getTimeE(id);
		int durIs=pg.getTimeIs(id);
		double tinf=it.second-timestep*(durE+durIs+1);
		if(it.second<=tf){
			for(double t=t0; t<tinf; t+=timestep){
				if(pg.getStatus(id, t)==S){
					p=infectionProbability(t, id);
					pold=pg.getInfectionProbability(t, id);
					pg.setInfectionProbability(t, id, pold+p/N);
					if(k==N-1){
						p=pold+p/N;
						l+=log(1-p);
					}
				} else {
					break;
				}
			}
			if(tinf>t0 && pg.getStatus(id, tinf)==S){
				p=infectionProbability(tinf, id);
				pold=pg.getInfectionProbability(tinf, id);
				pg.setInfectionProbability(tinf, id, pold+p/N);
				if(k==N-1){
					p=pold+p/N;
					l+=log(p);
				}
			}
		} else {
			for(double t=t0; t<=tf-durE-durIs; t+=timestep){
				if(pg.getStatus(id, t)==S){
					p=infectionProbability(t, id);
					pold=pg.getInfectionProbability(t, id);
					pg.setInfectionProbability(t, id, pold+p/N);
					if(k==N-1){
						p=pold+p/N;
						l+=log(1-p);
					}
				} else {
					break;
				}
			}
		}
	}	

	// Herds without observation
	for(auto &it : pg.getType()){
		id=it.first;
		int durE=pg.getTimeE(id);
		int durIs=pg.getTimeIs(id);
		for(double t=t0; t<=tf-durE-durIs; t+=timestep){
			if(pg.getStatus(id, t)==S){
				p=infectionProbability(t, id);
				pold=pg.getInfectionProbability(t, id);
				pg.setInfectionProbability(t, id, pold+p/N);
				if(k==N-1){
					p=pold+p/N;
					l+=log(1-p);
				}
			} else {
				break;
			}
		}
	}	
}

	if(islog){
		return l;
	} else {
		return exp(l+tol);
	}
}

double SimulatorPG::likelihood(vector<obs> tobs, bool islog){
	double tol=1e-16, p, pold; 
	string id;
	double l=0;

	// Fill with obs
int N=LIKELIHOOD_REP;
for(int k=0; k<N; k++){
	pg.newStatusTimes();
	fillWithObs(tobs);

	// Herds with observation
	for(auto &it: tobs){
		id=it.id;
		int durE=pg.getTimeE(id);
		int durIs=pg.getTimeIs(id);
		double tinf=it.t-timestep*(durE+durIs+1);
		if(it.t<=tf){
			for(double t=t0; t<tinf; t+=timestep){
				if(pg.getStatus(id, t)==S){
					p=infectionProbability(t, id);
					pold=pg.getInfectionProbability(t, id);
					pg.setInfectionProbability(t, id, pold+p/N);
					if(k==N-1){
						p=pold+p/N;
						l+=log(1-p);
					}
				} else {
					break;
				}
			}
			if(tinf>t0 && pg.getStatus(id, tinf)==S){
				p=infectionProbability(tinf, id);
				pold=pg.getInfectionProbability(tinf, id);
				pg.setInfectionProbability(tinf, id, pold+p/N);
				if(k==N-1){
					p=pold+p/N;
					l+=log(p);
				}
			}
		} else {
			for(double t=t0; t<=tf-durE-durIs; t+=timestep){
				if(pg.getStatus(id, t)==S){
					p=infectionProbability(t, id);
					pold=pg.getInfectionProbability(t, id);
					pg.setInfectionProbability(t, id, pold+p/N);
					if(k==N-1){
						p=pold+p/N;
						l+=log(1-p);
					}
				} else {
					break;
				}
			}
		}
	}	

	// Herds without observation
	for(auto &it : pg.getType()){
		id=it.first;
		int durE=pg.getTimeE(id);
		int durIs=pg.getTimeIs(id);
		for(double t=t0; t<=tf-durE-durIs; t+=timestep){
			if(pg.getStatus(id, t)==S){
				p=infectionProbability(t, id);
				pold=pg.getInfectionProbability(t, id);
				pg.setInfectionProbability(t, id, pold+p/N);
				if(k==N-1){
					p=pold+p/N;
					l+=log(1-p);
				}
			} else {
				break;
			}
		}
	}	
}

	if(islog){
		return l;
	} else {
		return exp(l+tol);
	}
}

void SimulatorPG::fillWithInit(){
	string id;
	double t, curt;
	unsigned int size;

	for(auto &it: pg.getType()){
		id=it.first;	
		size=pg.getSize(id);
		curt=tmin;
		while(curt<=t0){
			pg.addSnapshotPop(curt, id, S, size);
			curt+=timestep;
		}
	}
}


void SimulatorPG::fillWithObs(unordered_map<string, double> tsusp){
	string id;
	double t, curt;
	unsigned int size;
	for(auto &it : tsusp){
		id=it.first;	
		t=it.second;
		size=pg.getSize(id);

		if(t<=tfobs){
			// Initialise Control measures
			addControlMeasures(id, t);

			// Setting Ic and R
			for(int i=0; i<timeIc; i++){
				pg.addSnapshotPop(t+i*timestep, id, Ic, size);
			}
			pg.addSnapshotPop(t+timeIc*timestep, id, R, size);
			pg.setClock(t+timeIc*timestep, id, 1);

			// Setting E, Is
			int durE=pg.getTimeE(id);
			int durIs=pg.getTimeIs(id);
			curt=t-timestep;
			while(curt>=tmin){
				if((t-curt)<=timestep*durIs){
					pg.addSnapshotPop(curt, id, Is, size);
				} else if((t-curt)<=timestep*(durIs+durE)){
					pg.addSnapshotPop(curt, id, E, size);
                    if(!pg.isSet(id, curt)){
				        pg.addSnapshotPop(curt, id, S, size);
                    }
				}
				curt-=timestep;
			}
		} else {
			int durE=pg.getTimeE(id);
			int durIs=pg.getTimeIs(id);
			curt=tfobs-durE-durIs;
			while(curt>=tmin){
				pg.addSnapshotPop(curt, id, S, size);
				curt-=timestep;
			}
		}
	}

	// Unobserved herds
	for(auto &it: pg.getType()){
		id=it.first;	
		if(tsusp.count(id)==0){
			int durE=pg.getTimeE(id);
			int durIs=pg.getTimeIs(id);
			size=pg.getSize(id);
			t=tfobs+1;
			curt=tfobs-durE-durIs;
			curt=curt<t0?t0:curt;
			while(curt>=tmin){
				pg.addSnapshotPop(curt, id, S, size);
				curt-=timestep;
			}
		}
	}

}

void SimulatorPG::fillWithObs(vector<obs> tsusp){
	string id;
	double t, curt;
	unsigned int size;
    set<string> detected;

	for(auto &it : tsusp){
		id=it.id;
		t=it.t;
		size=pg.getSize(id);
        detected.insert(id);

		if(t<=tfobs){
			// Initialise Control measures
			addControlMeasures(id, t);

			// Setting Ic and R
			for(int i=0; i<timeIc; i++){
				pg.addSnapshotPop(t+i*timestep, id, Ic, size);
			}
			for(int i=0; i<PAR_TIMER; i++){
			    pg.addSnapshotPop(t+timeIc*timestep+i, id, R, size);
			    pg.setClock(t+timeIc*timestep+i, id, i+1);
            }

			// Setting E, Is
			int durE=pg.getTimeE(id);
			int durIs=pg.getTimeIs(id);
			curt=t-timestep;
			while(curt>=tmin){
				if((t-curt)<=timestep*durIs){
					pg.addSnapshotPop(curt, id, Is, size);
				} else if((t-curt)<=timestep*(durIs+durE)){
					pg.addSnapshotPop(curt, id, E, size);
				} else {
                    if(!pg.isSet(id, curt)){
					    pg.addSnapshotPop(curt, id, S, size);
                    } 
                }
				curt-=timestep;
			}
		} else {
			int durE=pg.getTimeE(id);
			int durIs=pg.getTimeIs(id);
			curt=tfobs-durE-durIs;
			while(curt>=tmin){
				pg.addSnapshotPop(curt, id, S, size);
				curt-=timestep;
			}
		}
	}

	// Unobserved herds
	for(auto &it: pg.getType()){
		id=it.first;	
		if(!detected.count(id)){
			int durE=pg.getTimeE(id);
			int durIs=pg.getTimeIs(id);
			size=pg.getSize(id);
			t=tfobs+1;
			curt=tfobs-durE-durIs;
			curt=curt<t0?t0:curt;
			while(curt>=tmin){
				pg.addSnapshotPop(curt, id, S, size);
				curt-=timestep;
			}
		}
	}

}


void SimulatorPG::fillWithObs(unordered_map<string, double> tsusp, unordered_map<string, double> tpreventive){
	string id;
	double t;
	unsigned int size;
	
	// Detections
	fillWithObs(tsusp);

	// Preventive culling
	for(auto &it : tpreventive){
		id=it.first;	
		t=it.second;
		if(t<tfobs){
			size=pg.getSize(id);
			pg.addSnapshotPop(t, id, R, size);
			pg.setClock(t, id, 1);
		}
	}
}

void SimulatorPG::fillWithObs(vector<obs> tsusp, vector<obs> tpreventive){
	string id;
	double t;
	unsigned int size;
	
	// Detections
	fillWithObs(tsusp);

	// Preventive culling
	for(auto &it : tpreventive){
		id=it.id;	
		t=it.t;
		if(t<tfobs){
			size=pg.getSize(id);
			pg.addSnapshotPop(t, id, R, size);
			pg.setClock(t, id, 1);
		}
	}
}

void SimulatorPG::addControlMeasures(string id, double t1){
	double tend;

	// Protection zones
	tend=t1+durpz<tf?t1+durpz:tf;
	for(double t=t1; t<tend; t+=timestep){
		pg.addPZ(id, t, radpz);

		// Culling herds in PZ
#ifdef CMP2_CULL_PZ
		if(t>=CMP2_START){
			pg.addSnapshotPop(t, id, R, pg.getSize(id));
		}
#endif
	}

	// Surveillance zones
	tend=t1+dursz<tf?t1+dursz:tf;
	for(double t=t1; t<tend; t+=timestep){
		pg.addSZ(id, t, radsz);
	}

	// Backtrace
	tend=t1+durbacktrace<tf?t1+durbacktrace:tf;
	pg.addBacktrace(id, t1, pastbacktrace, tend, timestep);
}


unordered_map<string, double> SimulatorPG::readSuspTimes(string file){
	using namespace boost;
	unordered_map<string, double> tsusp;
	typedef boost::tokenizer<boost::char_separator<char>> Tokenizer;
	boost::char_separator<char> sep{","};
	string line;
	vector<string> vec;
	
	ifstream in(file.c_str());
	if (!in.is_open()){
		return tsusp;
	}

	// header
	if(getline(in,line)){
		Tokenizer tok(line, sep);
		vec.assign(tok.begin(),tok.end());
	}

	while(getline(in,line)){
		Tokenizer tok(line, sep);
		vec.assign(tok.begin(),tok.end());
		tsusp[vec[0]]=stod(vec[1]);
	}
	in.close();

	return tsusp;
}

int SimulatorPG::getInfections(double t){
	int inf=0;
	for(auto &id : pg.getIds(t)){
		if(pg.getStatus(id, t)==Ic && pg.getClock(t, id)==1)
			inf++;
	}

	return inf;
}

double SimulatorPG::lsCriterion(unordered_map<string, double> tobs){
	int Nsim=0, Nobs=0;
	double c;
	for(double t=t0; t<=tf; t+=timestep){
		for(auto &it : tobs){
			if(it.second==t){
				Nobs++;
			}
		}
		Nsim+=getInfections(t);
	}
	c=(Nsim-Nobs)*(Nsim-Nobs);
    cerr << "Nsim = " << Nsim << " Nobs = " << Nobs << " c = " << c << endl;
	return c;
}

double SimulatorPG::lsCriterion(vector<obs> tobs){
	int Nsim=0, Nobs=0;
	double c;
	for(double t=t0; t<=tf; t+=timestep){
		for(auto &it : tobs){
			if(it.t==t){
				Nobs++;
			}
		}
		Nsim+=getInfections(t);
	}
	c=(Nsim-Nobs)*(Nsim-Nobs);
    cerr << "Nsim = " << Nsim << " Nobs = " << Nobs << " c = " << c << endl;
	return c;
}

int SimulatorPG::preventiveCulling(std::unordered_map<double, std::unordered_map<std::string, std::vector<std::unordered_map<std::string, double>>>> carcasses_location, double t){
	int count=0;
	// At selected time, go over populations
	for(auto &it : carcasses_location[t]){
		string idWB=it.first;
		// Get positions of carcass
		for(std::unordered_map<std::string, double> carcasses_location_item : it.second) {
			double wbX = carcasses_location_item["x_coord"];
			double wbY = carcasses_location_item["y_coord"];

			// Check if DP pops are in range
			for(auto &idDP : wb.getContactsDP(idWB)){
				double dpX=pg.getX(idDP);	
				double dpY=pg.getY(idDP);	
				double d=sqrt((wbX-dpX)*(wbX-dpX)+(wbY-dpY)*(wbY-dpY));
				if(d<PREVCULL_DIST){
					// Set to R
					pg.addSnapshotPop(t+timestep, idDP, R, pg.getSize(idDP));
					pg.setClock(t+timestep, idDP, 1);
					count++;
				}
			}
		}
	}
	return count;
}

vector<obs> SimulatorPG::readObsVec(string file){
	using namespace boost;
    vector<obs> tsusp;
	typedef boost::tokenizer<boost::char_separator<char>> Tokenizer;
	boost::char_separator<char> sep{","};
	string line;
	vector<string> vec;
	
	ifstream in(file.c_str());
	if (!in.is_open()){
		return tsusp;
	}

	// header
	if(getline(in,line)){
		Tokenizer tok(line, sep);
		vec.assign(tok.begin(),tok.end());
	}

	while(getline(in,line)){
		Tokenizer tok(line, sep);
		vec.assign(tok.begin(),tok.end());
        struct obs o;
        o.id=vec[0];
        o.t=stod(vec[1]);
        tsusp.push_back(o);
	}
	in.close();

	return tsusp;
}

