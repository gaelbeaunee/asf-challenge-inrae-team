//
//  population.hpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#ifndef population_hpp
#define population_hpp

#include <stdio.h>
#include <iostream>
#include <string>
#include <unordered_map>
#include <set>
#include <algorithm>
#include <cmath>
#include <memory>

#include "simulationParameters.hpp"
#include "simulationData.hpp"
#include "sirHeadcount.hpp"
#include "metapopulationSummary.hpp"
#include "auxiliaryFunctions.hpp"


class population {
public:
    // Constructor
    population(){}
    population(const std::string& _pop_id, const int& _pop_index, const int& _pop_init_size, const bool& _is_infected, std::unordered_map<std::string, std::unique_ptr <population>>& _all_populations, metapopulationSummary& _metapop_summary);

    // Variables
    // population characteristics
    std::string pop_id = "nan";
    int pop_index= NAN;
    std::string pop_species = "nan";
    bool is_sp1 = false;
    bool is_sp2 = false;
    int pop_init_size = 0;
    //
    double coord_centroid_X;
    double coord_centroid_Y;
    // control measure - hunting
    bool is_within_a_fenced_area = false;
    bool is_15km_around_a_fenced_area = false;
    // control measure - active search
    bool ongoing_active_search = false;
    double nb_current_ongoing_active_search = 0.0;
    std::unordered_map<double, std::vector<double>> period_of_active_search;
    // headcount
    sirHeadcount headcount = sirHeadcount();
    //
    std::vector<std::string> ordered_event_list;
    std::unordered_map<std::string, sirHeadcount> m_change;
    std::unordered_map<std::string, double> m_rate;
    std::unordered_map<std::string, int> m_events_count;
    //

    // Member functions
    virtual void updateStochasticDynamic(const double& _timestep, std::unordered_map<std::string, std::unique_ptr <population>>& _all_populations, metapopulationSummary& _metapop_summary);
    void updateMetapopSummary(const double& _timestep, metapopulationSummary& _metapop_summary);
    void performOutgoingMovements(const double& _timestep, std::unordered_map<std::string, sirHeadcount>& _list_of_next_pending_movements, metapopulationSummary& _metapop_summary);
    void performIngoingMovements(const double& _timestep, const std::unordered_map<std::string, sirHeadcount>& _list_of_current_pending_movements, metapopulationSummary& _metapop_summary);
    void displayCurrentStatus();
    void test() {std::cout << "population" << std::endl;}


	 // TODO: @Gael, I moved it here for visibility when calling from metapopulation.cpp, no time to find a better solution ;)
    void updateSnapshotOfTheDynamic(const double& _timestep, const sirHeadcount& _sirHeadcount_for_updating, metapopulationSummary& _metapop_summary);
};

class species1Population : public population {
public:
    // Constructor
    species1Population(const std::string& _pop_id, const int& _pop_index, const int& _pop_init_size, const bool& _is_infected, std::unordered_map<std::string, std::unique_ptr <population>>& _all_populations, metapopulationSummary& _metapop_summary);

    // Member functions
    void updateStochasticDynamic(const double& _timestep, std::unordered_map<std::string, std::unique_ptr <population>>& _all_populations, metapopulationSummary& _metapop_summary);
    //void updateSnapshotOfTheDynamic(const double& _timestep, const sirHeadcount& _sirHeadcount_for_updating, metapopulationSummary& _metapop_summary);
    void test() {std::cout << "species1Population" << std::endl;}
};

class species2Population : public population {
public:
    // Constructor
    species2Population(const std::string& _pop_id, const int& _pop_index, const int& _pop_init_size, const bool& _is_infected, std::unordered_map<std::string, std::unique_ptr <population>>& _all_populations, metapopulationSummary& _metapop_summary);

    // Member functions
    void updateStochasticDynamic(const double& _timestep, std::unordered_map<std::string, std::unique_ptr <population>>& _all_populations, metapopulationSummary& _metapop_summary);
    void test() {std::cout << "species2Population" << std::endl;}
};

#endif /* population_hpp */
