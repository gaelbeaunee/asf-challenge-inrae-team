//
//  main.cpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#include <iostream>
#include <vector>
#include <memory>
#include <string>

#ifdef PGCOUPLING
#include <time.h>
#endif

#include "simulationParameters.hpp"
#include "simulationData.hpp"
#include "metapopulation.hpp"

int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "Hello, World!\n";

    simulationParameters& simuparam = simulationParameters::get_instance(argc, argv);
    simuparam.saveCurrentRandomEngineSeed();
    simulationData& simudata = simulationData::get_instance();

    std::vector<double> v_compuation_time;
    for (auto i = simulationParameters::first_run_index; i < (simulationParameters::first_run_index + simulationParameters::number_of_runs); i++) {
        metapopulation metapop_test = metapopulation(i);

		  #ifdef PGCOUPLING
		  if (simulationParameters::model_coupling_for_prediction) {
		  	// Initialize simpg, Parameters : initial time, final time
		  	metapop_test.initSimpg(0, simulationParameters::sub_period_length, simulationParameters::observation_period);
		  	// Initialize the RNG (srand48)
		  	metapop_test.simpg.setSeed(time(NULL));
		}
		  #endif

        double computation_time_for_this_rep = metapop_test.stoc_dyn_iteration();
        v_compuation_time.emplace_back(computation_time_for_this_rep);
        metapop_test.save_headcount_over_time();
        metapop_test.save_events_over_time();
        if(simulationParameters::get_obs_from_data_for_inference){
            metapop_test.save_rho_over_time();
        }
        metapop_test.save_carcasses_location();

		  #ifdef PGCOUPLING
		  if (simulationParameters::model_coupling_for_prediction) {
		  	metapop_test.save_pg(0, simulationParameters::sub_period_length);
		  }
		  #endif
    }

    double mean_computation_time = std::accumulate(v_compuation_time.begin(), v_compuation_time.end(), 0.0) / v_compuation_time.size();
    std::cout << "The computation time for one single repetition takes an average of " << mean_computation_time << " second(s)" << std::endl;

    return 0;
}
