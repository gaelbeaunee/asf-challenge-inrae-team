#!/bin/bash
#$ -S /bin/bash
#$ -N smplxWB
#$ -cwd
# #$ -wd

# WB model newcrit+om
#beta_sp2    beta_sp2D beta_sp1_sp2 beta_sp2_sp2 beta_sp2D_sp2
#0.159261 9.781216e-05   0.06033231 1.983415e-07  6.250972e-17
#rho: 0.0183312
#rho_passive_search_inside_fenced_buffer_area: 0.0093876
#rho_active_search: 0.48807

# PG model
#beta_sp1_sp1_ex	beta_sp2_sp1 beta_sp1_sp1
#1e-3					2e-5				1e-11

BETA_SP2=0.159261
BETA_SP2D=9.781216e-05
BETA_SP1_SP2=0.06033231
BETA_SP2_SP2=1.983415e-07
BETA_SP2D_SP2=6.250972e-17
RHO=0.0183312
RHO_ACTIVE_SEARCH=0.48807
RHO_FENCE=0.0093876
BETA_SP1_SP1_EX=1e-3
BETA_SP2_SP1=2e-5
BETA_SP1_SP1=1e-4
DATA_DIR="../data/day_110/17321om"
RES_DIR="./res_day_110"
OBS_PERIOD=110
PERIOD=140
REPETITIONS=1

[ -z "$1" ] || RES_DIR="$1"
mkdir -p "$RES_DIR"

./sir_model_metapop_two_species \
	--is_model_coupling_for_prediction \
	--is_save_sp1_dynamic \
	--sim_duration ${PERIOD} \
	--obs_duration ${OBS_PERIOD} \
	-d ${DATA_DIR} \
	-r ${RES_DIR} \
	--number_of_runs ${REPETITIONS} --first_run_index 1 \
	--beta_sp2 ${BETA_SP2} \
	--beta_sp2D ${BETA_SP2D} \
	--beta_sp1_sp2 ${BETA_SP1_SP2} \
	--beta_sp2_sp2 ${BETA_SP2_SP2} \
	--beta_sp2D_sp2 ${BETA_SP2D_SP2} \
	--rho ${RHO} \
	--rho_active_search ${RHO_ACTIVE_SEARCH} \
	--rho_passive_search_inside_fenced_buffer_area ${RHO_FENCE} \
	--beta_sp1_sp1_ex ${BETA_SP1_SP1_EX} \
	--beta_sp1_sp1 ${BETA_SP1_SP1} \
	--beta_sp2_sp1 ${BETA_SP2_SP1} \
	--is_get_obs_from_data_for_inference \
	--tile_size 17321
