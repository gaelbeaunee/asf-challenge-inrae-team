//
//  Chronometer.hpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#ifndef Chronometer_hpp
#define Chronometer_hpp

#include <chrono>
#include <cmath>

class Chronometer {
    // Variables ===========================================================
    std::chrono::steady_clock::time_point  tbegin,tend;
    double texec=0.;

public:
    // Constructor =========================================================
    Chronometer();
    // Member functions ====================================================
    void start();
    void stop();
    double displayInSecond(){return texec/pow(10, 9);}
    double displayInMillisecond(){return texec/pow(10, 6);}
    double displayInMicrosecond(){return texec/pow(10, 3);}
    double displayInNanosecond(){return texec;}

};

#endif /* Chronometer_hpp */
