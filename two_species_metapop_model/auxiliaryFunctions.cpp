//
//  auxiliaryFunctions.cpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#include "auxiliaryFunctions.hpp"

void auxFunctions::randomSamplingWithoutReplacement(std::vector<std::string> _input, std::vector<std::string>& _output, const int& _count, std::mt19937& _random_engine){
    if (_count < _input.size()) {
        std::shuffle(_input.begin(), _input.end(), _random_engine);
        _output.insert(_output.end(), _input.end() - _count, _input.end());
    } else {
        _output.insert(_output.end(), _input.begin(), _input.end());
    }
}

void auxFunctions::randomSamplingWithoutReplacement(const std::set<std::string>& _input, std::set<std::string>& _output, const int& _count, std::mt19937& _random_engine){
    std::vector<std::string> v_input(_input.begin(), _input.end());
    if (_count < v_input.size()) {
        std::shuffle(v_input.begin(), v_input.end(), _random_engine);
        _output.insert(v_input.end() - _count, v_input.end());
    } else {
        _output.insert(_input.begin(), _input.end());
    }
//    std::cout << "randomSamplingWithoutReplacement of " << _count << " element(s)" << std::endl;
//    for (auto item : v_input) {
//        std::cout << item << ' ';
//    }
}

std::vector<std::string> auxFunctions::randomSamplingWithoutReplacement(std::vector<std::string> _input, const int& _count, std::mt19937& _random_engine){
    std::vector<std::string> output;
    if (_count < _input.size()) {
        std::shuffle(_input.begin(), _input.end(), _random_engine);
        output.insert(output.end(), _input.end() - _count, _input.end());
    } else {
        output.insert(output.end(), _input.begin(), _input.end());
    }
    return output;
}

std::vector<std::string> auxFunctions::randomSamplingWithReplacement(const std::set<std::string>& _input, const int& _count, std::mt19937& _random_engine){
    std::vector<std::string> output;
    int min = 0;
    int max = static_cast<int>(_input.size())-1;
    std::uniform_int_distribution<> dist(min, max);
    for (int n=0; n < _count; ++n) {
        int index = dist(_random_engine);
//        // DEBUG
//        std::cout << "index: " << index << " | size: " << _input.size() << std::endl;
//        // DEBUG
        std::string item = *std::next(_input.begin(), index);
        output.emplace_back(item);
    }
    return output;
}

void auxFunctions::createDirectory(const std::string& the_dir_path){
    struct stat dir_info;
    bool is_the_path_exists = (stat(the_dir_path.c_str(), &dir_info) == 0);
    if (is_the_path_exists) {
        if (dir_info.st_mode & S_IFDIR) {
            // nothing to do, the directory already exists
        } else {
            std::cout << "WARNING! - " << the_dir_path << "already exists but is not a directory" << std::endl;
        }
    } else {
        // create the experiment directory
        // mkdir(the_dir_path.c_str(), S_IRWXU | S_IRGRP | S_IXGRP);
        mkdir(the_dir_path.c_str(), 0750);
    }
}

bool auxFunctions::pointInTriangle(const double p_X, const double p_Y, const double t0_X, const double t0_Y, const double t1_X, const double t1_Y, const double t2_X, const double t2_Y)
{
    double s = t0_Y * t2_X - t0_X * t2_Y + (t2_Y - t0_Y) * p_X + (t0_X - t2_X) * p_Y;
    double t = t0_X * t1_Y - t0_Y * t1_X + (t0_Y - t1_Y) * p_X + (t1_X - t0_X) * p_Y;

    if ((s < 0) != (t < 0))
        return false;

    double A = -t1_Y * t2_X + t0_Y * (t2_X - t1_X) + t0_X * (t1_Y - t2_Y) + t1_X * t2_Y;

    return A < 0 ? (s <= 0 && s + t >= A) : (s >= 0 && s + t <= A); // condition ? expression_if_true : expression_if_false;
}

std::unordered_map<std::string, double> auxFunctions::randomPointInHexagon(const double h_X, const double h_Y, const double outer_circle_diameter, std::mt19937& _random_engine){
    std::unordered_map<std::string, double> candidate_coord;
    double radius = outer_circle_diameter/2.0;
    double pointInHexagon = false;
    double candidate_x;
    double candidate_y;
    std::uniform_real_distribution<double> distribution(0.0,1.0);

    // Define the six points of the hexagon
    // left point
    double h0_X = -radius + h_X;
    double h0_Y = 0 + h_Y;
    // top left point
    double h1_X = -radius/2.0 + h_X;
    double h1_Y = std::sqrt(3)*radius/2.0 + h_Y;
    // top right point
    double h2_X = radius/2.0 + h_X;
    double h2_Y = std::sqrt(3)*radius/2.0 + h_Y;
    // right point
    double h3_X = radius + h_X;
    double h3_Y = 0 + h_Y;
    // bottom right point
    double h4_X = radius/2.0 + h_X;
    double h4_Y = -std::sqrt(3)*radius/2.0 + h_Y;
    // bottom left point
    double h5_X = -radius/2.0 + h_X;
    double h5_Y = -std::sqrt(3)*radius/2.0 + h_Y;

//    // Define candidate point in Cartesian coordinates
//    double a = distribution(_random_engine) * 2 * M_PI;
//    double r = radius * std::sqrt(distribution(_random_engine));
//    candidate_x = r * cos(a) + h_X;
//    candidate_y = r * sin(a) + h_Y;

    while (pointInHexagon == false) {
        // Define candidate point in Cartesian coordinates
        double a = distribution(_random_engine) * 2 * M_PI;
        double r = radius * std::sqrt(distribution(_random_engine));
        candidate_x = r * cos(a) + h_X;
        candidate_y = r * sin(a) + h_Y;
        // Test if point in one of the triangle in the corresponding corner
        if (candidate_x <= h_X) {
            if (candidate_y >= h_Y) {
                // top_left corner
                pointInHexagon = pointInTriangle(candidate_x, candidate_y, h_X, h_Y, h0_X, h0_Y, h1_X, h1_Y) + pointInTriangle(candidate_x, candidate_y, h_X, h_Y, h1_X, h1_Y, h2_X, h2_Y);
            } else {
                // bottom_left corner
                pointInHexagon = pointInTriangle(candidate_x, candidate_y, h_X, h_Y, h4_X, h4_Y, h5_X, h5_Y) + pointInTriangle(candidate_x, candidate_y, h_X, h_Y, h5_X, h5_Y, h0_X, h0_Y);
            }
        } else {
            if (candidate_y >= h_Y) {
                // top_right corner
                pointInHexagon = pointInTriangle(candidate_x, candidate_y, h_X, h_Y, h1_X, h1_Y, h2_X, h2_Y) + pointInTriangle(candidate_x, candidate_y, h_X, h_Y, h2_X, h2_Y, h3_X, h3_Y);
            } else {
                // bottom_right corner
                pointInHexagon = pointInTriangle(candidate_x, candidate_y, h_X, h_Y, h3_X, h3_Y, h4_X, h4_Y) + pointInTriangle(candidate_x, candidate_y, h_X, h_Y, h4_X, h4_Y, h5_X, h5_Y);
            }
        }
    }

    candidate_coord = {{"x_coord", candidate_x}, {"y_coord", candidate_y}};

    return candidate_coord;
}
