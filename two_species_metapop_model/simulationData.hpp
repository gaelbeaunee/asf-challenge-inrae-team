//
//  simulationData.hpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#ifndef simulationData_hpp
#define simulationData_hpp

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <set>
#include <armadillo>

#include "simulationParameters.hpp"
#include "populationCharacteristicsUnit.hpp"
#include "observedDataPoint.hpp"
#include "sirHeadcount.hpp"
#include "Chronometer.hpp"

class simulationData {
private:
    static simulationData *instance;

    simulationData();

    // Member functions
    void loadPopulationCharacteristics();
    void loadMovementNetworkData();
    void loadContactNetworkData();
    void buildContactMatrix();
    void loadDynamicData();
    void loadObsData();

    // Variables
    bool population_characteristics_loaded = false;
    bool movement_network_data_loaded = false;
    bool contact_network_data_loaded = false;
    bool contact_matrix_built = false;

public:
    // Constructor
    static simulationData& get_instance();
    
    static void kill();

    // Member functions
    static void rebuildContactMatrix(const bool& with_fenced_area);

    // Variables
    static std::unordered_map<std::string, populationCharacteristicsUnit> pop_characteristics;
    static std::vector<std::string> all_pop_id_list;
    static std::unordered_map<std::string, int> map_of_pop_index;
    static std::set<std::string> sp1_pop_id_list;
    static std::set<std::string> sp2_pop_id_list;
    static std::unordered_map<std::string, std::unordered_map<double, std::unordered_map<std::string, int>>> mvts_network;
    static std::unordered_map<std::string, std::set<std::string>> network_contact_with_sp1_pop;
    static std::unordered_map<std::string, std::set<std::string>> network_contact_with_sp2_pop;
    static arma::mat mat_contact_sp1_pop;
    static arma::mat mat_contact_sp2_pop;
    static std::unordered_map<std::string, std::unordered_map<double, sirHeadcount>> sp1_predefined_dynamic;
    static std::unordered_map<std::string, std::unordered_map<double, sirHeadcount>> sp2_predefined_dynamic;

    static std::unordered_map<std::string, std::unordered_map<double, observedDataPoint>> sp2_obs_data;

};

#endif /* simulationData_hpp */
