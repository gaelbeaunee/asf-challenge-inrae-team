# ASF challenge - INRAE team

Source code for the modelling and inference approach implemented by the INRAE team

# Two-species metapopulation model

## A SIR model for a two-species metapopulation, coded in c++.

Populations can be linked through the movement of individuals or contacts between populations, which are both described using graph (adjacency list).

# Compilation

This section describes the dependancies and requirements to compile the different parts of the model.
The code is split in two parts, on section that contains the code for simulation of the coupled model and wild boar-specific model, and another section for simulation specific to the domestic pig model.

## Coupled model

Depencies are : openblas, lapack, armadillo and tclap.
If not available by default, paths to include files for armadillo and tclap must be specified in `INCLDIR=` using the `-I` option.
Go in the compilationOnCluster directory and run : `make`
This will compile the coupled model.

The code is set up to run on a sge cluster.
The SGE section can be replace with a standard bash script using the following code (Line 287-288 of *inference_WB_main.py*) :
```
RUNS=theNumberOfRuns
for i in $(seq 1 $RUNS)
do
    SGE_TASK_ID=$i <the launching command> &
done
wait
```

## Domestic pig model

Go in the pg_model directory and run : `make`
This will compile the domestic pig specific parts

## Inference

The inference part for the wild boar part of the model is based on a python implementation for the simplex method.
Dependencies are : numpy, pandas

# Data files

There are numerous data files used to specify input data such as wild board and domestic herds characteristics, local contact data, exchange data and epidemic observations.
There are also model simulation output store in various data files.

The data files are coma separated with headers specifying the column data.

## Input files

The input files are located in the *data* directory.

Population related files : 
- mvts_network.csv
- network_contact_***.csv
- pop_characteristics.csv
- pop_geo_pos.csv

Epidemic related files : 
- adj_list_DP_neighbours_in_ProtectionZone.csv
- adj_list_DP_neighbours_in_SurveillanceZone.csv
- obs_wild_boar_events_dynamic.csv
- predefined_dynamic_DP_pop.csv
- preventive_cull_time_DP.csv
- susp_time_DP.csv
- moves_Players_day_110.csv

## Output files

The output files are located in the coupled iteration output directory (see next section), in sub-directories *pg* (for domestic pif herds) and *wb* (for wild board herds).

Domestic pig herds related output files : 
- status_rep_***.csv
- parameters_pg_optim.csv

Wild board heards related output files : 
- headcount_over_time_***.csv
- wild_boar_calibrated_rho_***.csv
- wild_boar_carcasses_location_***.csv
- wild_boar_events_dynamic_***.csv
- wild_boar_SEIsIcDRN_dynamic_***.csv

# Parameter inference

The bash script *coupled_inference.sh* describes both how to estimate parameters and run model simulations.
It is split in two parts :

1. Parameters definitions for both the domestic pigs and wild boar parts of the model.
2. Inference loop alternating inference and simulations for the wild boar part of the model, and inference and simulation for the domestic pigs part of the model.

After properly configuring the script (see section *Parameter definitions* below), parameter inference can be performed using the following line :

```
./coupled_inference.sh
```

## Parameter definitions

### Domestic pig parameters

These variables should be changes accordingly depending on the period :

- TMIN : initial time
- T0 : origin of time (first observation)
- TF : final time
- TFOBSEST=80
- SEED : seed value (not used)
- ALPHA : starting value for parameter
- BETA : starting value for parameter
- GAMMA : starting value for parameter
- POPDESC : path to the population file
- MVTFILE : path to the movements file
- CONTFILE : path to the contacts file
- LOCALCONTFILE : path to the local contacts file
- OBSFILE : path to the observations file
- PCULLFILE : path to the culling information

These variables are set according to the directory structure and should not be changed : 

- PGAVGWB : path to the averaging script to compute average poplation sizes from wild boar simulmations
- PGLIK : path to the likelihood or cost function used in inference
- PGINF : path to the inference script
- PGSIM : path to the simulator for domestic pigs

### Wild boar parameters

- SIMPLX : path to the inference script for wild boar inference
- OUTPUT_SIMPLX : (empty)
- PROGWB : path to the simulator
- DATAWB : path to the directory containing wild boar initial files
- WBOBSDATA : path to the directory containing wild boar initial files
- RESWB : (empty)
- ESTWB : (empty)
- IVAL : control paramter for inference script 
- FVAL : control parameter for inference script
- PARAMWBNAMES : space separated list of parameter names
- PARAMWBVAL : space separated list of initial parameter values
- PARAMWBMIN : space separated list of minimum parameter values
- PARAMWBMAX : space separated list of maximum parameter values
- RELAUNCHTHRES : control parameter for inference script
- RELAUNCHREP : control parameter for inference script
- INITIALSTATUS : path to inital status of domestic pig herds
- TILESIE : tile size
- SIMDUR : simulation end time
- DATESTOPPLUGDATA : (SIMDUR+1)

### Other parameters

- TFENCE : fence date
- NITER : number of iteration of the coupled inference (should be 1)
- NREP : number of repetitions


## Inference and simulation

A directory is created for each iteration of the inference loop.
Each directory contains two sub-directories, one for the wild boar related outputs and one for the domestic pig related output.

### Wild boar iteration

The files describing the domestic pig populations are set to the correct files.
Then inference is performed with the following line :

```
    python3 $SIMPLX \
        -d $DATAWB \
        -r $RESWB \
        -n $OUTPUT_SMPLX \
        -e $WBOBSDATA \
        -I $IVAL \
        -F $FVAL \
        -i $NREP \
        --paramName $PARAMWBNAMES \
        --paramVal $PARAMWBVAL \
        --paramMin $PARAMWBMIN \
        --paramMax $PARAMWBMAX \
        --relaunchThreshold $RELAUNCHTHRES \
        --relaunchNbRepeat $RELAUNCHREP \
        --tile_size $TILESIZE \
        --sim_duration $SIMDUR \
        --date_on_which_the_fence_is_built $TFENCE \
        --date_from_which_to_stop_using_plugged_data $DATESTOPPLUGDATA
```

After inference is performed, simulations are found in the wild boar related output directory. The output files are put in variables for use in the domestic pig part.

### Domestic pig iteration

Simulated output populations for widl boar are averaged prior to performing inference for the domestic pig model parameters.

Inference is performed using the following line :

```
    Rscript $PGINF $TMIN $T0 $TF $TFOBSEST $SEED $ALPHA $BETA $GAMMA $PARAMFILE $PGLIK $PGINFILES
```

Simulations of domestic pig status is then performed using the estimated parameters with the following line : 

```
        $PGSIM $TMIN $T0 $TF $TF $j $ALPHA $BETA $GAMMA < $PGINFILES > ${RESDIR}/coupling_iter_${i}/pg/status_rep_${j}.csv 
```

The simulation ouputs are in the corresponding directory and will be used for the next iterations of wild boar inference.
