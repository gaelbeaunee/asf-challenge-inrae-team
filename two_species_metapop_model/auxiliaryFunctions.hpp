//
//  auxiliaryFunctions.hpp
//  two_species_metapop_model
//
//  Created by Gael Beaunée on 23/09/2020.
//

#ifndef auxiliaryFunctions_hpp
#define auxiliaryFunctions_hpp

#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <random>
#include <algorithm>
#include <unordered_map>
#include <sys/types.h>
#include <sys/stat.h>
#include <memory>
#include <cmath>

namespace auxFunctions {

    void randomSamplingWithoutReplacement(std::vector<std::string> _input, std::vector<std::string>& _output, const int& _count, std::mt19937& _random_engine);

    void randomSamplingWithoutReplacement(const std::set<std::string>& _input, std::set<std::string>& _output, const int& _count, std::mt19937& _random_engine);

    std::vector<std::string> randomSamplingWithoutReplacement(std::vector<std::string> _input, const int& _count, std::mt19937& _random_engine);

    std::vector<std::string> randomSamplingWithReplacement(const std::set<std::string>& _input, const int& _count, std::mt19937& _random_engine);

    void createDirectory(const std::string& the_dir_path);

    bool pointInTriangle(const double p_X, const double p_Y, const double t0_X, const double t0_Y, const double t1_X, const double t1_Y, const double t2_X, const double t2_Y);

    std::unordered_map<std::string, double> randomPointInHexagon(const double h_X, const double h_Y, const double outer_circle_diameter, std::mt19937& _random_engine);

}

#endif /* auxiliaryFunctions_hpp */
